

#include "FPSProject.h"

#include "GameHUD.h"
#include "Slate/MenuStyles.h"
#include "Slate/GlobalGameHUDStyle.h"
#include "Slate/SGameUI.h"
#include "FPSGameMode.h"
#include "FPSPlayerController.h"

AGameHUD::AGameHUD(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    IsSlateInitialized = false;
}

void AGameHUD::PostInitializeComponents() {
    Super::PostInitializeComponents();

    FSlateStyleRegistry::UnRegisterSlateStyle(FMenuStyles::GetStyleSetName());
    FMenuStyles::Initialize();

    UE_LOG(LogTemp, Warning, TEXT("Slate wasn't initialized"));
    if (GEngine && GEngine->GameViewport) {
        UGameViewportClient* ViewPort = GEngine->GameViewport;

        SAssignNew(GameHUDWidget, SGameUI).GameHUD(TWeakObjectPtr<AGameHUD>(this));

        ViewPort->AddViewportWidgetContent(SAssignNew(WidgetContainer, SWeakWidget).PossiblyNullContent(GameHUDWidget.ToSharedRef()));
    }

    IsSlateInitialized = true;
}

bool AGameHUD::GetInventoryOpen() {
    return GameHUDWidget->InventoryOverlay->GetVisibility() == EVisibility::Visible ? true : false;
}

void AGameHUD::SetInventoryOpen(bool Open) {
    if (!GameHUDWidget.IsValid() || !GameHUDWidget->InventoryOverlay.IsValid()) {
        return;
    }

    if (Open) {
        GameHUDWidget->InventoryOverlay->SetVisibility(EVisibility::Visible);
    }
    else {
        GameHUDWidget->InventoryOverlay->SetVisibility(EVisibility::Collapsed);
    }
}

bool AGameHUD::GetCharacterPanelOpen() {
    return GameHUDWidget->CharacterPanelOverlay->GetVisibility() == EVisibility::Visible ? true : false;
}

void AGameHUD::SetCharacterPanelOpen(bool Open) {
    if (!GameHUDWidget.IsValid() || !GameHUDWidget->CharacterPanelOverlay.IsValid()) {
        return;
    }

    if (Open) {
        GameHUDWidget->CharacterPanelOverlay->SetVisibility(EVisibility::Visible);
    }
    else {
        GameHUDWidget->CharacterPanelOverlay->SetVisibility(EVisibility::Collapsed);
    }
}

bool AGameHUD::GetCraftingManagerOpen() {
    return GameHUDWidget->CraftingMenuOverlay->GetVisibility() == EVisibility::Visible ? true : false;
}

void AGameHUD::SetCraftingManagerOpen(bool Open) {
    if (!GameHUDWidget.IsValid() || !GameHUDWidget->CraftingMenuOverlay.IsValid()) {
        return;
    }

    if (Open) {
        GameHUDWidget->CraftingMenuOverlay->SetVisibility(EVisibility::Visible);
    }
    else {
        GameHUDWidget->CraftingMenuOverlay->SetVisibility(EVisibility::Collapsed);
    }
}

bool AGameHUD::IsAnySubwidgetOpen() {
    return GetCraftingManagerOpen() || GetInventoryOpen() || GetCharacterPanelOpen();
}

void AGameHUD::DrawHUD() {
    // Check if any subwidgets are open, if not set mouse focus to game viewport, this is a hacky workaround

    auto CurrentFocus = FSlateApplication::Get().GetKeyboardFocusedWidget();
    if (CurrentFocus.IsValid() && FSlateApplication::Get().GetGameViewport().IsValid()) {
        if (IsAnySubwidgetOpen() && CurrentFocus != GameHUDWidget.ToSharedRef()) {
            FSlateApplication::Get().ClearKeyboardFocus(EKeyboardFocusCause::SetDirectly);
            FSlateApplication::Get().SetKeyboardFocus(GameHUDWidget, EKeyboardFocusCause::SetDirectly);
            UE_LOG(LogTemp, Log, TEXT("Focus to Widget"));
        }
        else if (!IsAnySubwidgetOpen() && CurrentFocus != FSlateApplication::Get().GetGameViewport().ToSharedRef()) {
            FSlateApplication::Get().ClearKeyboardFocus(EKeyboardFocusCause::SetDirectly);
            FSlateApplication::Get().SetFocusToGameViewport();
            UE_LOG(LogTemp, Log, TEXT("Set focus to gameviewport"));
        }
    }
}
