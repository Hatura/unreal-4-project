#include "FPSProject.h"
#include "GameMenuHUD.h"
#include "SMenuUI.h"
#include "GlobalMenuStyle.h"
#include "MenuStyles.h"

void SMenuUI::Construct(const FArguments &args) {
    GameMenuHUD = args._GameMenuHUD;

    MenuStyle = &FMenuStyles::Get().GetWidgetStyle<FGlobalStyle>("Global_Menu");

    ChildSlot
    [
        SNew(SOverlay)
        + SOverlay::Slot()
            .HAlign(HAlign_Center)
            .VAlign(VAlign_Top)
            [
                SNew(STextBlock)
                    .TextStyle(&MenuStyle->MenuTitleStyle)
                    .Text(FText::FromString("Main Menu"))
            ]
        + SOverlay::Slot()
            .HAlign(HAlign_Right)
            .VAlign(VAlign_Bottom)
            [
                SNew(SVerticalBox)
                + SVerticalBox::Slot()
                    [
                        SNew(SButton)
                            .ButtonStyle(&MenuStyle->MenuButtonStyle)
                            .TextStyle(&MenuStyle->MenuButtonTextStyle)
                            .Text(FText::FromString("Play Game!"))
                            .OnClicked(this, &SMenuUI::PlayGameClicked)
                    ]
                + SVerticalBox::Slot()
                    [
                        SNew(SButton)
                            .ButtonStyle(&MenuStyle->MenuButtonStyle)
                            .TextStyle(&MenuStyle->MenuButtonTextStyle)
                            .Text(FText::FromString("Quit Game"))
                            .OnClicked(this, &SMenuUI::QuitGameClicked)
                    ]
            ]
    ];
}

FReply SMenuUI::PlayGameClicked()
{
    GameMenuHUD->PlayGameClicked();
    return FReply::Handled();
}

FReply SMenuUI::QuitGameClicked()
{
    GameMenuHUD->QuitGameClicked();
    return FReply::Handled();
}
