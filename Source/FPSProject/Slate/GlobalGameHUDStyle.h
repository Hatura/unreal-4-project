/**
  * This is for the INGAME-Menu!
  */

#pragma once

#include "Slate.h"
#include "SlateWidgetStyleContainerBase.h"
#include "Inventory/Inventory.h"

#include "GlobalGameHUDStyle.generated.h"

// Provides a group of global style settings for our game menus
USTRUCT()
struct FGlobalGameHUDStyle_Struct : public FSlateWidgetStyle {
    GENERATED_USTRUCT_BODY()

    // Stores a list of Brushes we are using into OutBrushes
    virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;

    // Stores the TypeName for our widget style.
    static const FName TypeName;

    // Retrieves the type name for our global style, which will be used by our style set to load the right file
    virtual const FName GetTypeName() const override;

    // Allows us to set default values for our various styles.
    static const FGlobalGameHUDStyle_Struct& GetDefault();

    // TitleBar Style
    UPROPERTY(EditAnywhere, Category = CommonStyle)
    FButtonStyle MinimizeButton;

    UPROPERTY(EditAnywhere, Category = CommonStyle)
    FSlateBrush TitleBarBorder;

    UPROPERTY(EditAnywhere, Category = CommonStyle)
    FTextBlockStyle TitleBarText;

    UPROPERTY(EditAnywhere, Category = CommonStyle)
    FSlateBrush FieldBackground_Common;

    UPROPERTY(EditAnywhere, Category = CommonStyle)
    FSlateBrush FieldBackground_Uncommon;

    // Hotbar
    UPROPERTY(EditDefaultsOnly, Category = ActionBar)
    FSlateBrush HotbarBackground;

    UPROPERTY(EditDefaultsOnly, Category = ActionBar)
    FSlateBrush HotbarBackgroundEquiped;

    // Inventory
    UPROPERTY(EditAnywhere, Category = Inventory)
    FTextBlockStyle TitleText;

    UPROPERTY(EditDefaultsOnly, Category = Inventory)
    FSlateBrush InventoryBackground;

    UPROPERTY(EditDefaultsOnly, Category = Inventory)
    FSlateBrush InventoryBackground_Selected;

    UPROPERTY(EditDefaultsOnly, Category = Inventory)
    FSlateBrush InventoryBackground_Hover;

    // Item-Empty for fail safe
    UPROPERTY(EditDefaultsOnly, Category = Inventory)
    FSlateBrush Item_Empty;

    // Character panel
    UPROPERTY(EditDefaultsOnly, Category = CharacterPanel)
    FSlateBrush CharacterPanelBackground;

    // Crafting menu
    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FSlateBrush CraftingMenuBorderStyle;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FSlateBrush CraftingMenuTooltipBorderStyle;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FButtonStyle CraftingMenuCraftButton;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FButtonStyle CraftingMenuCraftButtonBuilding;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FSlateBrush CraftingMenuCraftButtonBG;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FTextBlockStyle DisplayNameStyle;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FTextBlockStyle TypeAndTimeStyle;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FTextBlockStyle TooltipTitle;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FTextBlockStyle TooltipProperty;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FTextBlockStyle TooltipBottomInfo;

    UPROPERTY(EditDefaultsOnly, Category = CraftingMenu)
    FSlateBrush TooltipIconHightlightBorder;

    // Crafting Progress Bar
    UPROPERTY(EditDefaultsOnly, Category = CraftingProgressBar)
    FProgressBarStyle CraftingProgressBarStyle;
};

// Proveis a widget style container to allow us to edit properties in-editor
UCLASS(hidecategories = Object, MinimalAPI)
class UGlobalGameHUDStyle : public USlateWidgetStyleContainerBase {
    GENERATED_UCLASS_BODY()

public:
    // This is our actual Style object
    UPROPERTY(EditAnywhere, Category = Appearance, meta = (ShowOnlyInnerProperties))
    FGlobalGameHUDStyle_Struct GameHUDStyle;

    // Retrieves the style that his container manages
    virtual const struct FSlateWidgetStyle* const GetStyle() const override;
};
