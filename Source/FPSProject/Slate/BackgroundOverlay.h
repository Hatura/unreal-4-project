#pragma once

#include "Slate.h"

class SHackButton : public SButton {
    FReply OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;

    FReply OnMouseButtonUp(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;

    FReply OnMouseButtonDoubleClick(const FGeometry &InMyGeometry, const FPointerEvent &InMouseEvent) override;
};

class SBackgroundOverlay : public SCompoundWidget
{
public:
    SLATE_BEGIN_ARGS(SBackgroundOverlay)
    {
    }

    SLATE_END_ARGS()

    void Construct(const FArguments &InArgs);

    bool SupportsKeyboardFocus() const override { return true; }

    FReply OnKeyboardFocusReceived(const FGeometry &MyGeometry, const FKeyboardFocusEvent &InKeyboardFocusEvent) override;

    FReply OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;

    FReply OnMouseButtonUp(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;

    FReply OnMouseButtonDoubleClick(const FGeometry &InMyGeometry, const FPointerEvent &InMouseEvent) override;

    FReply OnButtonClick();
};
