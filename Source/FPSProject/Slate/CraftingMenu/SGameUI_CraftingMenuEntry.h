#pragma once
#include "Slate.h"
#include "../SGameUI.h"
#include "Crafting/BaseRecipe.h"
#include "FPSPlayerController.h"

class SGameUI_CraftingMenuEntry : public SCompoundWidget
{
public:
    SLATE_BEGIN_ARGS(SGameUI_CraftingMenuEntry)
        : _GameHUD(),
          _PCOwner(),
          _CraftingMenu(),
          _Recipe()
    {
    }

    SLATE_ARGUMENT(TWeakObjectPtr<class AGameHUD>, GameHUD)
    SLATE_ARGUMENT(TWeakObjectPtr<class AFPSPlayerController>, PCOwner)
    SLATE_ARGUMENT(SGameUI_CraftingMenu*, CraftingMenu)
    SLATE_ARGUMENT(ABaseRecipe*, Recipe)

    SLATE_END_ARGS()

    void Construct(const FArguments& args);

    void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

    FReply OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;

private:
    TWeakObjectPtr<class AGameHUD> GameHUD;
    TWeakObjectPtr<class AFPSPlayerController> PCOwner;
    SGameUI_CraftingMenu* CraftingMenu;
    ABaseRecipe* Recipe;

    TAttribute<const FSlateBrush*> SlateBrush;
    const FSlateBrush* GetSlateBrush() const;

    TAttribute<FText> DisplayName;
    FText GetDisplayName() const;

    TAttribute<FText> ObjectType;
    FText GetObjectType() const;

    TAttribute<FText> CraftingTime;
    FText GetCraftingTime() const;

    TAttribute<FSlateColor> CraftingButtonColor;
    FSlateColor GetCraftingButtonColor() const;

    FReply TryCrafting();

    const struct FGlobalGameHUDStyle_Struct* HUDStyle;
};
