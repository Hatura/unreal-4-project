#include "FPSProject.h"

#include "../GlobalGameHUDStyle.h"
#include "../MenuStyles.h"
#include "FPSCharacter.h"
#include "SGameUI_CraftingMenuTooltip.h"

void SGameUI_CraftingMenuTooltip::Construct(const FArguments &args) {
    PCOwner = args._PCOwner;
    GameHUD = args._GameHUD;
    HoveredRecipe = args._HoveredRecipe;

    HUDStyle = &FMenuStyles::Get().GetWidgetStyle<FGlobalGameHUDStyle_Struct>("Global_GameHUD");

    DisplayName.Bind(this, &SGameUI_CraftingMenuTooltip::GetDisplayName);
    ObjectImage.Bind(this, &SGameUI_CraftingMenuTooltip::GetObjectImage);

    Ingredient1_SB.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient1_SB);
    Ingredient1_NameAndAmount.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient1_NameAndAmount);

    Ingredient2_SB.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient2_SB);
    Ingredient2_NameAndAmount.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient2_NameAndAmount);

    Ingredient3_SB.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient3_SB);
    Ingredient3_NameAndAmount.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient3_NameAndAmount);

    Ingredient4_SB.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient4_SB);
    Ingredient4_NameAndAmount.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient4_NameAndAmount);

    Ingredient5_SB.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient5_SB);
    Ingredient5_NameAndAmount.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient5_NameAndAmount);

    Ingredient6_SB.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient6_SB);
    Ingredient6_NameAndAmount.Bind(this, &SGameUI_CraftingMenuTooltip::GetIngredient6_NameAndAmount);

    const float BrushIconMaxHeight = 29.0f;

    ChildSlot//.HAlign(HAlign_Fill).VAlign(VAlign_Fill)
    [
        SNew(SBorder).BorderImage(&HUDStyle->CraftingMenuTooltipBorderStyle)
        [
            SNew(SBox)
            //.HeightOverride(120)
            .WidthOverride(300)
            .HAlign(HAlign_Center)
            .Padding(6.0f)
            [
                SNew(SVerticalBox)
                // Title
                + SVerticalBox::Slot().HAlign(HAlign_Center).VAlign(VAlign_Top)
                [
                    SNew(STextBlock)
                        .TextStyle(&HUDStyle->TooltipTitle)
                        .Text(DisplayName)
                ]
                // Image
//                + SVerticalBox::Slot().HAlign(HAlign_Center).VAlign(VAlign_Top)
//                [
//                    SNew(SImage).Image(ObjectImage)
//                ]
                + SVerticalBox::Slot().VAlign(VAlign_Top).MaxHeight(BrushIconMaxHeight)
                [
                    SNew(SUniformGridPanel)
                    + SUniformGridPanel::Slot(0, 0)
                    [
                        SNew(SHorizontalBox)
                        + SHorizontalBox::Slot().FillWidth(1)
                        [
                            SNew(SBorder).BorderImage(&HUDStyle->TooltipIconHightlightBorder)
                            [
                                SNew(SImage).Image(Ingredient1_SB)
                            ]
                        ]
                        + SHorizontalBox::Slot().FillWidth(4).Padding(FMargin(2.0f, 8.0f, 0, 0))
                        [
                            SNew(STextBlock)
                                .TextStyle(&HUDStyle->TooltipProperty)
                                .Text(Ingredient1_NameAndAmount)
                        ]
                    ]
                    + SUniformGridPanel::Slot(1, 0)
                    [
                        SNew(SHorizontalBox)
                        + SHorizontalBox::Slot().FillWidth(1)
                        [
                            SNew(SBorder).BorderImage(&HUDStyle->TooltipIconHightlightBorder)
                            [
                                SNew(SImage).Image(Ingredient2_SB)
                            ]
                        ]
                        + SHorizontalBox::Slot().FillWidth(4).Padding(FMargin(2.0f, 8.0f, 0, 0))
                        [
                            SNew(STextBlock)
                                .TextStyle(&HUDStyle->TooltipProperty)
                                .Text(Ingredient2_NameAndAmount)
                        ]
                    ]
                ]
                + SVerticalBox::Slot().VAlign(VAlign_Top).MaxHeight(BrushIconMaxHeight).Padding(FMargin(0, 8.0f, 0, 0))
                [
                    SNew(SUniformGridPanel)
                    + SUniformGridPanel::Slot(0, 0)
                    [
                        SNew(SHorizontalBox)
                        + SHorizontalBox::Slot().FillWidth(1)
                        [
                            SNew(SBorder).BorderImage(&HUDStyle->TooltipIconHightlightBorder)
                            [
                                SNew(SImage).Image(Ingredient3_SB)
                            ]
                        ]
                        + SHorizontalBox::Slot().FillWidth(4).Padding(FMargin(2.0f, 8.0f, 0, 0))
                        [
                            SNew(STextBlock)
                                .TextStyle(&HUDStyle->TooltipProperty)
                                .Text(Ingredient3_NameAndAmount)
                        ]
                    ]
                    + SUniformGridPanel::Slot(1, 0)
                    [
                        SNew(SHorizontalBox)
                        + SHorizontalBox::Slot().FillWidth(1)
                        [
                            SNew(SBorder).BorderImage(&HUDStyle->TooltipIconHightlightBorder)
                            [
                                SNew(SImage).Image(Ingredient4_SB)
                            ]
                        ]
                        + SHorizontalBox::Slot().FillWidth(4).Padding(FMargin(2.0f, 8.0f, 0, 0))
                        [
                            SNew(STextBlock)
                                .TextStyle(&HUDStyle->TooltipProperty)
                                .Text(Ingredient4_NameAndAmount)
                        ]
                    ]
                ]
                + SVerticalBox::Slot().VAlign(VAlign_Top).MaxHeight(BrushIconMaxHeight).Padding(FMargin(0, 8.0f, 0, 0))
                [
                    SNew(SUniformGridPanel)
                    + SUniformGridPanel::Slot(0, 0)
                    [
                        SNew(SHorizontalBox)
                        + SHorizontalBox::Slot().FillWidth(1)
                        [
                            SNew(SBorder).BorderImage(&HUDStyle->TooltipIconHightlightBorder)
                            [
                                SNew(SImage).Image(Ingredient5_SB)
                            ]
                        ]
                        + SHorizontalBox::Slot().FillWidth(4).Padding(FMargin(2.0f, 8.0f, 0, 0))
                        [
                            SNew(STextBlock)
                                .TextStyle(&HUDStyle->TooltipProperty)
                                .Text(Ingredient5_NameAndAmount)
                        ]
                    ]
                    + SUniformGridPanel::Slot(1, 0)
                    [
                        SNew(SHorizontalBox)
                        + SHorizontalBox::Slot().FillWidth(1)
                        [
                            SNew(SBorder).BorderImage(&HUDStyle->TooltipIconHightlightBorder)
                            [
                                SNew(SImage).Image(Ingredient6_SB)
                            ]
                        ]
                        + SHorizontalBox::Slot().FillWidth(4).Padding(FMargin(2.0f, 8.0f, 0, 0))
                        [
                            SNew(STextBlock)
                                .TextStyle(&HUDStyle->TooltipProperty)
                                .Text(Ingredient6_NameAndAmount)
                        ]
                    ]
                ]
            ]
        ]
    ];

    SetVisibility(EVisibility::Collapsed);
}

void SGameUI_CraftingMenuTooltip::SetHoveredRecipe(TWeakObjectPtr<ABaseRecipe> NewRecipe) {
    if (HoveredRecipe == NewRecipe && GetVisibility() == EVisibility::Visible) {
        Hide();
    }
    else {
        SetVisibility(EVisibility::Visible);
        HoveredRecipe = NewRecipe;
    }
}

/** Delegates */
FText SGameUI_CraftingMenuTooltip::GetDisplayName() const {
    if (!HoveredRecipe.IsValid()) {
        return FText::FromString("");
    }
    else {
        return FText::FromString(HoveredRecipe->DisplayName);
    }
}

const FSlateBrush* SGameUI_CraftingMenuTooltip::GetObjectImage() const {
    if (HoveredRecipe == nullptr) {
        return &HUDStyle->Item_Empty;
    }
    else {
        return &HoveredRecipe->SlateBrush;
    }
}

void SGameUI_CraftingMenuTooltip::Hide() {
    SetVisibility(EVisibility::Collapsed);
}

/** Ingredient stuff */
/** 1- */
FText SGameUI_CraftingMenuTooltip::GetIngredient1_NameAndAmount() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 0 || HoveredRecipe->Ingredients[0].IngredientClass == nullptr) {
        return FText::FromString("");
    }

    FString ReturnString = HoveredRecipe->Ingredients[0].IngredientClass.GetDefaultObject()->ItemName;
    ReturnString.Append(": ");
    ReturnString.Append(FString::FromInt(HoveredRecipe->Ingredients[0].Amount));

    return FText::FromString(ReturnString);
}

const FSlateBrush* SGameUI_CraftingMenuTooltip::GetIngredient1_SB() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 0 || HoveredRecipe->Ingredients[0].IngredientClass == nullptr) {
        return &HUDStyle->Item_Empty;
    }

    return &HoveredRecipe->Ingredients[0].IngredientClass.GetDefaultObject()->SlateBrush;
}

/** 2- */
FText SGameUI_CraftingMenuTooltip::GetIngredient2_NameAndAmount() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 1 || HoveredRecipe->Ingredients[1].IngredientClass == nullptr) {
        return FText::FromString("");
    }

    FString ReturnString = HoveredRecipe->Ingredients[1].IngredientClass.GetDefaultObject()->ItemName;
    ReturnString.Append(": ");
    ReturnString.Append(FString::FromInt(HoveredRecipe->Ingredients[1].Amount));

    return FText::FromString(ReturnString);
}

const FSlateBrush* SGameUI_CraftingMenuTooltip::GetIngredient2_SB() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 1 || HoveredRecipe->Ingredients[1].IngredientClass == nullptr) {
        return &HUDStyle->Item_Empty;
    }

    return &HoveredRecipe->Ingredients[1].IngredientClass.GetDefaultObject()->SlateBrush;
}

/** 3- */
FText SGameUI_CraftingMenuTooltip::GetIngredient3_NameAndAmount() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 2 || HoveredRecipe->Ingredients[2].IngredientClass == nullptr) {
        return FText::FromString("");
    }

    FString ReturnString = HoveredRecipe->Ingredients[2].IngredientClass.GetDefaultObject()->ItemName;
    ReturnString.Append(": ");
    ReturnString.Append(FString::FromInt(HoveredRecipe->Ingredients[2].Amount));

    return FText::FromString(ReturnString);
}

const FSlateBrush* SGameUI_CraftingMenuTooltip::GetIngredient3_SB() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 2 || HoveredRecipe->Ingredients[2].IngredientClass == nullptr) {
        return &HUDStyle->Item_Empty;
    }

    return &HoveredRecipe->Ingredients[2].IngredientClass.GetDefaultObject()->SlateBrush;
}

/** 4- */
FText SGameUI_CraftingMenuTooltip::GetIngredient4_NameAndAmount() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 3 || HoveredRecipe->Ingredients[3].IngredientClass == nullptr) {
        return FText::FromString("");
    }

    FString ReturnString = HoveredRecipe->Ingredients[3].IngredientClass.GetDefaultObject()->ItemName;
    ReturnString.Append(": ");
    ReturnString.Append(FString::FromInt(HoveredRecipe->Ingredients[3].Amount));

    return FText::FromString(ReturnString);
}

const FSlateBrush* SGameUI_CraftingMenuTooltip::GetIngredient4_SB() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 3 || HoveredRecipe->Ingredients[3].IngredientClass == nullptr) {
        return &HUDStyle->Item_Empty;
    }

    return &HoveredRecipe->Ingredients[3].IngredientClass.GetDefaultObject()->SlateBrush;
}

/** 5- */
FText SGameUI_CraftingMenuTooltip::GetIngredient5_NameAndAmount() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 4 || HoveredRecipe->Ingredients[4].IngredientClass == nullptr) {
        return FText::FromString("");
    }

    FString ReturnString = HoveredRecipe->Ingredients[4].IngredientClass.GetDefaultObject()->ItemName;
    ReturnString.Append(": ");
    ReturnString.Append(FString::FromInt(HoveredRecipe->Ingredients[4].Amount));

    return FText::FromString(ReturnString);
}

const FSlateBrush* SGameUI_CraftingMenuTooltip::GetIngredient5_SB() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 4 || HoveredRecipe->Ingredients[4].IngredientClass == nullptr) {
        return &HUDStyle->Item_Empty;
    }

    return &HoveredRecipe->Ingredients[4].IngredientClass.GetDefaultObject()->SlateBrush;
}

/** 5- */
FText SGameUI_CraftingMenuTooltip::GetIngredient6_NameAndAmount() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 5 || HoveredRecipe->Ingredients[5].IngredientClass == nullptr) {
        return FText::FromString("");
    }

    FString ReturnString = HoveredRecipe->Ingredients[5].IngredientClass.GetDefaultObject()->ItemName;
    ReturnString.Append(": ");
    ReturnString.Append(FString::FromInt(HoveredRecipe->Ingredients[5].Amount));

    return FText::FromString(ReturnString);
}

const FSlateBrush* SGameUI_CraftingMenuTooltip::GetIngredient6_SB() const {
    if (HoveredRecipe == nullptr || HoveredRecipe->Ingredients.Num() <= 5 || HoveredRecipe->Ingredients[5].IngredientClass == nullptr) {
        return &HUDStyle->Item_Empty;
    }

    return &HoveredRecipe->Ingredients[5].IngredientClass.GetDefaultObject()->SlateBrush;
}
