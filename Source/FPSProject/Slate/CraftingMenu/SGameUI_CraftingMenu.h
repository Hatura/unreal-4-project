#pragma once

#include "Slate.h"
#include "Slate/MovableWidget.h"
#include "../SGameUI.h"
#include "SGameUI_CraftingMenuTooltip.h"
#include "FPSPlayerController.h"

class SGameUI_CraftingMenu : public SMovableWidget {
public:
    SLATE_BEGIN_ARGS(SGameUI_CraftingMenu)
        : _GameHUD(),
          _PCOwner(),
          _SGameUIInstance()
    {
    }

    SLATE_ARGUMENT(TWeakObjectPtr<class AGameHUD>, GameHUD)
    SLATE_ARGUMENT(TWeakObjectPtr<class AFPSPlayerController>, PCOwner)
    SLATE_ARGUMENT(SGameUI*, SGameUIInstance)

    SLATE_END_ARGS()

    void Construct(const FArguments& args);

    void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

    void OnMouseLeave(const FPointerEvent& MouseEvent);

    TSharedRef<ITableRow> InsertRecipeListEntry(TSharedPtr<FKnownRecipeEntry> KnownRecipeEntry, const TSharedRef<STableViewBase>& OwnerTable);

    //void RecipeSelected(TSharedPtr<FKnownRecipeEntry> SelectedRecipe, ESelectInfo::Type SelectInfo);

    TArray<TSharedPtr<FKnownRecipeEntry>> RecipeBuffer;

    // The hover/selection tooltip
    TSharedPtr<class SGameUI_CraftingMenuTooltip> CraftingTooltip;

    void SetVisibility(EVisibility visibility);

    void UpdateBufferArray();

private:
    TSharedPtr<SScrollBox> ScrollBox;
    TSharedPtr<SListView<TSharedPtr<FKnownRecipeEntry>>> LiveRecipeList;

    TWeakObjectPtr<class AGameHUD> GameHUD;
    TWeakObjectPtr<class AFPSPlayerController> PCOwner;
    SGameUI* SGameUIInstance;

    float LastUpdateTime;

    const struct FGlobalGameHUDStyle_Struct* HUDStyle;
};
