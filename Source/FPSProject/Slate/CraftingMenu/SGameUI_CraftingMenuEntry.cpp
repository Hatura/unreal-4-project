#include "FPSProject.h"

#include "../GlobalGameHUDStyle.h"
#include "../MenuStyles.h"
#include "FPSCharacter.h"
#include "SGameUI_CraftingMenuEntry.h"

void SGameUI_CraftingMenuEntry::Construct(const FArguments &args) {
    GameHUD = args._GameHUD;
    PCOwner = args._PCOwner;
    CraftingMenu = args._CraftingMenu;
    Recipe = args._Recipe;

    HUDStyle = &FMenuStyles::Get().GetWidgetStyle<FGlobalGameHUDStyle_Struct>("Global_GameHUD");

    SlateBrush.Bind(this, &SGameUI_CraftingMenuEntry::GetSlateBrush);
    DisplayName.Bind(this, &SGameUI_CraftingMenuEntry::GetDisplayName);
    ObjectType.Bind(this, &SGameUI_CraftingMenuEntry::GetObjectType);
    CraftingTime.Bind(this, &SGameUI_CraftingMenuEntry::GetCraftingTime);
    CraftingButtonColor.Bind(this, &SGameUI_CraftingMenuEntry::GetCraftingButtonColor);

    TSharedPtr<SButton> CraftButton;

    ChildSlot.VAlign(VAlign_Fill).HAlign(HAlign_Fill)
    [
        SNew(SBorder).BorderImage(&HUDStyle->CraftingMenuBorderStyle)
        [
            SNew(SBox)
            .HAlign(HAlign_Center)
            .VAlign(VAlign_Center)
            .WidthOverride(244)
            .HeightOverride(52)
            [
                SNew(SHorizontalBox)
                + SHorizontalBox::Slot().AutoWidth()
                [
                    SNew(SBox)
                    .WidthOverride(48)
                    .HeightOverride(48)
                    .Padding(2)
                    [
                        SNew(SImage).Image(SlateBrush)
                    ]
                ]
                + SHorizontalBox::Slot().AutoWidth()
                [
                    SNew(SBox)
                    .WidthOverride(148)
                    [
                        SNew(SVerticalBox)
                        + SVerticalBox::Slot().FillHeight(1).HAlign(HAlign_Center)
                        [
                            SNew(STextBlock)
                            .TextStyle(&HUDStyle->DisplayNameStyle)
                            .Text(DisplayName)
                        ]
                        + SVerticalBox::Slot().FillHeight(1).VAlign(VAlign_Bottom).HAlign(HAlign_Center)
                        [
                            SNew(SHorizontalBox)
                            + SHorizontalBox::Slot().AutoWidth().Padding(FMargin(0, 0, 24, 0))
                            [
                                SNew(STextBlock)
                                .TextStyle(&HUDStyle->TypeAndTimeStyle)
                                .Text(ObjectType)
                            ]
                            + SHorizontalBox::Slot().AutoWidth().Padding(FMargin(24, 0, 0, 0))
                            [
                                SNew(STextBlock)
                                .TextStyle(&HUDStyle->TypeAndTimeStyle)
                                .Text(CraftingTime)
                            ]
                        ]
                    ]
                ]
                + SHorizontalBox::Slot().AutoWidth()
                [
                    SNew(SBox)
                    .WidthOverride(48)
                    .HeightOverride(48)
                    .Padding(2)
                    [
                        SNew(SBorder).BorderImage(&HUDStyle->CraftingMenuCraftButtonBG)
                        [
                            SAssignNew(CraftButton, SButton)
                            .ButtonStyle(&HUDStyle->CraftingMenuCraftButton)
                            .ButtonColorAndOpacity(CraftingButtonColor)
                            .ClickMethod(EButtonClickMethod::MouseDown)
                            .OnClicked(this, &SGameUI_CraftingMenuEntry::TryCrafting)
                        ]
                    ]
                ]
            ]
        ]
    ];

    if (Recipe) {
        if (Recipe->CreatesObjectType == ECreatesObjectType::OBJECT_ITEM) {
            CraftButton->SetButtonStyle(&HUDStyle->CraftingMenuCraftButton);
        }
        else if (Recipe->CreatesObjectType == ECreatesObjectType::OBJECT_BUILDING) {
            CraftButton->SetButtonStyle(&HUDStyle->CraftingMenuCraftButtonBuilding);
        }
    }
}

void SGameUI_CraftingMenuEntry::OnMouseEnter(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    UE_LOG(LogTemp, Log, TEXT("Mouse over me"));
//    if (!CraftingTooltip.IsValid()) {
////        SGameUI_CraftingMenuTooltip::FArguments arguments = SGameUI_CraftingMenuTooltip::FArguments();
////        arguments.HoveredRecipe(Recipe);
////        arguments.PCOwner(PCOwner);
////        arguments.GameHUD(GameHUD);
////        CraftingTooltip = MakeShareable(new SGameUI_CraftingMenuTooltip);
////        CraftingTooltip->Construct(arguments);
//    }

//    if (CraftingMenu == nullptr) {
//        UE_LOG(LogTemp, Error, TEXT("!CraftingMenu"));
//    }
//    else if (!CraftingMenu->CraftingTooltip.IsValid()) {
//        UE_LOG(LogTemp, Error, TEXT("!Tooltip"));
//    }

//    // Update recipe on Tooltip
//    if (CraftingMenu != nullptr && CraftingMenu->CraftingTooltip.IsValid()) {
//        CraftingMenu->CraftingTooltip->SetHoveredRecipe(Recipe);
//    }
}

FReply SGameUI_CraftingMenuEntry::OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    if (CraftingMenu == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("!CraftingMenu"));
    }
    else if (!CraftingMenu->CraftingTooltip.IsValid()) {
        UE_LOG(LogTemp, Error, TEXT("!Tooltip"));
    }

    // Update recipe on Tooltip
    if (CraftingMenu != nullptr && CraftingMenu->CraftingTooltip.IsValid()) {
        CraftingMenu->CraftingTooltip->SetHoveredRecipe(Recipe);
    }

    return FReply::Handled();
}

FReply SGameUI_CraftingMenuEntry::TryCrafting() {
    UE_LOG(LogTemp, Log, TEXT("-> TryCrafting()"));

    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(PCOwner->GetPawn());

    if (ActualCharacter && ActualCharacter->CraftingManager) {
        ActualCharacter->CraftingManager->StartCrafting(Recipe);
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("TryCrafting(): No Character or CraftingManager"));
    }

    return FReply::Handled();
}

const FSlateBrush* SGameUI_CraftingMenuEntry::GetSlateBrush() const {
    if (!PCOwner->MyRMInstance) {
        UE_LOG(LogTemp, Warning, TEXT("!MyRMInstance"));
    }
    else if (!PCOwner->MyRMInstance->IsRMInitialized) {
        UE_LOG(LogTemp, Warning, TEXT("!IsRMInitialized"));
    }
    else if (Recipe == nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("nullptr"));
    }
    else {
        return &Recipe->SlateBrush;
    }

    return &HUDStyle->InventoryBackground;
}

FText SGameUI_CraftingMenuEntry::GetDisplayName() const {
    if (!PCOwner->MyRMInstance || !PCOwner->MyRMInstance->IsRMInitialized || Recipe == nullptr) {
        return FText::FromString("");
    }
    else {
        return FText::FromString(Recipe->DisplayName);
    }
}

FText SGameUI_CraftingMenuEntry::GetObjectType() const {
    if (!PCOwner->MyRMInstance || !PCOwner->MyRMInstance->IsRMInitialized || Recipe == nullptr) {
        return FText::FromString("");
    }
    else {
        if (Recipe->CreatesObjectType == ECreatesObjectType::OBJECT_BUILDING) {
            return FText::FromString("Building");
        }
        else {
            return FText::FromString("Item");
        }
    }
}

FText SGameUI_CraftingMenuEntry::GetCraftingTime() const {
    if (!PCOwner->MyRMInstance || !PCOwner->MyRMInstance->IsRMInitialized || Recipe == nullptr) {
        return FText::FromString("");
    }
    else {
        if (Recipe->CreatesObjectType == ECreatesObjectType::OBJECT_ITEM) {
            return FText::FromString(Recipe->GetTotalCraftingTimeString());
        }
        else {
            return FText::FromString("");
        }
    }
}

FSlateColor SGameUI_CraftingMenuEntry::GetCraftingButtonColor() const {
    if (!Recipe) {
        return FSlateColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
    }

    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(PCOwner->GetPawn());

    if (ActualCharacter && ActualCharacter->CraftingManager) {
        if (Recipe && Recipe->CreatesObjectType == ECreatesObjectType::OBJECT_BUILDING) {
            // always white for buildingicon
            return FSlateColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
        }

        bool ResAvailability = ActualCharacter->CraftingManager->AreResourcesAvailable(Recipe);
        if (ResAvailability) {
            // green
            return FSlateColor(FLinearColor(0.1f, 1.0f, 0, 0.65f));
        }
    }

    // red
    return FSlateColor(FLinearColor(1.0f, 0, 0, 0.65f));
}
