#pragma once
#include "Slate.h"
#include "Crafting/BaseRecipe.h"
#include "FPSPlayerController.h"

class SGameUI_CraftingMenuTooltip : public SCompoundWidget
{
public:
    SLATE_BEGIN_ARGS(SGameUI_CraftingMenuTooltip)
        : _GameHUD(),
          _PCOwner(),
          _HoveredRecipe()
    {
    }

    SLATE_ARGUMENT(TWeakObjectPtr<class AGameHUD>, GameHUD)
    SLATE_ARGUMENT(TWeakObjectPtr<class AFPSPlayerController>, PCOwner)
    SLATE_ARGUMENT(TWeakObjectPtr<class ABaseRecipe>, HoveredRecipe)

    SLATE_END_ARGS()

    void Construct(const FArguments& args);

    // Make visible and set Recipe
    void SetHoveredRecipe(TWeakObjectPtr<class ABaseRecipe> NewRecipe);

    // Hide the tooltip
    void Hide();

private:
    TWeakObjectPtr<class AGameHUD> GameHUD;
    TWeakObjectPtr<class AFPSPlayerController> PCOwner;
    TWeakObjectPtr<class ABaseRecipe> HoveredRecipe;

    // Title
    TAttribute<FText> DisplayName;
    FText GetDisplayName() const;

    // Title image
    TAttribute<const FSlateBrush*> ObjectImage;
    const FSlateBrush* GetObjectImage() const;

    // Ingredients, is there a better way?

    /** 1- */
    TAttribute<const FSlateBrush*> Ingredient1_SB;
    const FSlateBrush* GetIngredient1_SB() const;

    TAttribute<FText> Ingredient1_NameAndAmount;
    FText GetIngredient1_NameAndAmount() const;

    /** 2- */
    TAttribute<const FSlateBrush*> Ingredient2_SB;
    const FSlateBrush* GetIngredient2_SB() const;

    TAttribute<FText> Ingredient2_NameAndAmount;
    FText GetIngredient2_NameAndAmount() const;

    /** 3- */
    TAttribute<const FSlateBrush*> Ingredient3_SB;
    const FSlateBrush* GetIngredient3_SB() const;

    TAttribute<FText> Ingredient3_NameAndAmount;
    FText GetIngredient3_NameAndAmount() const;

    /** 4- */
    TAttribute<const FSlateBrush*> Ingredient4_SB;
    const FSlateBrush* GetIngredient4_SB() const;

    TAttribute<FText> Ingredient4_NameAndAmount;
    FText GetIngredient4_NameAndAmount() const;

    /** 5- */
    TAttribute<const FSlateBrush*> Ingredient5_SB;
    const FSlateBrush* GetIngredient5_SB() const;

    TAttribute<FText> Ingredient5_NameAndAmount;
    FText GetIngredient5_NameAndAmount() const;

    /** 6- */
    TAttribute<const FSlateBrush*> Ingredient6_SB;
    const FSlateBrush* GetIngredient6_SB() const;

    TAttribute<FText> Ingredient6_NameAndAmount;
    FText GetIngredient6_NameAndAmount() const;

    const struct FGlobalGameHUDStyle_Struct* HUDStyle;
};
