#include "FPSProject.h"

#include "Slate/GlobalGameHUDStyle.h"
#include "Slate/MenuStyles.h"
#include "FPSPlayerController.h"
#include "FPSCharacter.h"
#include "Crafting/CraftingManager.h"
#include "SGameUI_CraftingProgressBar.h"

void SGameUI_CraftingProgressBar::Construct(const FArguments& InArgs)
{
    PCOwner = InArgs._PCOwner;

    HUDStyle = &FMenuStyles::Get().GetWidgetStyle<FGlobalGameHUDStyle_Struct>("Global_GameHUD");

    CraftingPercent = 0;

    ChildSlot
    [
        SNew(SBox).WidthOverride(100) [
            SNew(SProgressBar)
            .Percent(this, &SGameUI_CraftingProgressBar::GetCraftingPercent)
            .Style(&HUDStyle->CraftingProgressBarStyle)
        ]
    ];
}

TOptional<float> SGameUI_CraftingProgressBar::GetCraftingPercent() const {
    if (!PCOwner.IsValid()) {
        return 0;
    }

    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(PCOwner->GetPawn());

    if (!ActualCharacter || !ActualCharacter->IsAlive) {
        return 0;
    }

    if (!ActualCharacter->CraftingManager) {
        return 0;
    }

    return ActualCharacter->CraftingManager->GetItemCraftingPercent();

}

void SGameUI_CraftingProgressBar::Tick(const FGeometry &AllottedGeometry, const double InCurrentTime, const float InDeltaTime) {
    SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

    if (Visibility != EVisibility::Collapsed && !IsCraftingItem()) {
        Visibility = EVisibility::Collapsed;
    }
    else if (Visibility == EVisibility::Collapsed && IsCraftingItem()) {
        Visibility = EVisibility::Visible;
    }
}

bool SGameUI_CraftingProgressBar::IsCraftingItem() {
    if (!PCOwner.IsValid()) {
        return false;
    }

    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(PCOwner->GetPawn());

    if (!ActualCharacter || !ActualCharacter->IsAlive) {
        return false;
    }

    if (!ActualCharacter->CraftingManager) {
        return false;
    }

    if (ActualCharacter->CraftingManager->IsCraftingItem()) {
        return true;
    }

    return false;
}
