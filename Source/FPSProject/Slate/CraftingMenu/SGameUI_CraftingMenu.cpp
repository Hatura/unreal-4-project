#include "FPSProject.h"

#include "../GlobalGameHUDStyle.h"
#include "../MenuStyles.h"
#include "FPSCharacter.h"
#include "SGameUI_CraftingMenuEntry.h"
#include "SGameUI_CraftingMenu.h"

void SGameUI_CraftingMenu::Construct(const FArguments &args) {
    GameHUD = args._GameHUD;
    PCOwner = args._PCOwner;
    SGameUIInstance = args._SGameUIInstance;

    HUDStyle = &FMenuStyles::Get().GetWidgetStyle<FGlobalGameHUDStyle_Struct>("Global_GameHUD");

    LastUpdateTime = 0;

    ChildSlot
    [
        SAssignNew(WidgetContent, SOverlay)
        + SOverlay::Slot()
        [
            SNew(SVerticalBox)
            + SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 0, 0, 4.0f))
            [
                SNew(SHorizontalBox)
                + SHorizontalBox::Slot().MaxWidth(20.0f)
                [
                    SNew(SButton)
                    .ButtonStyle(&HUDStyle->MinimizeButton)
                    .ClickMethod(EButtonClickMethod::MouseDown)
                    .OnClicked(this, &SMovableWidget::MinimizeButtonClicked)
                ]
                + SHorizontalBox::Slot().MaxWidth(140.0f).Padding(FMargin(4.0f, 0, 0, 0))
                [
                    SAssignNew(DragInitializer, SDragInitializer).BorderImage(&HUDStyle->TitleBarBorder)
                    [
                        SNew(STextBlock)
                        .TextStyle(&HUDStyle->TitleBarText)
                        .Text(NSLOCTEXT("HaturLoc", "CraftingMenuTitlebarText", "Crafting Menu"))
                    ]
                ]
            ]
            + SVerticalBox::Slot()
            [
                SNew(SHorizontalBox)
                + SHorizontalBox::Slot().AutoWidth()
                [
                    SNew(SVerticalBox)
                    + SVerticalBox::Slot().MaxHeight(240)
                    [
                        SAssignNew(LiveRecipeList, SListView<TSharedPtr<FKnownRecipeEntry>>)
                        .ListItemsSource(&RecipeBuffer)
                        .OnGenerateRow(this, &SGameUI_CraftingMenu::InsertRecipeListEntry)
                        .SelectionMode(ESelectionMode::Single)
                        //.OnSelectionChanged(this, &SGameUI_CraftingMenu::RecipeSelected)
                    ]
                ]
                + SHorizontalBox::Slot().Padding(FMargin(12.0f, 0, 0, 0))
                [
                    SNew(SOverlay)
                    +SOverlay::Slot().VAlign(VAlign_Top)
                    [
                         SAssignNew(CraftingTooltip, SGameUI_CraftingMenuTooltip).PCOwner(PCOwner).GameHUD(GameHUD)
                    ]
                ]
            ]
        ]
    ];

//    ChildSlot.VAlign(VAlign_Fill).HAlign(HAlign_Fill)
//    [
//        SNew(SCanvas)
//        + SCanvas::Slot().Position(TAttribute<FVector2D>(this, &SMovableWidget::GetMovableWidgetPos)).Size(TAttribute<FVector2D>(this, &SMovableWidget::GetContentSize))//Padding(TAttribute<FMargin>(this, &SMovableWidget::GetMovableWidgetMargin))
//        [

//        ]
//    ];

    SetInitialPosition(500, 500);

    DragInitializer->LinkWidget(this);

    SetVisibility(EVisibility::Collapsed);
}

void SGameUI_CraftingMenu::Tick(const FGeometry &AllottedGeometry, const double InCurrentTime, const float InDeltaTime) {
    SMovableWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

    if (PCOwner->GetWorld() && PCOwner->MyRMInstance && PCOwner->MyRMInstance->IsRMInitialized && PCOwner->MyRMInstance->SlateNeedsUpdate) {
        float Now = PCOwner->GetWorld()->TimeSeconds;
        if (Now > LastUpdateTime + 0.25f) {
            UpdateBufferArray();
//            FGeometry RecipeGeometry = FindChildGeometry(AllottedGeometry, LiveRecipeList.ToSharedRef());
//            LiveRecipeList->ReGenerateItems(RecipeGeometry);
            LiveRecipeList->RequestListRefresh();
            LiveRecipeList->UpdateSelectionSet();
            LastUpdateTime = Now;
        }
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("Trying to update CraftingMenu but got no World!"));
    }
}

void SGameUI_CraftingMenu::UpdateBufferArray() {
    if (PCOwner->MyRMInstance && PCOwner->MyRMInstance->IsRMInitialized) {
        RecipeBuffer.Reset();

        for (TSharedPtr<FKnownRecipeEntry> RecipeEntry : PCOwner->MyRMInstance->KnownRecipes) {
            RecipeBuffer.Add(RecipeEntry);
        }
//        switch (StoredInteractionObject) {
//        case EInteractableWorldObject::NONE:
//            for (TSharedPtr<FKnownRecipeEntry> RecipeEntry : PCOwner->MyRMInstance->KnownRecipes) {
//                if (RecipeEntry->Recipe && RecipeEntry->Recipe->IsAlwaysCraftable) {
//                    RecipeBuffer.Add(RecipeEntry);
//                }
//            }
//            break;
//        case EInteractableWorldObject::CRAFTINGSTATION_SIMPLE:
//            for (TSharedPtr<FKnownRecipeEntry> RecipeEntry : PCOwner->MyRMInstance->KnownRecipes) {
//                if (RecipeEntry->Recipe && !RecipeEntry->Recipe->IsAlwaysCraftable) {
//                    RecipeBuffer.Add(RecipeEntry);
//                }
//            }
//            break;
//        }
    }
    else {
        UE_LOG(LogTemp, Log, TEXT("UpdaetBufferArray fehler"));
    }
}

TSharedRef<ITableRow> SGameUI_CraftingMenu::InsertRecipeListEntry(TSharedPtr<FKnownRecipeEntry> KnownRecipeEntry, const TSharedRef<STableViewBase> &OwnerTable) {
    int32 SearchIndex = PCOwner->MyRMInstance->KnownRecipes.IndexOfByKey(KnownRecipeEntry);

    UE_LOG(LogTemp, Log, TEXT("LRList enter"));

    check(KnownRecipeEntry.IsValid());

    if (!KnownRecipeEntry.IsValid()) {
        UE_LOG(LogTemp, Log, TEXT("Invalid Table thing"));
    }

    UE_LOG(LogTemp, Log, TEXT("LRList valid"));

    return SNew(STableRow<TSharedPtr<FKnownRecipeEntry>>, OwnerTable)
    [
        SNew(SGameUI_CraftingMenuEntry).GameHUD(GameHUD).PCOwner(PCOwner).Recipe(KnownRecipeEntry->Recipe).CraftingMenu(this)
    ];
}

// This is only for right click !?
//void SGameUI_CraftingMenu::RecipeSelected(TSharedPtr<FKnownRecipeEntry> SelectedRecipe, ESelectInfo::Type SelectInfo) {
//    UE_LOG(LogTemp, Log, TEXT("Selecto!"));
//}

void SGameUI_CraftingMenu::OnMouseLeave(const FPointerEvent &MouseEvent) {
//    if (CraftingTooltip.IsValid()) {
//        CraftingTooltip->Hide();
//    }
}

void SGameUI_CraftingMenu::SetVisibility(EVisibility visibility) {
    SGameUI_CraftingMenu::Visibility = visibility;
}
