#pragma once

#include "Slate.h"

/**
 * 
 */
class SGameUI_CraftingProgressBar : public SCompoundWidget
{
public:
    SLATE_BEGIN_ARGS(SGameUI_CraftingProgressBar)
        : _PCOwner()
	{}
    SLATE_ARGUMENT(TWeakObjectPtr<class AFPSPlayerController>, PCOwner)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

    void Tick(const FGeometry &AllottedGeometry, const double InCurrentTime, const float InDeltaTime);

private:
    const struct FGlobalGameHUDStyle_Struct* HUDStyle;

    TWeakObjectPtr<class AFPSPlayerController> PCOwner;

    float CraftingPercent;

    TOptional<float> GetCraftingPercent() const;

    bool IsCraftingItem();
};
