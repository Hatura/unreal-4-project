#include "FPSProject.h"
#include "LayoutUtils.h"
#include "Slate/SGameUI.h"
#include "MovableWidget.h"

// +++++++++++++
// SAutoCanvas
// +++++++++++++

//FVector2D SAutoCanvas::ComputeDesiredSize() const {
//    FVector2D MaxSize(0, 0);
//    for (int32 ChildIndex=0; ChildIndex < Children.Num(); ++ChildIndex) {
//        const FOverlaySlot& CurSlot = Children[ChildIndex];
//        const EVisibility ChildVisibilty = CurSlot.Widget->GetVisibility();
//        if (ChildVisibilty != EVisibility::Collapsed) {
//            FVector2D ChildDesiredSize = CurSlot.Widget->GetDesiredSize() + CurSlot.SlotPadding.Get().GetDesiredSize();
//            MaxSize.X = FMath::Max( MaxSize.X, ChildDesiredSize.X );
//            MaxSize.Y = FMath::Max( MaxSize.Y, ChildDesiredSize.Y );
//        }
//    }

//    return MaxSize;
//}

// +++++++++++++
// SMovableWidget
// +++++++++++++

void SMovableWidget::SetInitialPosition(int32 PosX, int32 PosY) {
    DragPosX = PosX;
    DragPosY = PosY;
}

void SMovableWidget::UpdateLocation(int32 PosX, int32 PosY) {
    DragPosX = PosX;
    DragPosY = PosY;
}

FReply SMovableWidget::MinimizeButtonClicked() {
    SetVisibility(EVisibility::Collapsed);

    return FReply::Handled();
}

FVector2D SMovableWidget::GetMovableWidgetPos() const {
    return FVector2D(DragPosX, DragPosY);
}

FVector2D SMovableWidget::GetContentSize() const {
    if (WidgetContent.IsValid()) {
        return WidgetContent->GetDesiredSize();
    }

    UE_LOG(LogTemp, Error, TEXT("WidgetContent invalid"));
    return FVector2D(0, 0);
}

// +++++++++++++
// SDragInitializer
// +++++++++++++

void SDragInitializer::LinkWidget(SMovableWidget* MovableWidgetLink) {
    MovableWidget = MovableWidgetLink;
}

FReply SDragInitializer::OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    if (MovableWidget != nullptr) {
        return FReply::Handled().DetectDrag(SharedThis(this), EKeys::LeftMouseButton);
    }

    return FReply::Unhandled();
}

FReply SDragInitializer::OnDragDetected(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    if (MovableWidget != nullptr) {
        if (MouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton)) {
            TSharedRef<FMovableWidgetDrag> Operation = FMovableWidgetDrag::New();
            Operation->LinkWidget(MovableWidget);

            return FReply::Handled().BeginDragDrop(Operation);
        }
    }

    return FReply::Unhandled();
}

// +++++++++++++
// FMovableWindowDrag
// +++++++++++++

TSharedRef<FMovableWidgetDrag> FMovableWidgetDrag::New() {
    TSharedRef<FMovableWidgetDrag> Operation = MakeShareable(new FMovableWidgetDrag);

    Operation->Construct();

    return Operation;
}

void FMovableWidgetDrag::LinkWidget(SMovableWidget* MovableWidgetLink) {
    MovableWidget = MovableWidgetLink;
}

void FMovableWidgetDrag::OnDragged(const FDragDropEvent &DragDropEvent) {
    if (MovableWidget == nullptr) {
         UE_LOG(LogTemp, Error, TEXT("FMovableWidgetDrag::OnDragged(): MovableWidget is nullptr"));
    }

    FWidgetPath MyWidgetPath;
    FSlateApplication::Get().GeneratePathToWidgetChecked(MovableWidget->AsShared(), MyWidgetPath);

    FVector2D vec = MyWidgetPath.GetWindow()->GetWindowGeometryInScreen().AbsoluteToLocal(FVector2D(DragDropEvent.GetScreenSpacePosition().X, DragDropEvent.GetScreenSpacePosition().Y));

    int TitleBarSizeInt = 0;

    FOptionalSize TitleBarSize = MyWidgetPath.GetWindow()->GetTitleBarSize();

    if (TitleBarSize.IsSet()) {
        TitleBarSizeInt = FPlatformMath::FloorToInt(TitleBarSize.Get());
    }

    MovableWidget->UpdateLocation(FPlatformMath::FloorToInt(vec.X - 5), FPlatformMath::FloorToInt(vec.Y - 5 - TitleBarSizeInt));
}
