/**
 * This is the ingame Action-HUD
 */

#pragma once

#include "Slate.h"
#include "Inventory/SGameUI_InventoryOverlay.h"
#include "Inventory/SGameUI_InventoryContentBox.h"
#include "Inventory/SGameUI_CharacterPanelOverlay.h"
#include "Slate/CraftingMenu/SGameUI_CraftingProgressBar.h"
#include "CraftingMenu/SGameUI_CraftingMenu.h"

class SCustomDecorator : public SImage {
};

class SGameUI : public SCompoundWidget {
public:
    SLATE_BEGIN_ARGS(SGameUI)
        : _GameHUD()
    {
    }

    SLATE_ARGUMENT(TWeakObjectPtr<class AGameHUD>, GameHUD)

    SLATE_END_ARGS()

    // For Cursor Decorator, filled with Cursor position externally
    static int32 MouseX;
    static int32 MouseY;

    void Construct(const FArguments& args);

    void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

    bool SupportsKeyboardFocus() const override { return true; }

//    FReply OnDrop(const FGeometry &MyGeometry, const FDragDropEvent &DragDropEvent) override;

    FReply OnKeyboardFocusReceived(const FGeometry &MyGeometry, const FKeyboardFocusEvent &InKeyboardFocusEvent) override;

    TSharedPtr<SCanvas> OverlayCanvas;

    TSharedPtr<class SGameUI_InventoryOverlay> InventoryOverlay;

    TSharedPtr<class SGameUI_CharacterPanelOverlay> CharacterPanelOverlay;

    TSharedPtr<class SMovableWidget> CraftingMenuOverlay;

    TSharedPtr<class SGameUI_CraftingProgressBar> CraftingProgressBar;

    static TSharedPtr<class SCustomDecorator> DragDropDecorator;

    TArray<TSharedPtr<class SGameUI_InventoryContentBox>> ContentBoxArray;

    static void SetDragDropStart(const FSlateBrush* DragBrush);

    static void SetDragDropFinished();

    const struct FGlobalGameHUDStyle_Struct* HUDStyle;
protected:

private:
    TWeakObjectPtr<class AGameHUD> GameHUD;

    AFPSPlayerController * PCOwner;

    // this is shitty, find a better way
    FVector2D GetCraftingMenuPos() const;
    FVector2D GetCraftingMenuSize() const;

    // this is shitty, find a better way
    FVector2D GetInventoryOverlayPos() const;
    FVector2D GetInventoryOverlaySize() const;

    // this is shitty, find a better way
    FVector2D GetCharacterPanelPos() const;
    FVector2D GetCharacterPanelSize() const;

    FVector2D GetCraftingProgressBarPos() const;
    FVector2D GetCraftingProgressBarSize() const;

    // ++ This is both for the Cursor decorator, it's a workaround because the default cursordecorator of FDragDropOperation is a window
    // which is A. Invisible on video capturing (cause it is a new window) and B. ignores alpha value of the brush?
    TAttribute<const FSlateBrush*> DragDropDelegate;
    const FSlateBrush* GetDragDropBrush() const;

    TAttribute<FMargin> DecoratorPosition;
    FMargin GetDecoratorPosition() const;

    static const FSlateBrush* DragDropBrush;
    // -- Decorator

    bool CraftingMenuAdded;
};

class FItemDragDrop : public FDragDropOperation {
public:
    DRAG_DROP_OPERATOR_TYPE(FItemDragDrop, FDragDropOperation)

    // TODO: It's certainly not the best idea to work with statics!

    void OnDrop(bool bDropWasHandled, const FPointerEvent &MouseEvent) override;

    void OnDragged(const class FDragDropEvent& DragDropEvent) override;

    // Sets the root widget to get the window, needs to be called in WidgetConstructor!
    static void LinkWidget(SCompoundWidget *WidgetToLink);

    static SCompoundWidget* LinkedWidget;

    //virtual TSharedPtr<SWidget> GetDefaultDecorator() const override;

    static TSharedRef<FItemDragDrop> New(int32 StartingField, const FSlateBrush* DragBrush);

    int32 StartingField;

    // This is a memory leak?
    const FSlateBrush* DragBrush;
};
