#include "FPSProject.h"
#include "GlobalGameHUDStyle.h"

void FGlobalGameHUDStyle_Struct::GetResources(TArray<const FSlateBrush *> &OutBrushes) const {
    OutBrushes.Empty();
}

const FName FGlobalGameHUDStyle_Struct::TypeName = TEXT("FGlobalGameHUDStyle_Struct");

// This returned name HAS TO MATCH THE STRUCTS STYLE NAME!
const FName FGlobalGameHUDStyle_Struct::GetTypeName() const {
    static const FName TypeName = TEXT("FGlobalGameHUDStyle_Struct");
    return TypeName;
}

const FGlobalGameHUDStyle_Struct& FGlobalGameHUDStyle_Struct::GetDefault() {
    static FGlobalGameHUDStyle_Struct Default;
    return Default;
}

UGlobalGameHUDStyle::UGlobalGameHUDStyle(const class FPostConstructInitializeProperties& PCIP)
    : Super(PCIP) {

}
const struct FSlateWidgetStyle * const UGlobalGameHUDStyle::GetStyle() const {
    return static_cast<const struct FSlateWidgetStyle*>(&GameHUDStyle);
}
