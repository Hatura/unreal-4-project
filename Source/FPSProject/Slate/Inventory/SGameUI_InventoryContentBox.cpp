#include "FPSProject.h"

#include "../GlobalGameHUDStyle.h"
#include "../MenuStyles.h"
#include "FPSCharacter.h"
#include "SGameUI_InventoryContentBox.h"

void SGameUI_InventoryContentBox::Construct(const FArguments &args) {
    GameHUD = args._GameHUD;
    PCOwner = args._PCOwner;
    FieldID = args._FieldID.Get();
    IsHotbar = args._IsHotbar.Get();

    IsMouseOverMe = false;

    HUDStyle = &FMenuStyles::Get().GetWidgetStyle<FGlobalGameHUDStyle_Struct>("Global_GameHUD");

    BackgroundBrush.Bind(this, &SGameUI_InventoryContentBox::GetBackgroundBrush);
    RarityBrush.Bind(this, &SGameUI_InventoryContentBox::GetRarityBrush);
    StackSize.Bind(this, &SGameUI_InventoryContentBox::GetStackSize);
    FieldBrush.Bind(this, &SGameUI_InventoryContentBox::GetFieldBrush);

    ChildSlot
    [
        SNew(SOverlay)
        + SOverlay::Slot()
        [
            SNew(SImage).Image(RarityBrush)
        ]
        + SOverlay::Slot()
        [
            SNew(SImage).Image(FieldBrush)
        ]
        + SOverlay::Slot()
        [
            SNew(SImage).Image(BackgroundBrush)
        ]
        + SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Top).Padding(FMargin(0, 5, 5, 0))
        [
            SNew(STextBlock)
                .TextStyle(&HUDStyle->TitleText)
                .Text(StackSize)
        ]
    ];
}

void SGameUI_InventoryContentBox::OnMouseEnter(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    IsMouseOverMe = true;
}

void SGameUI_InventoryContentBox::OnMouseLeave(const FPointerEvent &MouseEvent) {
    IsMouseOverMe = false;
}

FReply SGameUI_InventoryContentBox::OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton) {
        AFPSCharacter* Character = Cast<AFPSCharacter>(PCOwner->GetPawn());
        if (!IsInventoryLegit(Character)) {
            return FReply::Unhandled();
        }
        if (Character->MyInventoryInstance->InventoryContent[FieldID]->IsManagableItem) {
            // Only allow managable Items (no placeholder) to be moved;
            return FReply::Handled().DetectDrag(SharedThis(this), EKeys::LeftMouseButton);
        }
    }

    return FReply::Unhandled();
}

//FReply SGameUI_InventoryContentBox::OnMouseButtonUp(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
////    if (GEngine) {
////        FString Helper = "Mouse up on Field: ";
////        Helper.Append(FString::FromInt(FieldID));
////        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), Helper);
////    }

//    return FReply::Handled();
//}

FReply SGameUI_InventoryContentBox::OnDragDetected(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
//    if (GEngine) {
//        FString Helper = "Mouse drag on Field: ";
//        Helper.Append(FString::FromInt(FieldID));
//        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), Helper);
//    }

    if (MouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton)) {
        TSharedRef<FItemDragDrop> Operation = FItemDragDrop::New(FieldID, GetFieldBrush());

        SGameUI::SetDragDropStart(Operation->DragBrush);

        return FReply::Handled().BeginDragDrop(Operation);
    }

    return FReply::Unhandled();//.BeginDragDrop(SGameUIInstance->ItemDragDropper());
}

FReply SGameUI_InventoryContentBox::OnDrop(const FGeometry &MyGeometry, const FDragDropEvent &DragDropEvent) {
//    if (GEngine) {
//        FString Helper = "Mouse drop on Field: ";
//        Helper.Append(FString::FromInt(FieldID));
//        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), Helper);
//    }

    TSharedPtr<FItemDragDrop> DragDropContent = DragDropEvent.GetOperationAs<FItemDragDrop>();
    SGameUI::SetDragDropFinished();

    if (DragDropContent.IsValid()) {
//        if (GEngine) {
//            FString infoString = "Landed on: ";
//            infoString.Append(FString::FromInt(FieldID));
//            infoString.Append(TEXT(", OriginID: "));
//            infoString.Append(FString::FromInt(DragDropContent->StartingField));
//            GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), infoString);
//        }

        AFPSCharacter* Character = Cast<AFPSCharacter>(PCOwner->GetPawn());
        if (IsInventoryLegit(Character)) {
            Character->MyInventoryInstance->RequestInventoryMove(DragDropContent->StartingField, FieldID);
        }

        return FReply::Handled();
    }
    else {
        UE_LOG(LogTemp, Log, TEXT("DragDropContent is invalid!?"));
        return FReply::Unhandled();
    }


}

const FSlateBrush* SGameUI_InventoryContentBox::GetBackgroundBrush() const {
    if (IsHotbar) {
        AFPSCharacter * Character = Cast<AFPSCharacter>(PCOwner->GetPawn());
        if (!PCOwner->IsLocalPlayerController()) {
            return &HUDStyle->HotbarBackground;
        }
        if (!Character) {
            return &HUDStyle->HotbarBackground;
        }
        if (!Character->MyInventoryInstance) {
            return &HUDStyle->HotbarBackground;
        }
        if (!Character->MyInventoryInstance->isInitialized) {
            return &HUDStyle->HotbarBackground;
        }

        if (Character->MyInventoryInstance->GetNumberOfEquipedItem() == FieldID) {
            return &HUDStyle->HotbarBackgroundEquiped;
        }

        return &HUDStyle->HotbarBackground;

    }
    else {
        // Inventory
        if (IsMouseOverMe) {
            return &HUDStyle->InventoryBackground_Hover;
        }
        else {
            return &HUDStyle->InventoryBackground;
        }
    }
}

const FSlateBrush* SGameUI_InventoryContentBox::GetRarityBrush() const {
    AFPSCharacter * Character = Cast<AFPSCharacter>(PCOwner->GetPawn());
    if (!PCOwner->IsLocalPlayerController()) {
        return &HUDStyle->Item_Empty;
    }
    if (!Character) {
        return &HUDStyle->Item_Empty;
    }
    if (!Character->MyInventoryInstance) {
        return &HUDStyle->Item_Empty;
    }
    if (!Character->MyInventoryInstance->isInitialized) {
        return &HUDStyle->Item_Empty;
    }
    if (Character->MyInventoryInstance->InventoryContent[FieldID] == nullptr) {
        return &HUDStyle->Item_Empty;
    }
    if (Character->MyInventoryInstance->InventoryContent[FieldID]->IsPendingKill()) {
        return &HUDStyle->Item_Empty;
    }

    switch (Character->MyInventoryInstance->InventoryContent[FieldID]->ItemRarity) {
    case EItemRarity::RARITY_COMMON:
        return &HUDStyle->FieldBackground_Common;
    case EItemRarity::RARITY_UNCOMMON:
        return &HUDStyle->FieldBackground_Uncommon;
    default:
        //UE_LOG(LogTemp, Warning, TEXT("GetRarityBrush(): No set rarity"));
        return &HUDStyle->Item_Empty;
    }

    return &HUDStyle->HotbarBackground;
}

FText SGameUI_InventoryContentBox::GetStackSize() const {
    AFPSCharacter * Character = Cast<AFPSCharacter>(PCOwner->GetPawn());
    if (!PCOwner->IsLocalPlayerController()) {
        return FText::FromString("NO_LCL");
    }
    if (!Character) {
        return FText::FromString("NO_CHR");
    }
    if (!Character->MyInventoryInstance) {
        return FText::FromString("NO_INV");
    }
    if (!Character->MyInventoryInstance->isInitialized) {
        return FText::FromString("NO_INI");
    }
//    if (!Character->MyInventoryInstance->InventoryContent.IsValidIndex(FieldID)) {
//        return FText::FromString("NO_VAL");
//    }
    if (Character->MyInventoryInstance->InventoryContent[FieldID] == nullptr) {
        return FText::FromString("NO_PTR");
    }
    if (Character->MyInventoryInstance->InventoryContent[FieldID]->IsPendingKill()) {
        return FText::FromString("IS_PK");
    }

    if (Character->MyInventoryInstance->InventoryContent.Num() == (AInventory::CHARACTERPANEL_SLOTS + AInventory::HOTBAR_SLOTS + AInventory::INVENTORY_SLOTS)) {
        int32 CStackSize = Character->MyInventoryInstance->InventoryContent[FieldID]->CurrentStackSize;
        if (CStackSize == 1 || CStackSize == 0) {
            return FText::FromString("");
        }
        return FText::FromString(FString::FromInt(Character->MyInventoryInstance->InventoryContent[FieldID]->CurrentStackSize));
    }

    return FText::FromString("NULL");
}

const FSlateBrush* SGameUI_InventoryContentBox::GetFieldBrush() const {
    AFPSCharacter* Character = Cast<AFPSCharacter>(PCOwner->GetPawn());
    if (!PCOwner->IsLocalPlayerController()) {
        return &HUDStyle->Item_Empty;
    }
    if (!Character) {
        //UE_LOG(LogTemp, Warning, TEXT("GetFieldBrush(): No Inventory-Instance defined"));
        return &HUDStyle->Item_Empty;
    }
    if (!Character->MyInventoryInstance) {
        //UE_LOG(LogTemp, Warning, TEXT("GetFieldBrush(): No Inventory-Instance defined"));
        return &HUDStyle->Item_Empty;
    }
    if (!Character->MyInventoryInstance->isInitialized) {
        return &HUDStyle->Item_Empty;
    }
    if (Character->MyInventoryInstance->InventoryContent[FieldID] == nullptr || Character->MyInventoryInstance->InventoryContent[FieldID]->IsPendingKill()) {
        return &HUDStyle->Item_Empty;
    }

    if (Character->MyInventoryInstance->InventoryContent.Num() == (AInventory::CHARACTERPANEL_SLOTS + AInventory::HOTBAR_SLOTS + AInventory::INVENTORY_SLOTS)) {
        //UE_LOG(LogTemp, Warning, TEXT("Giving back SlateBrush"));
        return &Character->MyInventoryInstance->InventoryContent[FieldID]->SlateBrush;
    }

//    UE_LOG(LogTemp, Error, TEXT("GetFieldBrush(): Error assigning Brush"));
    return &HUDStyle->Item_Empty;
}

bool SGameUI_InventoryContentBox::IsInventoryLegit(AFPSCharacter *Character) {
    if (!Character) {
        UE_LOG(LogTemp, Warning, TEXT("Character is not initialized"));
        return false;
    }
    if (!Character->MyInventoryInstance) {
        UE_LOG(LogTemp, Warning, TEXT("MyInventoryInstance is null"));
        return false;
    }
    if (!Character->MyInventoryInstance->isInitialized) {
        UE_LOG(LogTemp, Warning, TEXT("MyInventoryInstance is not initialized"));
        return false;
    }
    if (Character->MyInventoryInstance->InventoryContent[FieldID] == nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("This Inventory-Field is a nullpointer"));
        return false;
    }
    if (Character->MyInventoryInstance->InventoryContent[FieldID]->IsPendingKill()) {
        UE_LOG(LogTemp, Warning, TEXT("This Inventory-Field is pending to be killed"));
        return false;
    }

    return true;
}

//FPointerEvent SGameUI_InventoryContentBox::OnReceiveDrop() {

//}
