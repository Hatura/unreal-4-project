#pragma once

#include "Slate.h"
#include "../SGameUI.h"
#include "FPSCharacter.h"
#include "FPSPlayerController.h"

class SGameUI;

class SGameUI_InventoryContentBox : public SCompoundWidget {
public:
    SLATE_BEGIN_ARGS(SGameUI_InventoryContentBox)
        : _GameHUD(),
          _PCOwner(),
          _FieldID(),
          _IsHotbar(false)
    {
    }

    SLATE_ARGUMENT(TWeakObjectPtr<class AGameHUD>, GameHUD)
    SLATE_ARGUMENT(TWeakObjectPtr<class AFPSPlayerController>, PCOwner)
    SLATE_ATTRIBUTE(int32, FieldID)
    SLATE_ATTRIBUTE(bool, IsHotbar)

    SLATE_END_ARGS()

    void Construct(const FArguments& args);

    void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
    void OnMouseLeave(const FPointerEvent &MouseEvent) override;

    FReply OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;
//    FReply OnMouseButtonUp(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;

    FReply OnDragDetected(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;
    FReply OnDrop(const FGeometry &MyGeometry, const FDragDropEvent &DragDropEvent) override;

    //FPointerEvent OnReceiveDrop();

private:
    // Backgroundbrush, Different behaviour for Actionbar and Inventory
    TAttribute<const FSlateBrush*> BackgroundBrush;
    const FSlateBrush* GetBackgroundBrush() const;

    // Backgroundbrush, Coloring the field
    TAttribute<const FSlateBrush*> RarityBrush;
    const FSlateBrush* GetRarityBrush() const;

    // Item->CurrentStackSize
    TAttribute<FText> StackSize;
    FText GetStackSize() const;

    // Item->SlateBrush
    TAttribute<const FSlateBrush*> FieldBrush;
    const FSlateBrush* GetFieldBrush() const;

    TWeakObjectPtr<class AGameHUD> GameHUD;
    TWeakObjectPtr<class AFPSPlayerController> PCOwner;

    // My inventory-identifier
    int32 FieldID;

    // Is Hotbar instead of Inventory?
    bool IsHotbar;

    bool IsMouseOverMe;

    const struct FGlobalGameHUDStyle_Struct* HUDStyle;

    bool IsInventoryLegit(AFPSCharacter* Character);
};
