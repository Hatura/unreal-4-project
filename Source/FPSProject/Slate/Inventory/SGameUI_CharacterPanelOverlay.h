#pragma once

#include "Slate.h"
#include "Slate/MovableWidget.h"
#include "../SGameUI.h"
#include "FPSPlayerController.h"

class SGameUI_CharacterPanelOverlay : public SMovableWidget {
public:
    SLATE_BEGIN_ARGS(SGameUI_CharacterPanelOverlay)
        : _GameHUD(),
          _PCOwner(),
          _SGameUIInstance()
    {
    }

    SLATE_ARGUMENT(TWeakObjectPtr<class AGameHUD>, GameHUD)
    SLATE_ARGUMENT(TWeakObjectPtr<class AFPSPlayerController>, PCOwner)
    SLATE_ARGUMENT(SGameUI*, SGameUIInstance)

    SLATE_END_ARGS()

    void Construct(const FArguments& args);

    void SetVisibility(EVisibility visibility);

private:
    TWeakObjectPtr<class AGameHUD> GameHUD;
    TWeakObjectPtr<class AFPSPlayerController> PCOwner;
    SGameUI* SGameUIInstance;

    const struct FGlobalGameHUDStyle_Struct* HUDStyle;
};
