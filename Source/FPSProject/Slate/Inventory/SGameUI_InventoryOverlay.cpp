#include "FPSProject.h"

#include "../GlobalGameHUDStyle.h"
#include "../MenuStyles.h"
#include "FPSCharacter.h"
#include "SGameUI_InventoryOverlay.h"

void SGameUI_InventoryOverlay::Construct(const FArguments &args) {
    GameHUD = args._GameHUD;
    PCOwner = args._PCOwner;
    SGameUIInstance = args._SGameUIInstance;

    HUDStyle = &FMenuStyles::Get().GetWidgetStyle<FGlobalGameHUDStyle_Struct>("Global_GameHUD");

    ChildSlot
    [
        SAssignNew(WidgetContent, SOverlay)
        + SOverlay::Slot()
        [
            SNew(SVerticalBox)
            + SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 0, 0, 4.0f))
            [
                SNew(SHorizontalBox)
                + SHorizontalBox::Slot().MaxWidth(20.0f)
                [
                    SNew(SButton)
                    .ButtonStyle(&HUDStyle->MinimizeButton)
                    .ClickMethod(EButtonClickMethod::MouseDown)
                    .OnClicked(this, &SMovableWidget::MinimizeButtonClicked)
                ]
                + SHorizontalBox::Slot().MaxWidth(140.0f).Padding(FMargin(4.0f, 0, 0, 0))
                [
                    SAssignNew(DragInitializer, SDragInitializer).BorderImage(&HUDStyle->TitleBarBorder)
                    [
                        SNew(STextBlock)
                        .TextStyle(&HUDStyle->TitleBarText)
                        .Text(NSLOCTEXT("HaturLoc", "InventoryTitlebarText", "Inventory"))
                    ]
                ]
            ]
            + SVerticalBox::Slot()
            [
                SNew(SVerticalBox).Visibility(EVisibility::Visible)
                + SVerticalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Top)
                .Padding(0.0f, 2.0f)
                [
                    SNew(SHorizontalBox).Visibility(EVisibility::Visible)
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 5], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 5)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 6], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 6)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 7], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 7)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 8], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 8)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 9], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 9)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 10], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 10)
                    ]
                ]
                + SVerticalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Top)
                .Padding(0.0f, 2.0f)
                [
                    SNew(SHorizontalBox).Visibility(EVisibility::Visible)
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 11], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 11)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 12], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 12)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 13], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 13)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 14], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 14)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 15], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 15)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 16], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 16)
                    ]
                ]
                + SVerticalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Top)
                .Padding(0.0f, 2.0f)
                [
                    SNew(SHorizontalBox).Visibility(EVisibility::Visible)
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 17], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 17)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 18], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 18)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 19], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 19)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 20], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 20)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 21], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 21)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 22], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 22)
                    ]
                ]
                + SVerticalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Top)
                .Padding(0.0f, 2.0f)
                [
                    SNew(SHorizontalBox).Visibility(EVisibility::Visible)
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 23], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 23)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 24], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 24)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 25], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 25)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 26], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 26)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 27], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 27)
                    ]
                    + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
                    .Padding(2.0f, 0)
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 28], SGameUI_InventoryContentBox)
                            .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 28)
                    ]
                ]
            ]
        ]
    ];

    SetInitialPosition(650, 150);

    DragInitializer->LinkWidget(this);

    SetVisibility(EVisibility::Collapsed);
}

void SGameUI_InventoryOverlay::SetVisibility(EVisibility visibility) {
    SGameUI_InventoryOverlay::Visibility = visibility;
}

FReply SGameUI_InventoryOverlay::OnDrop(const FGeometry &MyGeometry, const FDragDropEvent &DragDropEvent) {
    if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Magenta, TEXT("Meta ebene 2")); }
    return FReply::Unhandled();
}
