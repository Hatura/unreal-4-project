#include "FPSProject.h"

#include "../GlobalGameHUDStyle.h"
#include "../MenuStyles.h"
#include "FPSCharacter.h"
#include "SGameUI_CharacterPanelOverlay.h"

void SGameUI_CharacterPanelOverlay::Construct(const FArguments &args) {
    GameHUD = args._GameHUD;
    PCOwner = args._PCOwner;
    SGameUIInstance = args._SGameUIInstance;

    HUDStyle = &FMenuStyles::Get().GetWidgetStyle<FGlobalGameHUDStyle_Struct>("Global_GameHUD");

    ChildSlot
    [
        SAssignNew(WidgetContent, SOverlay)
        + SOverlay::Slot()
        [
            SNew(SVerticalBox)
            + SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 0, 0, 4.0f))
            [
                SNew(SHorizontalBox)
                + SHorizontalBox::Slot().MaxWidth(20.0f)
                [
                    SNew(SButton)
                    .ButtonStyle(&HUDStyle->MinimizeButton)
                    .ClickMethod(EButtonClickMethod::MouseDown)
                    .OnClicked(this, &SMovableWidget::MinimizeButtonClicked)
                ]
                + SHorizontalBox::Slot().MaxWidth(140.0f).Padding(FMargin(4.0f, 0, 0, 0))
                [
                    SAssignNew(DragInitializer, SDragInitializer).BorderImage(&HUDStyle->TitleBarBorder)
                    [
                        SNew(STextBlock)
                        .TextStyle(&HUDStyle->TitleBarText)
                        .Text(NSLOCTEXT("HaturLoc", "CharacterPanelTitlebarText", "Character panel"))
                    ]
                ]
            ]
            + SVerticalBox::Slot()
            [
                SNew(SOverlay)
                + SOverlay::Slot()
                [
                    SNew(SImage).Image(&HUDStyle->CharacterPanelBackground)
                ]
                + SOverlay::Slot()
                [
                    SNew(SCanvas)
                    // Armor etc.
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(174, 10))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[0], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(0)
                    ]
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(242, 33))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[1], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(1)
                    ]
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(208, 160))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[2], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(2)
                    ]
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(208, 300))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[3], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(3)
                    ]
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(208, 454))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[4], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(4)
                    ]

                    // Gimmicks, Compass, Map etc.
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(47, 100))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[5], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(5)
                    ]
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(47, 170))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[6], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(6)
                    ]
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(47, 240))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[7], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(7)
                    ]
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(47, 310))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[8], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(8)
                    ]
                    + SCanvas::Slot().Size(FVector2D(64, 64)).Position(FVector2D(47, 380))
                    [
                        SAssignNew(SGameUIInstance->ContentBoxArray[9], SGameUI_InventoryContentBox).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(9)
                    ]
                ]
            ]
        ]
    ];

    SetInitialPosition(50, 150);

    DragInitializer->LinkWidget(this);

    SetVisibility(EVisibility::Collapsed);
}

void SGameUI_CharacterPanelOverlay::SetVisibility(EVisibility visibility) {
    SGameUI_CharacterPanelOverlay::Visibility = visibility;
}
