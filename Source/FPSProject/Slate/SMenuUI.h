#pragma once

#include "Slate.h"

class SMenuUI : public SCompoundWidget {
    SLATE_BEGIN_ARGS(SMenuUI)
        : _GameMenuHUD()
    {
    }

    SLATE_ARGUMENT(TWeakObjectPtr<class AGameMenuHUD>, GameMenuHUD);

    SLATE_END_ARGS()

    const struct FGlobalStyle* MenuStyle;

public:
    // -> args = Arguments structure that contains widget-specific setup information
    void Construct(const FArguments& args);

private:
    // Click handler for the buttons
    FReply PlayGameClicked();
    FReply QuitGameClicked();

    // Stores a reference to the hud controlling this class
    TWeakObjectPtr<class AGameMenuHUD> GameMenuHUD;
};
