#include "FPSProject.h"

#include "GameHUD.h"
#include "SGameUI.h"

#include "GlobalGameHUDStyle.h"
#include "MenuStyles.h"
#include "FPSPlayerController.h"
#include "FPSCharacter.h"
#include "Inventory/BaseItem.h"

// ++ Static Cursor Decorator stuff, maybe not the best solution?

TSharedPtr<class SCustomDecorator> SGameUI::DragDropDecorator;
const FSlateBrush* SGameUI::DragDropBrush;

int32 SGameUI::MouseX;
int32 SGameUI::MouseY;

// -- Static Cursor Decorator Stuff

void SGameUI::Construct(const FArguments &args) {
    GameHUD = args._GameHUD;

    HUDStyle = &FMenuStyles::Get().GetWidgetStyle<FGlobalGameHUDStyle_Struct>("Global_GameHUD");

    MouseX = 0;
    MouseY = 0;

    CraftingMenuAdded = false;

    PCOwner = Cast<AFPSPlayerController>(GameHUD->PlayerOwner);
    if (PCOwner == nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("PCOwner is NOT initialized in SGameUI::Construct()"));
    }

    ContentBoxArray.Reset();
    for (int i = 0; i < (AInventory::CHARACTERPANEL_SLOTS + AInventory::HOTBAR_SLOTS + AInventory::INVENTORY_SLOTS); i++) {
        ContentBoxArray.Add(nullptr);
    }

    DragDropDelegate.Bind(this, &SGameUI::GetDragDropBrush);
    DecoratorPosition.Bind(this, &SGameUI::GetDecoratorPosition);

    ChildSlot
    [
        SNew(SOverlay)
        + SOverlay::Slot()
        [
            SAssignNew(OverlayCanvas, SCanvas)
            + SCanvas::Slot().Position(TAttribute<FVector2D>(this, &SGameUI::GetCharacterPanelPos)).Size(TAttribute<FVector2D>(this, &SGameUI::GetCharacterPanelSize))
            [
                SAssignNew(CharacterPanelOverlay, SGameUI_CharacterPanelOverlay).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).SGameUIInstance(this)
            ]
            + SCanvas::Slot().Position(TAttribute<FVector2D>(this, &SGameUI::GetInventoryOverlayPos)).Size(TAttribute<FVector2D>(this, &SGameUI::GetInventoryOverlaySize))
            [
                SAssignNew(InventoryOverlay, SGameUI_InventoryOverlay).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).SGameUIInstance(this)
            ]
            + SCanvas::Slot().Position(TAttribute<FVector2D>(this, &SGameUI::GetCraftingMenuPos)).Size(TAttribute<FVector2D>(this, &SGameUI::GetCraftingMenuSize))
            [
                SAssignNew(CraftingMenuOverlay, SGameUI_CraftingMenu).GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).SGameUIInstance(this)
            ]
            + SCanvas::Slot().Position(TAttribute<FVector2D>(this, &SGameUI::GetCraftingProgressBarPos)).Size(TAttribute<FVector2D>(this, &SGameUI::GetCraftingProgressBarSize))
            [
                SAssignNew(CraftingProgressBar, SGameUI_CraftingProgressBar).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner))
            ]
        ]
        // Action-/HotBars
        + SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Bottom)
        [
            SNew(SHorizontalBox).Visibility(EVisibility::Visible)
            + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
            .Padding(2.0f, 0)
            [
                SAssignNew(ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 0], SGameUI_InventoryContentBox)
                    .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 0).IsHotbar(true)
            ]
            + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
            .Padding(2.0f, 0)
            [
                SAssignNew(ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 1], SGameUI_InventoryContentBox)
                    .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 1).IsHotbar(true)
            ]
            + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
            .Padding(2.0f, 0)
            [
                SAssignNew(ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 2], SGameUI_InventoryContentBox)
                    .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 2).IsHotbar(true)
            ]
            + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
            .Padding(2.0f, 0)
            [
                SAssignNew(ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 3], SGameUI_InventoryContentBox)
                    .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 3).IsHotbar(true)
            ]
            + SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
            .Padding(2.0f, 0)
            [
                SAssignNew(ContentBoxArray[AInventory::CHARACTERPANEL_SLOTS + 4], SGameUI_InventoryContentBox)
                    .GameHUD(GameHUD).PCOwner(TWeakObjectPtr<AFPSPlayerController>(PCOwner)).FieldID(AInventory::CHARACTERPANEL_SLOTS + 4).IsHotbar(true)
            ]
        ]
        + SOverlay::Slot().HAlign(HAlign_Left).VAlign(VAlign_Top).Padding(DecoratorPosition)
        [
            SAssignNew(DragDropDecorator, SCustomDecorator).Image(DragDropDelegate)
        ]
    ];

    DragDropDecorator->SetVisibility(EVisibility::HitTestInvisible);

    FItemDragDrop::LinkWidget(this);
}

FVector2D SGameUI::GetCraftingMenuPos() const {
    if (CraftingMenuOverlay.IsValid()) {
        return CraftingMenuOverlay->GetMovableWidgetPos();
    }

    return FVector2D(0, 0);
}

FVector2D SGameUI::GetCraftingMenuSize() const {
    if (CraftingMenuOverlay.IsValid()) {
        return CraftingMenuOverlay->GetContentSize();
    }

    return FVector2D(0, 0);
}

FVector2D SGameUI::GetInventoryOverlayPos() const {
    if (InventoryOverlay.IsValid()) {
        return InventoryOverlay->GetMovableWidgetPos();
    }

    return FVector2D(0, 0);
}

FVector2D SGameUI::GetInventoryOverlaySize() const {
    if (InventoryOverlay.IsValid()) {
        return InventoryOverlay->GetContentSize();
    }

    return FVector2D(0, 0);
}

FVector2D SGameUI::GetCharacterPanelPos() const {
    if (CharacterPanelOverlay.IsValid()) {
        return CharacterPanelOverlay->GetMovableWidgetPos();
    }

    return FVector2D(0, 0);
}

FVector2D SGameUI::GetCharacterPanelSize() const {
    if (CharacterPanelOverlay.IsValid()) {
        return CharacterPanelOverlay->GetContentSize();
    }

    return FVector2D(0, 0);
}

FVector2D SGameUI::GetCraftingProgressBarPos() const {
    if (CraftingProgressBar.IsValid() && GEngine) {
        FVector2D ViewportSize;

        GEngine->GameViewport->GetViewportSize(ViewportSize);

        FVector2D Position;
        Position.X = ViewportSize.X / 2 - CraftingProgressBar->GetDesiredSize().X / 2; // Center horizontally
        Position.Y = ViewportSize.Y - 86; // Absolute

        return Position;
    }

    return FVector2D(0, 0);
}

FVector2D SGameUI::GetCraftingProgressBarSize() const {
    if (CraftingProgressBar.IsValid()) {
        return CraftingProgressBar->GetDesiredSize();
    }

    return FVector2D(0, 0);
}

void SGameUI::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{    
    SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);
}

void SGameUI::SetDragDropStart(const FSlateBrush *DragBrush) {
    UE_LOG(LogTemp, Log, TEXT("Start Drag"));
    DragDropBrush = DragBrush;
    DragDropDecorator->SetVisibility(EVisibility::HitTestInvisible);
}

void SGameUI::SetDragDropFinished() {
    UE_LOG(LogTemp, Log, TEXT("End Drag"));
    DragDropBrush = nullptr;
    DragDropDecorator->SetVisibility(EVisibility::Collapsed);
}

FReply SGameUI::OnKeyboardFocusReceived(const FGeometry &MyGeometry, const FKeyboardFocusEvent &InKeyboardFocusEvent) {
    return FReply::Handled().ReleaseMouseCapture();
}

const FSlateBrush* SGameUI::GetDragDropBrush() const {
    return DragDropBrush;
}

FMargin SGameUI::GetDecoratorPosition() const {
    // Because he is still requesting it, even if it is invisible
    // if the X value is changed drap drop stops working??
    if (DragDropBrush == nullptr) {
        return FMargin(MouseX, MouseY, 0, 0);
    }
    else {
        return FMargin(MouseX - DragDropBrush->ImageSize.X / 2, MouseY - DragDropBrush->ImageSize.Y / 2, 0, 0);
    }
}

// +++++++++++++
// FItemDragDrop
// +++++++++++++

TSharedRef<FItemDragDrop> FItemDragDrop::New(int32 StartingField, const FSlateBrush* DragBrush) {
    TSharedRef<FItemDragDrop> Operation = MakeShareable(new FItemDragDrop);

    Operation->StartingField = StartingField;
    Operation->DragBrush = DragBrush;
    Operation->Construct();

    return Operation;
}

void FItemDragDrop::OnDragged(const FDragDropEvent &DragDropEvent) {
// This is from the default Decorator, maybe we use it if they change the way it works..

//    if (CursorDecoratorWindow.IsValid()) {
//        CursorDecoratorWindow->SetOpacity(0.75f);
//        CursorDecoratorWindow->MoveWindowTo(DragDropEvent.GetScreenSpacePosition());
//    }

//    TSharedPtr<FItemDragDrop> DragDropOP = DragDropEvent.GetOperationAs<FItemDragDrop>();

    // ++ Cursor decorator
    if (LinkedWidget == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("FItemDragDrop::OnDragged(): No linked widget"));
        return;
    }

    FWidgetPath MyWidgetPath;
    FSlateApplication::Get().GeneratePathToWidgetChecked(LinkedWidget->AsShared(), MyWidgetPath);

    FVector2D vec = MyWidgetPath.GetWindow()->GetWindowGeometryInScreen().AbsoluteToLocal(FVector2D(DragDropEvent.GetScreenSpacePosition().X, DragDropEvent.GetScreenSpacePosition().Y));

    SGameUI::MouseX = FMath::FloorToInt(vec.X);
    SGameUI::MouseY = FMath::FloorToInt(vec.Y);
    // -- Cursor Decorator
}

void FItemDragDrop::OnDrop(bool bDropWasHandled, const FPointerEvent &MouseEvent) {
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), TEXT("ItemDragDrop::OnDrop()"));
    }

    SGameUI::SetDragDropFinished();

    FDragDropOperation::OnDrop(bDropWasHandled, MouseEvent);
}

SCompoundWidget* FItemDragDrop::LinkedWidget;

void FItemDragDrop::LinkWidget(SCompoundWidget* WidgetToLink) {
    LinkedWidget = WidgetToLink;
}

// Default decorator, maybe later use after changes have been made?

//TSharedPtr<SWidget> FItemDragDrop::GetDefaultDecorator() const {
//    //return SNew(SOverlay) +SOverlay::Slot().HAlign(HAlign_Fill).VAlign(VAlign_Fill) [ SNew(SImage).Image(DragBrush) ];
//}
