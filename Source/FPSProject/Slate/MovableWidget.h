#pragma once

#include "Slate.h"

//class SAutoCanvas : public SCanvas
//{
//    virtual FVector2D ComputeDesiredSize() const override;
//};

/**
 * @brief A meta widget for movable HUD widgets!
 */
class SMovableWidget : public SCompoundWidget
{
public:
    TSharedPtr<SOverlay> WidgetContent;

    virtual void SetInitialPosition(int32 PosX, int32 PosY);

    virtual void UpdateLocation(int32 PosX, int32 PosY);

    virtual FReply MinimizeButtonClicked();

    virtual FVector2D GetMovableWidgetPos() const;

    // Returns the SOverlay (WidgetContent) size for the SCanvas
    virtual FVector2D GetContentSize() const;

protected:
    // Used to determine the content size 
    TSharedPtr<class SDragInitializer> DragInitializer;
    TSharedPtr<class SMinimizeButton> MinimizeButton;

    int32 DragPosX = 0;
    int32 DragPosY = 0;
};

class SDragInitializer : public SBorder
{
public:
    void LinkWidget(class SMovableWidget* MovableWidgetLink);

protected:
    class SMovableWidget* MovableWidget;

    // Needs to be called, is a substitute for SLATE_ARGS

    FReply OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;

    FReply OnDragDetected(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) override;
};

class FMovableWidgetDrag : public FDragDropOperation {
public:
    DRAG_DROP_OPERATOR_TYPE(FMovableWidgetDrag, FDragDropOperation)

    static TSharedRef<FMovableWidgetDrag> New();

    class SMovableWidget* MovableWidget;

    void LinkWidget(class SMovableWidget* MovableWidgetLink);

    void OnDragged(const class FDragDropEvent& DragDropEvent) override;
};
