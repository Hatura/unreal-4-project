#include "FPSProject.h"
#include "BackgroundOverlay.h"

FReply SHackButton::OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    UE_LOG(LogTemp, Log, TEXT("HACKBTN DOWN"));

    return FReply::Handled();
}

FReply SHackButton::OnMouseButtonUp(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    UE_LOG(LogTemp, Log, TEXT("HACKBTN UP"));

    return FReply::Handled();
}

FReply SHackButton::OnMouseButtonDoubleClick(const FGeometry &InMyGeometry, const FPointerEvent &InMouseEvent) {
    UE_LOG(LogTemp, Log, TEXT("HACKBTN DOUBLE"));

    return FReply::Handled();
}

void SBackgroundOverlay::Construct(const FArguments &InArgs) {
    ChildSlot
    .VAlign(VAlign_Fill)
    .HAlign(HAlign_Fill)
    [
        SNew(SOverlay)

        +SOverlay::Slot()
        .VAlign(VAlign_Fill)
        .HAlign(HAlign_Fill)
        [
            SNew(SHackButton)
            .ClickMethod(EButtonClickMethod::MouseDown)
            .OnClicked(this, &SBackgroundOverlay::OnButtonClick)
            [
                SNew(SImage)
                .Image(FCoreStyle::Get().GetBrush("Checkerboard"))
            ]
        ]
    ];

    Visibility = EVisibility::Visible;
}

FReply SBackgroundOverlay::OnKeyboardFocusReceived(const FGeometry &MyGeometry, const FKeyboardFocusEvent &InKeyboardFocusEvent) {
    UE_LOG(LogTemp, Log, TEXT("BG OVERLAY FOCUS"));

    return FReply::Handled().ReleaseMouseCapture();
}

FReply SBackgroundOverlay::OnMouseButtonDown(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    return FReply::Handled();
}

FReply SBackgroundOverlay::OnMouseButtonUp(const FGeometry &MyGeometry, const FPointerEvent &MouseEvent) {
    return FReply::Handled();
}

FReply SBackgroundOverlay::OnMouseButtonDoubleClick(const FGeometry &InMyGeometry, const FPointerEvent &InMouseEvent) {
    return FReply::Handled();
}

FReply SBackgroundOverlay::OnButtonClick() {
    UE_LOG(LogTemp, Log, TEXT("CheckerBUTTONCLICK"));

    return FReply::Handled();
}
