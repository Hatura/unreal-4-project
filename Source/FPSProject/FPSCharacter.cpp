

#include "FPSProject.h"
#include "FPSCharacter.h"
#include "FPSGameMode.h"
#include "FPSProjectile.h"
#include "Inventory/BaseItem.h"
#include "FPSPlayerController.h"
#include "GameHUD.h"
#include "Slate/MenuStyles.h"
#include "Slate.h"
#include "UnrealNetwork.h"


AFPSCharacter::AFPSCharacter(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// Create a CameraComponent
	FirstPersonCameraComponent = PCIP.CreateDefaultSubobject<UCameraComponent>(this, TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->AttachParent = CapsuleComponent;

	// Position the camera a bit above the eyes
	FirstPersonCameraComponent->RelativeLocation = FVector(0, 0, 50.0f + BaseEyeHeight);

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	FirstPersonMesh = PCIP.CreateAbstractDefaultSubobject<USkeletalMeshComponent>(this, TEXT("FirstPersonMesh"));
	FirstPersonMesh->SetOnlyOwnerSee(true);
	FirstPersonMesh->AttachParent = FirstPersonCameraComponent;
	FirstPersonMesh->bCastDynamicShadow = false;
    FirstPersonMesh->CastShadow = false;

//    EquipedItem_FP = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("EquipedItem_FP"));
//    EquipedItem_FP->SetOnlyOwnerSee(true);
//    EquipedItem_FP->AttachParent = FirstPersonMesh;
//    EquipedItem_FP->AttachSocketName = "RightHandSocket";
//    EquipedItem_FP->CastShadow = false;
//    EquipedItem_FP->bCastDynamicShadow = false;
//    EquipedItem_FP->SetCollisionEnabled(ECollisionEnabled::NoCollision);

    ItemCollectorBox = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("ItemCollectorBox"));
    ItemCollectorBox->SetCollisionProfileName(TEXT("ItemCollector"));
    ItemCollectorBox->AttachParent = RootComponent;
    ItemCollectorBox->OnComponentBeginOverlap.AddDynamic(this, &AFPSCharacter::OnItemCollectorOverlap);

    Mesh->SetOwnerNoSee(true);

//    EquipedItem_TP = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("EquipedItem_TP"));
//    EquipedItem_TP->SetOwnerNoSee(true);
//    EquipedItem_TP->AttachParent = Mesh;
//    EquipedItem_TP->AttachSocketName = "RightHandSocketTP";
//    EquipedItem_TP->CastShadow = true;
//    EquipedItem_TP->bCastDynamicShadow = true;
//    EquipedItem_TP->SetCollisionEnabled(ECollisionEnabled::NoCollision);

    CharacterInteractingWith = EInteractableWorldObject::NONE;

    IsAlive = true;
}

void AFPSCharacter::PostInitializeComponents() {
    Super::PostInitializeComponents();

    UWorld* MyWorld = GetWorld();

    // Set up default inventory, TODO: Implement network loading
    if (Role == ROLE_Authority) {
        if (MyWorld) {
            AFPSGameMode* FPSGameMode = Cast<AFPSGameMode>(MyWorld->GetAuthGameMode());
            if (FPSGameMode) {
                if (!MyInventoryInstance) {
                    FActorSpawnParameters SpawnParams;
                    SpawnParams.Owner = this;
                    MyInventoryInstance = MyWorld->SpawnActor<AInventory>(FPSGameMode->InventoryClass, SpawnParams);
                    //MyInventoryInstance->SetOwner(this);
                    MyInventoryInstance->Server_InitializeInventory(MyWorld);
                }
                if (!CraftingManager) {
                    CraftingManager = MyWorld->SpawnActor<ACraftingManager>(ACraftingManager::StaticClass());
                    CraftingManager->SetOwner(this);
                    CraftingManager->SetCharacterOwner(this);
                }
            }
            else {
                UE_LOG(LogTemp, Error, TEXT("Couldn't acces GameMode in Character::PostInitializeCompononents()"));
            }
        }
        else {
            FMessageDialog::Open(EAppMsgType::Ok, FText::FromString(TEXT("Failed to acquire World in Character::PostInitializeCompononents()!")));
        }
    }
}

void AFPSCharacter::BeginPlay() {
    Super::BeginPlay();
}

void AFPSCharacter::SetupPlayerInputComponent(UInputComponent* InputComponent) {
	// set up gameplay key bindings
	// axismapping
    InputComponent->BindAxis("MoveForward", this, &AFPSCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AFPSCharacter::MoveRight);
	InputComponent->BindAxis("Turn", this, &AFPSCharacter::AddControllerYawInput);
    InputComponent->BindAxis("LookUp", this, &AFPSCharacter::AddControllerPitchInput);

	// actionmapping
	InputComponent->BindAction("Jump", IE_Pressed, this, &AFPSCharacter::OnStartJump);
	InputComponent->BindAction("Jump", IE_Released, this, &AFPSCharacter::OnStopJump);

    InputComponent->BindAction("Fire", IE_Pressed, this, &AFPSCharacter::StartPrimaryAction);
    InputComponent->BindAction("Fire", IE_Released, this, &AFPSCharacter::StopPrimaryAction);
    InputComponent->BindAction("Interact", IE_Pressed, this, &AFPSCharacter::OnInteract);
    InputComponent->BindAction("AbortCrafting", IE_Pressed, this, &AFPSCharacter::OnAbortCrafting);
    InputComponent->BindAction("OpenInventory", IE_Pressed, this, &AFPSCharacter::OnToggleInventory);
    InputComponent->BindAction("OpenCharacterPanel", IE_Pressed, this, &AFPSCharacter::OnToggleCharacterPanel);
    InputComponent->BindAction("OpenCraftingManager", IE_Pressed, this, &AFPSCharacter::OnToggleCraftingManager);

    //InputComponent->BindAction("AnimChange", IE_Pressed, this, &AFPSCharacter::EquipRifle);
    InputComponent->BindAction("TestAction", IE_Pressed, this, &AFPSCharacter::TestAction);

    // cause these delegates can't have arguments, right?
    InputComponent->BindAction("EquipItem1", IE_Pressed, this, &AFPSCharacter::EquipItem1);
    InputComponent->BindAction("EquipItem2", IE_Pressed, this, &AFPSCharacter::EquipItem2);
    InputComponent->BindAction("EquipItem3", IE_Pressed, this, &AFPSCharacter::EquipItem3);
    InputComponent->BindAction("EquipItem4", IE_Pressed, this, &AFPSCharacter::EquipItem4);
    InputComponent->BindAction("EquipItem5", IE_Pressed, this, &AFPSCharacter::EquipItem5);
}

void AFPSCharacter::MoveForward(float Value) {
    if ((Controller != NULL) && (Value != 0.0f)) {
        // find out which way is forward
        FRotator Rotation = Controller->GetControlRotation();

        // Limit pitch when walking or falling
        if (CharacterMovement->IsMovingOnGround() || CharacterMovement->IsFalling()) {
            Rotation.Pitch = 0.0f;
        }

        // add movement in that direction
        const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
        AddMovementInput(Direction, Value);
    }
}

void AFPSCharacter::MoveRight(float Value) {
    if ((Controller != NULL) && (Value != 0.0f)) {
        // find out which way is right
        const FRotator Rotation = Controller->GetControlRotation();
        const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);

        // add movement in that direction
        AddMovementInput(Direction, Value);
    }
}

void AFPSCharacter::AddControllerPitchInput(float val) {
    if (GetMyGameHUD()) {
        // Only allow movement if inventory is not open
        if (!GetMyGameHUD()->GetInventoryOpen() && !GetMyGameHUD()->GetCharacterPanelOpen() && !GetMyGameHUD()->GetCraftingManagerOpen()) {
            Super::AddControllerPitchInput(val);
        }
    }
}

void AFPSCharacter::AddControllerYawInput(float val) {
    if (GetMyGameHUD()) {
        // Only allow movement if inventory is not open
        if (!GetMyGameHUD()->GetInventoryOpen() && !GetMyGameHUD()->GetCharacterPanelOpen() && !GetMyGameHUD()->GetCraftingManagerOpen()) {
            Super::AddControllerYawInput(val);
        }
    }
}

void AFPSCharacter::OnStartJump() {
	bPressedJump = true;
}

void AFPSCharacter::OnStopJump() {
	bPressedJump = false;
}

void AFPSCharacter::StartPrimaryAction() {
    if (GetMyGameHUD()->GetInventoryOpen() || GetMyGameHUD()->GetCharacterPanelOpen() || GetMyGameHUD()->GetCraftingManagerOpen()) {
        return;
    }

    // Is previewing Building to plant? Then only plant and return
    if (CraftingManager && CraftingManager->IsPreviewingBuilding()) {
        CraftingManager->PlantBuilding();
        return;
    }

    if (MyInventoryInstance && MyInventoryInstance->isInitialized && MyInventoryInstance->GetEquipedItem() != nullptr) {
        // Get item type and perform action
//        AInstantWeapon *InstantWeapon = Cast<AInstantWeapon>(MyInventoryInstance->GetEquipedItem());
//        if (InstantWeapon) {
//            InstantWeapon->FireWeapon_Trace();
//            return;
//        }

//        AProjectileWeapon* ProjectileWeapon = Cast<AProjectileWeapon>(MyInventoryInstance->GetEquipedItem());
//        if (ProjectileWeapon) {
//            ProjectileWeapon->FireWeapon_Projectile(FVector::ZeroVector, FVector_NetQuantizeNormal::ZeroVector);
//            return; // We are finished here
//        }

        // Start weapon fire
        AWeaponItem* Weapon = Cast<AWeaponItem>(MyInventoryInstance->GetEquipedItem());
        if (Weapon) {
            Weapon->StartFire();
            return; // We are finished here
        }

        // Use item
        AUsableItem *UsableItem = Cast<AUsableItem>(MyInventoryInstance->GetEquipedItem());
        if (UsableItem) {
            UsableItem->UseItem();
            return; // we are finished here
        }
    }
}

void AFPSCharacter::StopPrimaryAction() {
    if (MyInventoryInstance && MyInventoryInstance->isInitialized && MyInventoryInstance->GetEquipedItem() != nullptr) {

        // Stop weapon fire
        AWeaponItem* Weapon = Cast<AWeaponItem>(MyInventoryInstance->GetEquipedItem());
        if (Weapon) {
            UE_LOG(LogTemp, Log, TEXT("Stopping weapon fire"));
            Weapon->StopFire();
            return; // We are finished here
        }
    }
}

void AFPSCharacter::OnInteract() {
    UE_LOG(LogTemp, Log, TEXT(">> OnInteract()"));

    AActor *TracedActor = TraceAndReturnActor();

    if (TracedActor == nullptr) {
        // we hit nothing
        return;
    }

    ABaseBuilding* Building = Cast<ABaseBuilding>(TracedActor);
    if (Building && !Building->GetBuilt() && Building->GetOwner() != this) {
        // Building of another Player which is not finished, somehow change this TODO (Make invisible)
        return;
    }

    IInteractableObjectInterface* InteractableObject = InterfaceCast<IInteractableObjectInterface>(TracedActor);
    if (InteractableObject) {
        UE_LOG(LogTemp, Log, TEXT("Interaction with Object"));

        Server_OnInteract(this, TracedActor);
        //InteractableObject->Interact(this);
    }

}

bool AFPSCharacter::Server_OnInteract_Validate(AFPSCharacter* Initiator, AActor* InteractableActor) {
    return true;
}

void AFPSCharacter::Server_OnInteract_Implementation(AFPSCharacter* Initiator, AActor* InteractableActor) {
    //InteractableObject->Interact(Initiator);
    UE_LOG(LogTemp, Log, TEXT("Server_OnInteract()"));

    IInteractableObjectInterface* InteractableObject = InterfaceCast<IInteractableObjectInterface>(InteractableActor);
    if (InteractableObject) {
        InteractableObject->Interact(Initiator);
    }
}

void AFPSCharacter::OnToggleInventory() {
    if (GetMyGameHUD()) {
        GetMyGameHUD()->SetInventoryOpen(GetMyGameHUD()->GetInventoryOpen() ? false : true);
    }
}

void AFPSCharacter::OnToggleCharacterPanel() {
    if (GetMyGameHUD()) {
        GetMyGameHUD()->SetCharacterPanelOpen(GetMyGameHUD()->GetCharacterPanelOpen() ? false : true);
    }
}

void AFPSCharacter::OnToggleCraftingManager() {
    if (GetMyGameHUD()) {
        GetMyGameHUD()->SetCraftingManagerOpen(GetMyGameHUD()->GetCraftingManagerOpen() ? false : true);
    }
}

void AFPSCharacter::ReceiveDestroyed() {
    UE_LOG(LogTemp, Log, TEXT("Char destroyed"));
}

void AFPSCharacter::OnFire() {
    if (GetMyGameHUD()->GetInventoryOpen() || GetMyGameHUD()->GetCharacterPanelOpen() || GetMyGameHUD()->GetCraftingManagerOpen()) {
        return;
    }

    if (ProjectileClass != NULL) {
        // get the camera transform
        FVector CameraLoc;
        FRotator CameraRot;
        GetActorEyesViewPoint(CameraLoc, CameraRot);

        // MuzzleOffset is in camera space, so transform it to world space before offsetting from the camera to find the final muzzle position
        FVector const MuzzleLocation = CameraLoc + FTransform(CameraRot).TransformVector(MuzzleOffset);
        FRotator MuzzleRotation = CameraRot;
        MuzzleRotation.Pitch += 10.0f; // skew the aim upwards a bit

        Server_OnFire(MuzzleLocation, MuzzleRotation.Vector());
    }
}

bool AFPSCharacter::Server_OnFire_Validate(FVector Origin, FVector_NetQuantizeNormal ShootDir) {
    return true;
}

void AFPSCharacter::Server_OnFire_Implementation(FVector Origin, FVector_NetQuantizeNormal ShootDir) {
    UWorld* const World = GetWorld();
    if (World) {
        // spawn the projectile at the muzzle
        AFPSProjectile* const Projectile = World->SpawnActor<AFPSProjectile>(ProjectileClass, Origin, ShootDir.Rotation());
        if (Projectile) {
            // find launch direction
            Projectile->Instigator = Instigator;
            Projectile->SetOwner(this);
            Projectile->InitVelocity(ShootDir);
        }
    }
}

void AFPSCharacter::OnAbortCrafting() {
    if (IsLocallyControlled()) {
        if (CraftingManager && CraftingManager->IsCrafting()) {
            UE_LOG(LogTemp, Log, TEXT("Aborting Crafting"));
            CraftingManager->AbortCrafting();
        }
    }
}

void AFPSCharacter::EquipItem1() {
    if (MyInventoryInstance && MyInventoryInstance->isInitialized) {
        // This is the first item, starting at 10 currently, cause 10 is not part of the CharacterPanel-Slots [0-9]
        MyInventoryInstance->EquipItem(AInventory::CHARACTERPANEL_SLOTS);
    }
}

void AFPSCharacter::EquipItem2() {
    if (MyInventoryInstance && MyInventoryInstance->isInitialized) {
        MyInventoryInstance->EquipItem(AInventory::CHARACTERPANEL_SLOTS + 1);
    }
}

void AFPSCharacter::EquipItem3() {
    if (MyInventoryInstance && MyInventoryInstance->isInitialized) {
        MyInventoryInstance->EquipItem(AInventory::CHARACTERPANEL_SLOTS + 2);
    }
}

void AFPSCharacter::EquipItem4() {
    if (MyInventoryInstance && MyInventoryInstance->isInitialized) {
        MyInventoryInstance->EquipItem(AInventory::CHARACTERPANEL_SLOTS + 3);
    }
}

void AFPSCharacter::EquipItem5() {
    if (MyInventoryInstance && MyInventoryInstance->isInitialized) {
        MyInventoryInstance->EquipItem(AInventory::CHARACTERPANEL_SLOTS + 4);
    }
}

void AFPSCharacter::OnItemCollectorOverlap(AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) {
    if (!IsLocallyControlled()) {
        return;
    }

    UE_LOG(LogTemp, Log, TEXT("Collide"));

    ABaseItem* PickupItem = Cast<ABaseItem>(OtherActor);

    if (PickupItem && !PickupItem->IsInInventory) {
        // TODO: There is a bug here, sometimes this is returned as not initialized, but why?
        if (!MyInventoryInstance) {
            UE_LOG(LogTemp, Log, TEXT("No Inventory instance"));
            return;
        }

        UE_LOG(LogTemp, Log, TEXT("... with PickupItem"));
        MyInventoryInstance->OnItemPickup(PickupItem);
    }
}

void AFPSCharacter::TestAction() {
    // Prototype for CharacterEmote
    if (!MyInventoryInstance || !MyInventoryInstance->isInitialized) {
        UE_LOG(LogTemp, Log, TEXT("No Inventory"));
        return;
    }
    if (MyInventoryInstance->GetEquipedItem() != nullptr && MyInventoryInstance->GetEquipedItem()->IsInUse) {
        UE_LOG(LogTemp, Log, TEXT("Equiped item in use"));
        return;
    }
    if (CharacterEmoteAnimation != EEmoteAnimation::EMOTE_NONE) {
        return;
    }

    if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Magenta, TEXT("Test Action")); }

    Server_SetEmote();

    if (GEngine) { FString GInfo = "Dat: "; GInfo.Append(FString::FromInt(MyInventoryInstance->InventoryContent.Num())); GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, GInfo); }

    // Also do this here to prevent server-client lag?
//    GetWorldTimerManager().SetTimer(this, &AFPSCharacter::ResetEmoteStatus, 1.2f, false);
}

bool AFPSCharacter::Server_SetEmote_Validate() {
    return true;
}

void AFPSCharacter::Server_SetEmote_Implementation() {
    if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Magenta, TEXT(">> Emote Action")); }

    if (!MyInventoryInstance || !MyInventoryInstance->isInitialized) {
        UE_LOG(LogTemp, Log, TEXT("No Inventory"));
        return;
    }
    if (CharacterEmoteAnimation != EEmoteAnimation::EMOTE_NONE) {
        return;
    }

    float EmoteLength = 5.2f;

    CharacterEmoteAnimation = EEmoteAnimation::EMOTE_FUCKYOU;
    GetWorldTimerManager().SetTimer(this, &AFPSCharacter::ResetEmoteStatus, EmoteLength, false);

    MyInventoryInstance->MakeEquipedItemInvisible(EmoteLength);
}

void AFPSCharacter::ResetEmoteStatus() {
    if (Role != ROLE_Authority) {
        return;
    }

    CharacterEmoteAnimation = EEmoteAnimation::EMOTE_NONE;
}

void AFPSCharacter::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);

//    if (Role != ROLE_Authority && MyInventoryInstance && MyInventoryInstance->isInitialized) {
//        if (MyInventoryInstance->GetEquipedItem()) {
//            if (GEngine) { FString LogString = "Currently holding: "; LogString.Append(MyInventoryInstance->GetEquipedItem()->ItemName); GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Magenta, LogString); }
//        }
//    }

    // Fire trace to find interactable objects
    if (IsLocallyControlled()) {
        TraceAccumulator += DeltaTime;
        if (TraceAccumulator >= 0.05f) {
            TraceAccumulator -= 0.05f;
            DoInteractableTrace();
        }
    }
}

void AFPSCharacter::DoInteractableTrace() {
    UWorld *MyWorld = GetWorld();

    if (!MyWorld) {
        return;
    }

    FVector CamLoc;
    FRotator CamRot;

    Controller->GetPlayerViewPoint(CamLoc, CamRot);
    const FVector DirectionVector = CamRot.Vector();
    const FVector StartTraceVector = CamLoc;
    const FVector EndTraceVector = StartTraceVector + DirectionVector * 325;

    FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("CHARACTER_INTERACT_SEARCH_TRACE")), true, this);
    RV_TraceParams.bTraceComplex = true;
    RV_TraceParams.bTraceAsyncScene = true;
    RV_TraceParams.bReturnPhysicalMaterial = false;
    RV_TraceParams.AddIgnoredActor(this);

    FHitResult RV_Hit(ForceInit);

    if (MyWorld->LineTraceSingle(RV_Hit, StartTraceVector, EndTraceVector, ECC_WorldDynamic, RV_TraceParams)) {
        // We hit something
        // DrawDebugLine(MyWorld, StartTraceVector, EndTraceVector, FColor(FMath::RandRange(0, 255), FMath::RandRange(0, 255), FMath::RandRange(0, 255)), false, 2, 0, 2);

        ABaseBuilding* Building = Cast<ABaseBuilding>(RV_Hit.GetActor());
        if (Building && !Building->GetBuilt() && Building->GetOwner() != this) {
            return;
        }

        IInteractableObjectInterface* InteractableObject = InterfaceCast<IInteractableObjectInterface>(RV_Hit.GetActor());
        if (InteractableObject) {
            InteractableObject->ShowInteractionInfoText(this);
            InteractableObject->SetHighlighted(0.05f);

            // Not used atm
//            if (GEngine) {
//                GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), InteractableObject->GetInteractionInfoText());
//            }
        }
    }
}

AActor* AFPSCharacter::TraceAndReturnActor() {
    UWorld *MyWorld = GetWorld();

    if (!MyWorld) {
        UE_LOG(LogTemp, Error, TEXT("TraceAndReturnActor(): No world"));
        return nullptr;
    }

    FVector CamLoc;
    FRotator CamRot;

    Controller->GetPlayerViewPoint(CamLoc, CamRot);
    const FVector DirectionVector = CamRot.Vector();
    const FVector StartTraceVector = CamLoc;
    const FVector EndTraceVector = StartTraceVector + DirectionVector * 325;

    FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("CHARACTER_INTERACT_SEARCH_TRACE")), true, this);
    RV_TraceParams.bTraceComplex = true;
    RV_TraceParams.bTraceAsyncScene = true;
    RV_TraceParams.bReturnPhysicalMaterial = false;
    RV_TraceParams.AddIgnoredActor(this);

    FHitResult RV_Hit(ForceInit);

    if (MyWorld->LineTraceSingle(RV_Hit, StartTraceVector, EndTraceVector, ECC_WorldDynamic, RV_TraceParams)) {
        //DrawDebugLine(MyWorld, StartTraceVector, EndTraceVector, FColor(FMath::RandRange(0, 255), FMath::RandRange(0, 255), FMath::RandRange(0, 255)), false, 2, 0, 4);

        return RV_Hit.GetActor();
    }

    return nullptr;
}

void AFPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AFPSCharacter, CharacterEmoteAnimation);
    DOREPLIFETIME(AFPSCharacter, MyInventoryInstance);
    DOREPLIFETIME_CONDITION(AFPSCharacter, CraftingManager, COND_OwnerOnly);
}

AFPSPlayerController* AFPSCharacter::GetMyPlayerController() {
    return Cast<AFPSPlayerController>(Controller);
}

AGameHUD* AFPSCharacter::GetMyGameHUD() {
    AFPSPlayerController* OwningPlayerController = Cast<AFPSPlayerController>(Controller);
    AGameHUD* GameHUD = Cast<AGameHUD>(OwningPlayerController->GetHUD());
    return GameHUD;
}

void AFPSCharacter::OnRep_InvTest() {
    UE_LOG(LogTemp, Warning, TEXT("Replicating Inventory in Character"));
}
