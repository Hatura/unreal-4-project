#pragma once

class HaturHelper
{
public:
    template <typename T>
    static FString ConvertToTime(T t) {
        int PreComma = FPlatformMath::FloorToInt(t);

        float PostCommaFloat = t - PreComma;
        int PostComma = FPlatformMath::RoundToInt(PostCommaFloat * 100);

        FString ReturnString = FString::FromInt(PreComma);
        ReturnString.Append(".");
        ReturnString.Append(FString::FromInt(PostComma));
        ReturnString.Append(" s");

        return ReturnString;
    }

    // Quick log functions
    static void QL(FString LogText);
    static void QL(FString LogText, FString StringAddition);
    static void QL(FString LogText, int32 IntAddition);
    static void QL(FString LogText, float FloatAddition);
    static void QL(FString LogText, bool BoolAddition);
};
