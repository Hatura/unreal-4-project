#pragma once

#include "CraftableObjectInterface.generated.h"

/**
 * This is currently unused, delete if necessary
 */
UINTERFACE(MinimalAPI)
class UCraftableObjectInterface : public UInterface
{
    GENERATED_UINTERFACE_BODY()
};

class ICraftableObjectInterface
{
    GENERATED_IINTERFACE_BODY()
};
