#pragma once

#include "InteractableObjectInterface.generated.h"

// forward declaration
class AActor;

/**
  * Enum return type for interaction, to see in a child class if a parent class has handled interaction
  */
UENUM(BlueprintType)
namespace EInteractionResult {
    enum InteractionResult {
        IA_NOTINTERACTED,
        IA_INTERACTED,
        IA_ERROR
    };
}

/**
 * Interface for interactable Actors
 */
UINTERFACE(MinimalAPI, meta=(CannotImplementInterfaceInBlueprint))
class UInteractableObjectInterface : public UInterface
{
    GENERATED_UINTERFACE_BODY()
};

class IInteractableObjectInterface
{
    GENERATED_IINTERFACE_BODY()

    // PURe_VIRTUAL LowLevel warning 'no implementation' not showing up? https://answers.unrealengine.com/questions/28225/pure-virtual-methods.html

    UFUNCTION(BlueprintCallable, Category = "Interactable Object")
    virtual void ShowInteractionInfoText(AActor* ActorInteracting = nullptr) PURE_VIRTUAL(UInteractableObjectInterface::ShowInteractionInfoText, );

    // This is not directly associated to ShowInteractionInfotext, it needs to be manually called from overriden ShowInteractionInfoText with a timer or similar
    UFUNCTION(BlueprintCallable, Category = "Interactable Object")
    virtual void HideInteractionInfoText() PURE_VIRTUAL(UInteractableObjectInterface::HideInteractionInfoText, );

    UFUNCTION(BlueprintCallable, Category = "Interactable Object")
    virtual FString GetInteractionInfoText() PURE_VIRTUAL(UInteractableObjectInterface::GetInteractionInfoText, return "";);

    // Tell the object it is highlighted, should call Unhighlight per timer!
    UFUNCTION(BlueprintCallable, Category = "Interactable Object")
    virtual void SetHighlighted(float ForSeconds) PURE_VIRTUAL(UInteractableObjectInterface::SetHighlighted, );

    // If global interaction is wished the Interact needs to be called through the Server (See FPSCharacter->Server_OnInteract()), else only Owning Client + Server will be able to interact
    UFUNCTION(BlueprintCallable, Category = "Interactable Object")
    virtual TEnumAsByte<EInteractionResult::InteractionResult> Interact(AActor* ActorInteracting) PURE_VIRTUAL(UInteractableObjectInterface::Interact, return EInteractionResult::IA_ERROR;);

protected:
    // Needs to be called per timer!
    virtual void Unhighlight() PURE_VIRTUAL(UInteractableObjectInterface::Unhighlight, );
};
