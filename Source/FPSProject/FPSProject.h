#pragma once

#include "Engine.h"
#include "HaturHelper.h"

#define COLLISION_PROJECTILE 	ECC_GameTraceChannel1
#define COLLISION_LANDSCAPE     ECC_GameTraceChannel2
#define COLLISION_CONSTRUCTION  ECC_GameTraceChannel3
#define COLLISION_ITEM          ECC_GameTraceChannel4
#define COLLISION_ITEMCOLLECTOR ECC_GameTraceChannel5
