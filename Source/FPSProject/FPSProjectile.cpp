

#include "FPSProject.h"
#include "FPSProjectile.h"


AFPSProjectile::AFPSProjectile(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	// Use a sphere as a simple collision representation
	CollisionComp = PCIP.CreateDefaultSubobject<USphereComponent>(this, TEXT("SPhereComp"));
	CollisionComp->InitSphereRadius(15.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AFPSProjectile::OnHit);
    CollisionComp->AlwaysLoadOnClient = true;
    CollisionComp->AlwaysLoadOnServer = true;
    CollisionComp->bTraceComplexOnMove = true;
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = PCIP.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
    ProjectileMovement->bRotationFollowsVelocity = true;
    ProjectileMovement->bShouldBounce  = true;
    ProjectileMovement->Bounciness = 0.3f;

    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.TickGroup = TG_PrePhysics;
    SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);

    bReplicates = true;
    bReplicateInstigator = true;
    bReplicateMovement = true;
}

void AFPSProjectile::ReceiveDestroyed() {
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Destroyed"));
    }
}

void AFPSProjectile::InitVelocity(const FVector& ShootDirection) {
	if (ProjectileMovement) {
		// set the projectile's velocity to the desired direction
		ProjectileMovement->Velocity = ShootDirection * ProjectileMovement->InitialSpeed;
	}
}

void AFPSProjectile::OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {
//    switch (Role) {
//    case ROLE_None:
//        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("NONE"));
//        break;
//    case ROLE_SimulatedProxy:
//        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("SIM PROXY"));
//        break;
//    case ROLE_AutonomousProxy:
//        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("AUTO PROXY"));
//        break;
//    case ROLE_Authority:
//        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("AUTHORITY"));
//        break;
//    case ROLE_MAX:
//        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("MAX"));
//        break;
//    }

    if (OtherActor && (OtherActor != this) && OtherComp) {
		OtherComp->AddImpulseAtLocation(ProjectileMovement->Velocity * 100.0f, Hit.ImpactPoint);
	}
}

void AFPSProjectile::PostNetReceiveVelocity(const FVector& NewVelocity) {
    if (ProjectileMovement) {
        ProjectileMovement->Velocity = NewVelocity;
    }
}
