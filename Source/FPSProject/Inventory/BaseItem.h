#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/CraftableObjectInterface.h"
#include "Slate.h"
#include "BaseItem.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
namespace EItemType {
    enum ItemType {
        ITEM_NONE,
        ITEM_MATERIAL,
        ITEM_HEAD,
        ITEM_BREAST,
        ITEM_LEGS,
        ITEM_FOOT,
        ITEM_GLASSES,
        ITEM_TRINKET,
        ITEM_USABLE
    };
}

UENUM(BlueprintType)
namespace EItemRarity {
    enum ItemRarity {
        RARITY_UNDEFINED,
        RARITY_CRAP,
        RARITY_COMMON,
        RARITY_UNCOMMON,
        RARITY_RARE,
        RARITY_EPIC,
        RARITY_LEGENDARY
    };
}

UCLASS(Abstract)
class FPSPROJECT_API ABaseItem : public AActor, public ICraftableObjectInterface
{
    GENERATED_UCLASS_BODY()

    void PostInitializeComponents() override;

    virtual void ReceiveHit
        (
            class UPrimitiveComponent * MyComp,
            class AActor * Other,
            class UPrimitiveComponent * OtherComp,
            bool bSelfMoved,
            FVector HitLocation,
            FVector HitNormal,
            FVector NormalImpulse,
            const FHitResult & Hit
        ) override;

//    // Does this item use a USkeletalMeshComponent instead of a UStaticMeshComponent?
//    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
//    bool UsingSkeletalMesh;

    // This is the Droppedmesh and used for 3rd Person if no 3rd person skeletal mesh (below) is defined!
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
    TSubobjectPtr<UStaticMeshComponent> MeshDropped;

    // This is used for 3rd Person if the item has a 3rd person skeletal mesh!
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
    TSubobjectPtr<USkeletalMeshComponent> Mesh3P;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    bool Using3PSkeletalMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    TEnumAsByte<EItemRarity::ItemRarity> ItemRarity;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    FString ItemName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    FString DisplayName;

    // Item_Empty = NOT
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    bool IsManagableItem;

    // Can I hold this in my Hands?
//    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
//    bool IsEquipable;

    // Can be picked up after this amount of seconds have expired
    UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Item)
    float PickupReadyAfterSeconds;

    // Item type, used for armor etc. later
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    TEnumAsByte<EItemType::ItemType> MyItemType;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    bool IsStackable;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    float ItemWeight;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
    FSlateBrush SlateBrush;

    // Used for blueprint item atm
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    class UTexture2D* ItemTexture;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    int32 MaxStackSize;

    UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = Item)
    int32 CurrentStackSize;

    UPROPERTY(Replicated, BlueprintReadOnly, Category = Item)
    bool IsInInventory;

    // Property to hide/unhide the Static Mesh, replicated!
    UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_MeshDroppedVisiblityChanged, Category = "Usable Item")
    bool MeshDroppedVisibility;

    // Property to hide/unhide the third person Skeletal Mesh, replicated!
    UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_Mesh3PVisibilityChanged, Category = "Usable Item")
    bool Mesh3PVisibility;

    UFUNCTION()
    void SetPickedUp();

    // Disabling items is not just a visiblity (hiddeningame) change, it's deactivating the actor + setting location to zerovector!
    UFUNCTION(NetMulticast, Reliable, WithValidation)
    virtual void Multicast_DisableItem();

//    UFUNCTION(NetMulticast, Reliable, WithValidation)
//    void Multicast_ItemToWorld(FVector Position, FRotator Rotation);

    UFUNCTION()
    void OnRep_MeshDroppedVisiblityChanged();

    UFUNCTION()
    void OnRep_Mesh3PVisibilityChanged();

    bool IsPickupReady;

protected:
    UFUNCTION(BlueprintCallable, Category = Item)
    void SetPickupReady();
};
