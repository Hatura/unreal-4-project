#pragma once

#include "Inventory/BaseItem.h"
#include "Animation/AnimMontage.h"
#include "UsableItem.generated.h"

/**
  * These are default animation every usable item can have, but must not have! they are called within C++
  */
UENUM(BlueprintType)
namespace EMetaAnimation {
    enum MetaAnimation {
        META_NONE,
        META_2HWeapon,
        META_1H_STABWEAPON,
        META_BLUEPRINT
    };
}

/**
  * Item states defining animations, maybe other stuff but be careful
  */
UENUM(BlueprintType)
namespace EItemState {
    enum ItemState {
        IS_IDLE,
        IS_USE,
        IS_FIRE,
        IS_FIRE2,
        IS_RELOAD
    };
}

/**
 * Each usable item has to implement it's own Server methods
 * (Server_UseItem()), Weapons use Server_OnFire instead.
 */
UCLASS(Abstract)
class FPSPROJECT_API AUsableItem : public ABaseItem
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
    TSubobjectPtr<USkeletalMeshComponent> Mesh1P;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Item)
    bool Using1PSkeletalMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Usable Item")
    FName AttachmentSlotName;

    // Only used for usable items that are NOT weapons
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Usable Item")
    float AnimationToIdleTime;

    // The time after which the actual action is performed,
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Usable Item")
    float TimeBeforeActionProcessed;

    // Cooldown, time after fire in weapon is called again if mouse is down, or pause time for one click items/weapons to use
    // for Usable items that are not weapons this needs to be > (AnimationsToIdleTime + TimeBeforeActionProcessed)
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Usable Item")
    float ItemCooldown;

    UPROPERTY(BlueprintReadOnly, Replicated, Category = "Usable Item")
    bool IsOnCooldown;

    // Is doing work, can't change weapon then etc. | NOT REPLICATED
    UPROPERTY(BlueprintReadOnly, Category = "Usable Item")
    bool IsInUse;

    /**
      * Animation
      */
    // Set to determine the type of the meta animation in the state machine handler
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
    TEnumAsByte<EMetaAnimation::MetaAnimation> MetaAnimationType;

    // WeaponState, set by c++ and fetched by blueprint to play section of animation!
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = Animation)
    TEnumAsByte<EItemState::ItemState> ItemState;

    // With item associated montageparody
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
    UAnimMontage* AnimationMP;
    /**
      * - Animation
      */

    // Property to hide/unhide the first person Skeletal Mesh, replicated!
    UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_Mesh1PVisibilityChanged, Category = "Usable Item")
    bool Mesh1PVisibility;

    bool Multicast_DisableItem_Validate();
    void Multicast_DisableItem_Implementation();

    // Only called for usable Items that are NOT Weapons
    UFUNCTION(BlueprintCallable, Category = "Usable Item")
    virtual void UseItem();

    UFUNCTION(Server, Reliable, WithValidation)
    virtual void Server_UseItem();

    // Called per timer from UseItem() - A puffer to set animation alerady to idle and wait AnimationToIdleTime-length before processing consume
    UFUNCTION()
    virtual void WaitForIdleAnimationReach();

    // Called per timer from WaitForIdleAnimationReach()
    UFUNCTION()
    virtual void ConsumeItem();

    // Consume and remove item if stacksize is zero
    UFUNCTION(Server, Reliable, WithValidation)
    virtual void Server_ConsumeItem();

    // Server only! Call Consume effect, override this for consume effects
    UFUNCTION()
    virtual void OnConsume();

    UFUNCTION(BlueprintCallable, Category = "Usable Item")
    virtual void FreeCooldown();

    // Hide visible Static/Skeletal meshes for a period of time, used for emotes atm
    UFUNCTION(BlueprintCallable, Category = "Usable Item")
    void MakeTemporarelyInvisible(float DurationInSeconds);

    // Delegate functions called per MakeTemporarelyInvisible(), FTimerDelegate::BindRaw_ThreeVars does not exist?
    UFUNCTION()
    void MakeMeshDroppedVisibleAgain();

    UFUNCTION()
    void MakeMesh1PVisibleAgain();

    UFUNCTION()
    void MakeMesh3PVisibleAgain();

    UFUNCTION()
    void OnRep_Mesh1PVisibilityChanged();

//    UFUNCTION(BlueprintCallable, Category = "Usable Item")
//    virtual void DetermineItemState();
};
