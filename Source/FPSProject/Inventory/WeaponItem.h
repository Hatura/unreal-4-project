#pragma once

#include "Inventory/UsableItem.h"
#include "WeaponItem.generated.h"

/**
 * Reminder: This is an abstract class and children of this class have to implement
 * their own Server communication, Weapons have to implement Server_FireWeapon, Server_ReloadWeapon etc.
 */
UCLASS(Abstract)
class FPSPROJECT_API AWeaponItem : public AUsableItem
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    bool Reloadable;

    UFUNCTION(BlueprintCallable, Category = "Weapon")
    virtual void ReloadWeapon();

    UFUNCTION(BlueprintCallable, Category = "Weapon")
    virtual void StartFire();

    UFUNCTION(BlueprintCallable, Category = "Weapon")
    virtual void StopFire();

    //void DetermineItemState() override;

protected:
    UPROPERTY(BlueprintReadOnly, Category = "Weapon")
    bool IsFiring;
};
