#include "FPSProject.h"
#include "Crafting/BaseRecipe.h"
#include "Engine/CanvasRenderTarget2D.h"
#include "RenderUtils.h"
#include "FPSWorldSettings.h"
#include "FPSCharacter.h"
#include "FPSPlayerController.h"
#include "BlueprintUsable.h"


ABlueprintUsable::ABlueprintUsable(const class FPostConstructInitializeProperties& PCIP)
    : Super(PCIP)
{
    MetaAnimationType = EMetaAnimation::META_BLUEPRINT;

    PrimaryActorTick.bCanEverTick = true;

    SurfaceWidth = 512;
    SurfaceHeight = 512;
}

void ABlueprintUsable::PostInitializeComponents() {
    Super::PostInitializeComponents();

    RecipeDisplayName = FText::FromString(TeachingRecipe.GetDefaultObject()->DisplayName);

    ObjectTexture = DefaultTexture;

    if (TeachingRecipe.GetDefaultObject()->CreatesObjectType == ECreatesObjectType::OBJECT_ITEM) {
        ObjectRarity = TeachingRecipe.GetDefaultObject()->CreatesItem.GetDefaultObject()->ItemRarity;
        if (TeachingRecipe.GetDefaultObject()->CreatesItem.GetDefaultObject()->ItemTexture != nullptr) {
            ObjectTexture = TeachingRecipe.GetDefaultObject()->CreatesItem.GetDefaultObject()->ItemTexture;
        }
    }
    else if (TeachingRecipe.GetDefaultObject()->CreatesObjectType == ECreatesObjectType::OBJECT_BUILDING) {
        ObjectRarity = TeachingRecipe.GetDefaultObject()->CreatesBuilding.GetDefaultObject()->BuildingRarity;
        if (TeachingRecipe.GetDefaultObject()->CreatesBuilding.GetDefaultObject()->BuildingTexture != nullptr) {
            ObjectTexture = TeachingRecipe.GetDefaultObject()->CreatesBuilding.GetDefaultObject()->BuildingTexture;
        }
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("Critical! ABlueprintUsable::PostInitializeComponents(): ObjectType is not legit"));
        ObjectRarity = EItemRarity::RARITY_COMMON;
    }
}

void ABlueprintUsable::BeginPlay() {
    Super::BeginPlay();
    CanvasTargetFront = UCanvasRenderTarget2D::CreateCanvasRenderTarget2D(UCanvasRenderTarget2D::StaticClass(), SurfaceWidth, SurfaceHeight);
    CanvasTargetFront->OnCanvasRenderTargetUpdate.AddDynamic(this, &ABlueprintUsable::OnReceiveUpdateFront);
    CanvasTargetFront->UpdateResource();

    CanvasTargetPaper = UCanvasRenderTarget2D::CreateCanvasRenderTarget2D(UCanvasRenderTarget2D::StaticClass(), SurfaceWidth, SurfaceHeight);
    CanvasTargetPaper->OnCanvasRenderTargetUpdate.AddDynamic(this, &ABlueprintUsable::OnReceiveUpdatePaper);
    CanvasTargetPaper->UpdateResource();

    CanvasTargetBack = UCanvasRenderTarget2D::CreateCanvasRenderTarget2D(UCanvasRenderTarget2D::StaticClass(), SurfaceWidth, SurfaceHeight);
    CanvasTargetBack->OnCanvasRenderTargetUpdate.AddDynamic(this, &ABlueprintUsable::OnReceiveUpdateBack);
    CanvasTargetBack->UpdateResource();

    if (MaterialInstanceFront != nullptr) {
        MaterialInstanceFront->SetTextureParameterValue(ParameterName_FrontBack, CanvasTargetFront);
    }

    if (MaterialInstancePaper != nullptr) {
        MaterialInstancePaper->SetTextureParameterValue(ParameterName_Paper, CanvasTargetPaper);
    }

    if (MaterialInstanceBack != nullptr) {
        MaterialInstanceBack->SetTextureParameterValue(ParameterName_FrontBack, CanvasTargetBack);
    }
}

void ABlueprintUsable::OnReceiveUpdateFront(UCanvas *Canvas, int32 Width, int32 Height) {
    if (Canvas != nullptr) {
        AWorldSettings* WorldSettings = GetWorldSettings();
        if (!WorldSettings) {
            UE_LOG(LogTemp, Error, TEXT("OnRecieveUpdateFront(): No World Settings!"));
            return;
        }

        AFPSWorldSettings* FPSWorldSettings = Cast<AFPSWorldSettings>(WorldSettings);
        if (!FPSWorldSettings) {
            UE_LOG(LogTemp, Error, TEXT("OnReceiveUpdateFrnot(): No World Settings!"));
            return;
        }

        FCanvasTileItem BackgroundColor = FCanvasTileItem(FVector2D(0.0f, 0.0f), GWhiteTexture, FVector2D(Width, Height), FPSWorldSettings->GetColorForRarity(ObjectRarity));
        Canvas->DrawItem(BackgroundColor);

        FCanvasTextItem TextItem = FCanvasTextItem(FVector2D(174.0f, 0.0f), FText::FromString("Survival for Humans"), HeadlineFont, HeadlineColor);
        TextItem.BlendMode = SE_BLEND_MaskedDistanceField;
        TextItem.Scale = FVector2D(HeadlineScale, HeadlineScale);
        TextItem.bCentreX = true;

        Canvas->DrawItem(TextItem);
    }
}

void ABlueprintUsable::OnReceiveUpdatePaper(UCanvas *Canvas, int32 Width, int32 Height) {
    if (Canvas != nullptr) {
        FCanvasTileItem BackgroundColor = FCanvasTileItem(FVector2D(0.0f, 0.0f), GWhiteTexture, FVector2D(Width, Height), PaperColor);
        Canvas->DrawItem(BackgroundColor);

        FCanvasTextItem TextItem = FCanvasTextItem(FVector2D(16.0f, 16.0f), RecipeDisplayName, HeadlineFont, HeadlineColor);
        TextItem.BlendMode = SE_BLEND_MaskedDistanceField;
        TextItem.Scale = FVector2D(HeadlineScale, HeadlineScale);

        Canvas->DrawItem(TextItem);

        TextItem = FCanvasTextItem(FVector2D(16.0f, 50.0f), FText::FromString("Some text to describe item\n\nmore text"), DefaultTextFont, DefaultTextColor);
        TextItem.BlendMode = SE_BLEND_Translucent;
        TextItem.Scale = FVector2D(DefaultTextScale, DefaultTextScale);

        Canvas->DrawItem(TextItem);

        FCanvasTileItem TileTexture = FCanvasTileItem(FVector2D(110.0f, 150.0f), ObjectTexture->Resource, FVector2D(128.0f, 128.0f), FLinearColor(1.0f, 1.0f, 1.0f));
        TileTexture.BlendMode = SE_BLEND_Translucent;

        Canvas->DrawItem(TileTexture);
    }
}

void ABlueprintUsable::OnReceiveUpdateBack(UCanvas *Canvas, int32 Width, int32 Height) {
    if (Canvas != nullptr) {
        AWorldSettings* WorldSettings = GetWorldSettings();
        if (!WorldSettings) {
            UE_LOG(LogTemp, Error, TEXT("OnRecieveUpdateFront(): No World Settings!"));
            return;
        }

        AFPSWorldSettings* FPSWorldSettings = Cast<AFPSWorldSettings>(WorldSettings);
        if (!FPSWorldSettings) {
            UE_LOG(LogTemp, Error, TEXT("OnReceiveUpdateFrnot(): No World Settings!"));
            return;
        }

        FCanvasTileItem BackgroundColor = FCanvasTileItem(FVector2D(0.0f, 0.0f), GWhiteTexture, FVector2D(Width, Height), FPSWorldSettings->GetColorForRarity(ObjectRarity));
        Canvas->DrawItem(BackgroundColor);
    }
}
void ABlueprintUsable::UseItem() {
    ItemState = EItemState::IS_USE;

    Super::UseItem();
}

void ABlueprintUsable::WaitForIdleAnimationReach() {
    ItemState = EItemState::IS_IDLE;

    Super::WaitForIdleAnimationReach();
}

void ABlueprintUsable::OnConsume() {
    HaturHelper::QL("Consumerino!");

    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(GetOwner());

    if (!ActualCharacter) {
        UE_LOG(LogTemp, Error, TEXT("ABlueprintUsable::OnConsume(): No Character"));
        return;
    }

    AFPSPlayerController* PCOwner = Cast<AFPSPlayerController>(ActualCharacter->GetController());

    if (!PCOwner) {
        UE_LOG(LogTemp, Error, TEXT("ABlueprintUsable::OnConsume(): No Player Controller"));
        return;
    }

    if (!PCOwner->MyRMInstance || !PCOwner->MyRMInstance->IsRMInitialized) {
        UE_LOG(LogTemp, Error, TEXT("ABlueprintUsable::OnConsume(): Recipe manager not initialized"));
        return;
    }

    PCOwner->MyRMInstance->TeachRecipe(TeachingRecipe);
}

void ABlueprintUsable::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);
}
