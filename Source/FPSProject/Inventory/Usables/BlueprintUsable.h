

#pragma once

#include "Inventory/UsableItem.h"
#include "BlueprintUsable.generated.h"

// forward declaration
class ABaseRecipe;

/**
 * 
 */
UCLASS(Abstract)
class FPSPROJECT_API ABlueprintUsable : public AUsableItem
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(BlueprintReadOnly, Category = "Blueprint Item")
    bool IsUsed;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Blueprint Item")
    TSubclassOf<class ABaseRecipe> TeachingRecipe;

    UPROPERTY(BlueprintReadOnly, Category = "Blueprint Item")
    FText RecipeDisplayName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    FLinearColor PaperColor;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    class UFont* HeadlineFont;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    float HeadlineScale;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    FLinearColor HeadlineColor;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    class UFont* DefaultTextFont;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    float DefaultTextScale;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    FLinearColor DefaultTextColor;

    // Used if ItemTexture or BuildingTexture == nullptr
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    class UTexture2D* DefaultTexture;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    float ItemTextureScale;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    int32 SurfaceWidth;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dynamic Material Render")
    int32 SurfaceHeight;

    UPROPERTY(BlueprintReadWrite, Category = "Dynamic Material Render")
    class UMaterialInstanceDynamic* MaterialInstanceFront;

    UPROPERTY(BlueprintReadWrite, Category = "Dynamic Material Render")
    class UMaterialInstanceDynamic* MaterialInstancePaper;

    UPROPERTY(BlueprintReadWrite, Category = "Dynamic Material Render")
    class UMaterialInstanceDynamic* MaterialInstanceBack;

    UPROPERTY(EditDefaultsOnly, Category = "Dynamic Material Render")
    FName ParameterName_FrontBack;

    UPROPERTY(EditDefaultsOnly, Category = "Dynamic Material Render")
    FName ParameterName_Paper;

    void PostInitializeComponents() override;

    void BeginPlay() override;

    UFUNCTION()
    void OnReceiveUpdateFront(class UCanvas* Canvas, int32 Width, int32 Height);

    UFUNCTION()
    void OnReceiveUpdatePaper(class UCanvas* Canvas, int32 Width, int32 Height);

    UFUNCTION()
    void OnReceiveUpdateBack(class UCanvas* Canvas, int32 Width, int32 Height);
	
    void Tick(float DeltaTime) override;

    void UseItem() override;

    void WaitForIdleAnimationReach() override;

    void OnConsume() override;

protected:
    UPROPERTY()
    class UCanvasRenderTarget2D* CanvasTargetFront;

    UPROPERTY()
    class UCanvasRenderTarget2D* CanvasTargetPaper;

    UPROPERTY()
    class UCanvasRenderTarget2D* CanvasTargetBack;

    TEnumAsByte<EItemRarity::ItemRarity> ObjectRarity;
    UTexture2D* ObjectTexture;
};
