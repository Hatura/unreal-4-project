#pragma once

#include "InstantWeapon.h"
#include "Axe.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class FPSPROJECT_API AAxe : public AInstantWeapon
{
	GENERATED_UCLASS_BODY()

protected:
    void UseAsATool(AActor* Initiator, AActor* Target) override;

    void UseAsAWeapon(AActor* Initiator, const FHitResult &Impact) override;
};
