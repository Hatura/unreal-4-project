

#pragma once

#include "Inventory/WeaponItem.h"
#include "ProjectileWeapon.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class FPSPROJECT_API AProjectileWeapon : public AWeaponItem
{
	GENERATED_UCLASS_BODY()

    UFUNCTION(BlueprintCallable, Category = "Projectile Weapon")
    virtual void FireWeapon_Projectile(FVector Origin, FVector_NetQuantizeNormal ShootDirection);
	
};
