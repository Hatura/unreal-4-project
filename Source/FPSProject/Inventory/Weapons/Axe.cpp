#include "FPSProject.h"
#include "FPSCharacter.h"
#include "IAFoliage/SimpleResourceGatherPoint.h"
#include "Axe.h"

AAxe::AAxe(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
//    ThirdPersonAnimation = E_TP_AnimationType::TP_TWOHANDWEAPON;
//    FirstPersonAnimation = E_FP_AnimationType::FP_TWOHANDWEAPON;
    MetaAnimationType = EMetaAnimation::META_2HWeapon;
}

void AAxe::UseAsATool(AActor* Initiator, AActor* Target) {
    UE_LOG(LogTemp, Log, TEXT("Using Woodaxe as a tool"));

    // Reminder: This is a Server function, so basically I could just add a check here if the other Actor is a Resource-Gather-Point and spawn stuff based on that information

    ASimpleResourceGatherPoint* SimpleResourceGatherPoint = Cast<ASimpleResourceGatherPoint>(Target);
    if (SimpleResourceGatherPoint) {
        HaturHelper::QL("Target is SimpleResourceGatherPoint");

        if (SimpleResourceGatherPoint->ResourceType == ESRGP_Type::PALMWOOD) {
            HaturHelper::QL("It's awarding PALMWOOD");
            HaturHelper::QL("Number of instances: ", SimpleResourceGatherPoint->MeshIndex.Num());

        }
    }

    Super::UseAsATool(Initiator, Target);
}

void AAxe::UseAsAWeapon(AActor *Initiator, const FHitResult& Impact) {
    UE_LOG(LogTemp, Log, TEXT("Using Woodaxe as a weapon"));

    Super::UseAsAWeapon(Initiator, Impact);
}
