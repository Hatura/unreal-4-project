

#pragma once

#include "Inventory/WeaponItem.h"
#include "InstantWeapon.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class FPSPROJECT_API AInstantWeapon : public AWeaponItem
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    float TraceRange;

    void StartFire() override;
	
    // Setting up a timer
    UFUNCTION(BlueprintCallable, Category = "Weapon")
    virtual void FireWeapon_Trace();

    // The actual processing
    UFUNCTION()
    virtual void FireWeapon_Trace_Process();

    UFUNCTION(Server, Reliable, WithValidation)
    virtual void Server_ConfirmHit(const FHitResult Impact, FVector_NetQuantizeNormal ShootDir, AActor* Initiator);

    void FreeCooldown() override;

protected:
    AActor *TraceInitiator;

    /*
     * These 2 Functions are only called serverside
     */
    UFUNCTION()
    virtual void UseAsATool(AActor* Initiator, AActor* Target);

    UFUNCTION()
    virtual void UseAsAWeapon(AActor* Initiator, const FHitResult &Impact);
};
