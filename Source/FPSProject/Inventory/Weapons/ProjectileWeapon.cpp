

#include "FPSProject.h"
#include "ProjectileWeapon.h"


AProjectileWeapon::AProjectileWeapon(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}

void AProjectileWeapon::FireWeapon_Projectile(FVector Origin, FVector_NetQuantizeNormal ShootDirection) {
    // Default implementation
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), TEXT("AProjectileWeapon::FireWeapon - Projectile"));
    }
}
