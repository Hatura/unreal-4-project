

#include "FPSProject.h"
#include "FPSCharacter.h"
#include "FPSGameMode.h"
#include "InstantWeapon.h"


AInstantWeapon::AInstantWeapon(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    TraceRange = 200.0f;
}

void AInstantWeapon::StartFire() {
    Super::StartFire();

    if (!IsOnCooldown) {
        FireWeapon_Trace();
    }
}

void AInstantWeapon::FireWeapon_Trace() {
    // Default implementation
    // Set up the trace timer, cause it needs a delay (Animation)

    // Owner = Inventory, Owner of Owner (Inventory) = Character
    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(GetOwner());
    if (!ActualCharacter || !ActualCharacter->IsAlive) {
        // This already checks whether the owner is a character, if not its not going to continue firing
        UE_LOG(LogTemp, Warning, TEXT("Owner no Character or Character dead when trying to use weapon"));
        IsFiring = false;
        return;
    }

    // If timer was set but action got aborted, abort here!
    if (!IsFiring) {
        return;
    }

    TraceInitiator = ActualCharacter;

    GetWorldTimerManager().SetTimer(this, &AInstantWeapon::FireWeapon_Trace_Process, TimeBeforeActionProcessed, false);
    GetWorldTimerManager().SetTimer(this, &AInstantWeapon::FreeCooldown, ItemCooldown, false);

    // Set that is in use, preventing events
    IsOnCooldown = true;
    IsInUse = true;
    ItemState = EItemState::IS_FIRE;
}

void AInstantWeapon::FireWeapon_Trace_Process() {
    // Default implementation
    if (!TraceInitiator) {
        UE_LOG(LogTemp, Warning, TEXT("Trace Initiator is non-existant"));
        IsFiring = false;
        IsInUse = false;
        return;
    }

    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(TraceInitiator);

    if (!ActualCharacter || !ActualCharacter->IsAlive) {
        UE_LOG(LogTemp, Error, TEXT("Trace Initiator is no Character or Character is dead"));
        IsFiring = false;
        IsInUse = false;
        return;
    }

    UWorld* MyWorld = GetWorld();
    if (!MyWorld) {
        UE_LOG(LogTemp, Error, TEXT("World not present when trying to initiate Trace"));
        IsFiring = false;
        IsInUse = false;
        return;
    }

    if (!ActualCharacter->Controller) {
        UE_LOG(LogTemp, Warning, TEXT("Character has no valid Controller!"));
        IsFiring = false;
        IsInUse = false;
        return;
    }

    FVector CamLoc;
    FRotator CamRot;

    ActualCharacter->Controller->GetPlayerViewPoint(CamLoc, CamRot);
    const FVector DirectionVector = CamRot.Vector();
    const FVector StartTraceVector = CamLoc;
    const FVector EndTraceVector = StartTraceVector + DirectionVector * 200;

    FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("WEAPON_RV_TRACE")), true, this);
    RV_TraceParams.bTraceComplex = true;
    RV_TraceParams.bTraceAsyncScene = true;
    RV_TraceParams.bReturnPhysicalMaterial = false;
    RV_TraceParams.AddIgnoredActor(ActualCharacter);

    FHitResult RV_Hit(ForceInit);

    if (MyWorld->LineTraceSingle(RV_Hit, StartTraceVector, EndTraceVector, ECC_WorldDynamic, RV_TraceParams)) {
        // We hit something?
        DrawDebugLine(MyWorld, StartTraceVector, EndTraceVector, FColor(255, 0, 0), false, 3, 0, 1);
        if (!RV_Hit.GetActor()) {
            UE_LOG(LogTemp, Error, TEXT("Trace-Hit, but Actor is null?"));
            IsInUse = false;
            return;
        }

        Server_ConfirmHit(RV_Hit, DirectionVector, TraceInitiator);
        IsInUse = false;
    }
    else {
        // Hit nothing
        IsInUse = false;
    }

    // Fire still being held? repeat action
//    if (IsFiring) {
//        float Waittime = ItemCooldown - TimeBeforeActionProcessed;
//        if (Waittime < 0) {
//            Waittime = 0;
//        }
//        GetWorldTimerManager().SetTimer(this, &AInstantWeapon::FireWeapon_Trace, Waittime, false);
//    }

    ItemState = EItemState::IS_IDLE;
}


bool AInstantWeapon::Server_ConfirmHit_Validate(const FHitResult Impact, FVector_NetQuantizeNormal ShootDir, AActor* Initiator) {
    return true;
}

void AInstantWeapon::Server_ConfirmHit_Implementation(const FHitResult Impact, FVector_NetQuantizeNormal ShootDir, AActor* Initiator) {
    if (Role != ROLE_Authority) {
        UE_LOG(LogTemp, Error, TEXT("Trace ConfirmHit(): I am not the server!"));
        return;
    }

    AFPSCharacter* CharacterInitiator = Cast<AFPSCharacter>(Initiator);
    if (!CharacterInitiator || !CharacterInitiator->IsAlive) {
        UE_LOG(LogTemp, Warning, TEXT("Trace ConfirmHit(): No Character or Character dead"));
        return;
    }

    if (!Impact.GetActor()) {
        UE_LOG(LogTemp, Log, TEXT("Trace ConfirmHit(): Hit object non-existant"));
        return;
    }

    UWorld* MyWorld = GetWorld();
    if (!MyWorld) {
        UE_LOG(LogTemp, Error, TEXT("InstantWeapon Server_ConfirmHit(): No World present!"));
        return;
    }

    AFPSGameMode* GameMode = Cast<AFPSGameMode>(MyWorld->GetAuthGameMode());
    if (!GameMode) {
        UE_LOG(LogTemp, Error, TEXT("Couldn't access GameMode"));
        return;
    }

    FActorSpawnParameters SpawnParams;
    SpawnParams.Owner = GameMode;
    SpawnParams.Instigator = Instigator;
    TSubclassOf<ABaseItem> ItemToSpawn = GameMode->InventoryClass.GetDefaultObject()->GetItem("Item_Wood_Palm_Block");
    ABaseItem* SpawnedItem = MyWorld->SpawnActor<ABaseItem>(ItemToSpawn, Impact.ImpactPoint, FRotator(0, 0, 0));
    //SpawnedItem->SetOwner(GameMode);
    //SpawnedItem->CurrentStackSize = 1;
    //SpawnedItem->Multicast_ItemToWorld(Impact.GetActor()->GetActorLocation(), Impact.GetActor()->GetActorRotation());
    //CharacterInitiator->TeleportTo(Impact.ImpactPoint, FRotator(0, 0, 0), false, true);

//    if (GEngine) {
//        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), Impact.GetActor()->GetActorLocation().ToString().Append(Impact.GetActor()->GetActorRotation().ToString()));
//    }

//    if (GEngine) {
//        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), TEXT("AInstantWeapon::Server_FireWeapon()"));
//    }

    if (true) {
        UseAsATool(Initiator, Impact.GetActor());
        UseAsAWeapon(Initiator, Impact);
    }

    // Add security checks
}

void AInstantWeapon::UseAsATool(AActor* Initiator, AActor* Target) {
    // Default implementation
}

void AInstantWeapon::UseAsAWeapon(AActor* Initiator, const FHitResult& Impact) {
    // Default implementation
}

void AInstantWeapon::FreeCooldown() {
    Super::FreeCooldown();

    if (IsFiring) {
        FireWeapon_Trace();
    }
}
