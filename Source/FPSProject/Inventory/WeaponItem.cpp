#include "FPSProject.h"
#include "WeaponItem.h"


AWeaponItem::AWeaponItem(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    // Default variable initializations
    Reloadable = true;
}

void AWeaponItem::ReloadWeapon() {
    // Default implementation
}

void AWeaponItem::StartFire() {
    // Default implementation
    IsFiring = true;
}

void AWeaponItem::StopFire() {
    // Default implementation
    IsFiring = false;
}

//void AWeaponItem::DetermineItemState() {
//    if (IsFiring) {
//        ItemState = EItemState::IS_FIRE;
//    }
//    else {
//        ItemState = EItemState::IS_IDLE;
//    }
//}
