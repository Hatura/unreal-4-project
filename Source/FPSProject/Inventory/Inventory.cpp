#include "FPSProject.h"
#include "OutputDevice.h"
#include "FPSCharacter.h"
#include "UnrealNetwork.h"
#include "FPSGameMode.h"
#include "Inventory.h"

// TODO: Set Instigator of all creates items to creator, so it's retracable (this is independent form Owner)!

TArray<FInventoryFieldFlag> AInventory::InventoryFlags;

AInventory::AInventory(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    bReplicates = true;
    bAlwaysRelevant = true;

    if (Role == ROLE_Authority) {
        PrimaryActorTick.bCanEverTick = true;
    }

    if (AInventory::InventoryFlags.Num() == 0) {
        InitInventoryFlags();
    }
}

TSubclassOf<class ABaseItem> AInventory::GetItem(FString ItemName) {
    if (ItemClasses.Num() > 0) {
        for (int i = 0; i < ItemClasses.Num(); i++) {
            if (ItemName == ItemClasses[i].ItemName) {
                return ItemClasses[i].Item;
            }
        }
    }
    else {

    }

    FString ErrorMessage = TEXT("Did not find an item with the name ");
    ErrorMessage.Append(ItemName);

    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Green, ErrorMessage);
    }

    return NULL;
}

// Only called on the Server
bool AInventory::Server_InitializeInventory_Validate(UWorld *InitWorld) {
    return true;
}

void AInventory::Server_InitializeInventory_Implementation(UWorld * InitWorld) {
    if (Role != ROLE_Authority) {
        // fail safe
        UE_LOG(LogTemp, Error, TEXT("Can't initialize inventory, am not the server!"));
        return;
    }

    ParentWorld = InitWorld;

    isInitialized = false;
    if (ParentWorld) {
        InventoryContent.Reset();
        for (int i = 0; i < (CHARACTERPANEL_SLOTS + HOTBAR_SLOTS + INVENTORY_SLOTS); i++) {
            TSubclassOf<ABaseItem> ItemToSpawn = GetItem("Item_Empty");
            ABaseItem* SpawnedItem = ParentWorld->SpawnActor<ABaseItem>(ItemToSpawn);
            SpawnedItem->SetOwner(GetOwner());
            SpawnedItem->CurrentStackSize = 0;
            InventoryContent.Add(SpawnedItem);
        }

        NumberOfEquipedItem = -1; // no equipped item

        isInitialized = true;
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("Couldn't get World"));
        FMessageDialog::Open(EAppMsgType::Ok, FText::FromString(TEXT("World not present!")));
    }
}

void AInventory::InitInventoryFlags() {
    // Init flags for inventory mask filter
    AInventory::InventoryFlags.Reset();
    for (int32 i = 0; i < CHARACTERPANEL_SLOTS + HOTBAR_SLOTS + INVENTORY_SLOTS; i++) {
        FInventoryFieldFlag FieldFlag;
        if (i == 0) {
            FieldFlag.Flags.Add(EInventoryFieldValue::FLAG_HELMETARMOR);
        }
        else if (i == 1) {
            FieldFlag.Flags.Add(EInventoryFieldValue::FLAG_SUNGLASSES);
        }
        else if (i == 2) {
            FieldFlag.Flags.Add(EInventoryFieldValue::FLAG_BREASTARMOR);
        }
        else if (i == 3) {
            FieldFlag.Flags.Add(EInventoryFieldValue::FLAG_PANTSARMOR);
        }
        else if (i == 4) {
            FieldFlag.Flags.Add(EInventoryFieldValue::FLAG_BOOTSARMOR);
        }
        else if (i > 4 && i < CHARACTERPANEL_SLOTS) {
            FieldFlag.Flags.Add(EInventoryFieldValue::FLAG_TRINKET);
        }
        else if (i >= CHARACTERPANEL_SLOTS && i < CHARACTERPANEL_SLOTS + HOTBAR_SLOTS) {
            FieldFlag.Flags.Add(EInventoryFieldValue::FLAG_USABLE);
        }
        else {
            FieldFlag.Flags.Add(EInventoryFieldValue::FLAG_EVERYTHING);
        }

        AInventory::InventoryFlags.Add(FieldFlag);
    }
}

void AInventory::OnItemPickup(ABaseItem* BaseItemInvolved) {
    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(GetOwner());
    if (!ActualCharacter) {
        UE_LOG(LogTemp, Error, TEXT("Actor trying to pickup item is NOT a FPSCharacter!"));
    }
    if (!BaseItemInvolved->IsPickupReady) {
        return;
    }

    AInventory::Server_OnItemPickup(BaseItemInvolved);
}

bool AInventory::Server_OnItemPickup_Validate(ABaseItem* BaseItemInvolved) {
    return true;
}

void AInventory::Server_OnItemPickup_Implementation(ABaseItem* BaseItemInvolved) {
    AFPSCharacter * ActualCharacter = Cast<AFPSCharacter>(GetOwner());
    if (!ActualCharacter) {
        UE_LOG(LogTemp, Error, TEXT("Actor trying to pickup item is NOT a FPSCharacter (ServerFunction)!"));
        return;
    }
    if (!ActualCharacter->IsAlive) {
        return;
    }
    if (!isInitialized) {
        UE_LOG(LogTemp, Error, TEXT("Inventory of Character trying to pickup is not initialized!"));
        return;
    }
    if (Role != ROLE_Authority) {
        UE_LOG(LogTemp, Error, TEXT("Im not the server!"));
        return;
    }
    if (!ParentWorld) {
        UE_LOG(LogTemp, Error, TEXT("No world assigned!"));
        return;
    }
    if (!BaseItemInvolved) {
        UE_LOG(LogTemp, Error, TEXT("Item non existant!"));
        return;
    }
    if (!BaseItemInvolved->IsPickupReady) {
        return;
    }
    if (BaseItemInvolved->IsInInventory) {
        UE_LOG(LogTemp, Warning, TEXT("Item already picked up!"));
        return;
    }
    if (BaseItemInvolved->CurrentStackSize <= 0) {
        UE_LOG(LogTemp, Warning, TEXT("Illegal Stacksize for Item collected (Stacksize <= 0)"));
        BaseItemInvolved->Destroy();
    }

    // TODO: Deathcheck, what is happening if the character gets deleted while inventory is being assigned?

    bool FoundPlaceToStoreItem = false;
    bool FilledWholeStack = false;

    AUsableItem* CheckUsableItem = Cast<AUsableItem>(BaseItemInvolved);

    // First of all, fill stacks if there are some, Starting with the first inventory slot or the first action bar slot (if the item is equipable)
    int32 StartingRangeIndicator;
    if (CheckUsableItem) {
        StartingRangeIndicator = CHARACTERPANEL_SLOTS;
    }
    else {
        StartingRangeIndicator = CHARACTERPANEL_SLOTS + HOTBAR_SLOTS;
    }
    for (int i = StartingRangeIndicator; i < INVENTORY_SLOTS; i++) {
        if (InventoryContent[i]->ItemName == BaseItemInvolved->ItemName) {
            if (InventoryContent[i]->CurrentStackSize + BaseItemInvolved->CurrentStackSize <= BaseItemInvolved->MaxStackSize) {
                InventoryContent[i]->CurrentStackSize += BaseItemInvolved->CurrentStackSize;
                FilledWholeStack = true;
                FoundPlaceToStoreItem = true;
                break;
            }
            else {
                int32 fillSize = BaseItemInvolved->MaxStackSize - InventoryContent[i]->CurrentStackSize;
                InventoryContent[i]->CurrentStackSize += fillSize;
                BaseItemInvolved->CurrentStackSize -= fillSize;
            }
        }
    }

    if (BaseItemInvolved->CurrentStackSize < 0) {
        UE_LOG(LogTemp, Error, TEXT("CRITICAL! BaseItemInvolved CurrentStackSize is negative!!"));
    }

    if (BaseItemInvolved->CurrentStackSize <= 0 || FilledWholeStack) {
        BaseItemInvolved->SetPickedUp();
        // Filled up stacks, destroy Item
        ParentWorld->DestroyActor(BaseItemInvolved);
        FoundPlaceToStoreItem = true;
    }

    if (!FoundPlaceToStoreItem) {
        for(int i = StartingRangeIndicator; i < INVENTORY_SLOTS; i++) {
            if (!InventoryContent[i]->IsManagableItem) {
                // No Managable Item == Item_Empty, Found EMPTY place to store
                InventoryContent[i]->Destroy(); // Destroy empty item
                BaseItemInvolved->SetOwner(GetOwner()); // CHANGED
                BaseItemInvolved->SetPickedUp();
                BaseItemInvolved->Multicast_DisableItem();
                InventoryContent[i] = BaseItemInvolved;
                UE_LOG(LogTemp, Error, TEXT("Placed Item in Inventory"));
                FoundPlaceToStoreItem = true;
                break;
            }
            /*else {
                if (ActualCharacter->MyInventoryInstance->InventoryContent[i]->ItemName == BaseItemInvolved->ItemName) {
                    // Same item, found possible stackable
                    if (ActualCharacter->MyInventoryInstance->InventoryContent[i]->CurrentStackSize + BaseItemInvolved->CurrentStackSize > BaseItemInvolved->MaxStackSize) {
                        // Stack exceeding MaxStackSize, pickup part of it and somehow continue for place selection for the rest
                        continue; //temp
                    }
                    else {
                        ActualCharacter->MyInventoryInstance->InventoryContent[i]->CurrentStackSize += BaseItemInvolved->CurrentStackSize;
                        BaseItemInvolved->CurrentStackSize = 0; // fail safe security
                        BaseItemInvolved->SetPickedUp(); // fail safe mark as in inventory to prevent other overlap pickups
                        ParentWorld->DestroyActor(BaseItemInvolved);
                        // Can stack item, merge items
                        break; //temp
                    }
                }
            }*/
        }
    }

    if (FoundPlaceToStoreItem) {

    }
    else {

    }
}

void AInventory::RequestInventoryMove(int32 StartingField, int32 DestinationField) {
    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(GetOwner());
    if (!ActualCharacter) {
        UE_LOG(LogTemp, Error, TEXT("Actor trying to move Inventory-Field item is NOT a FPSCharacter!"));
        return;
    }
    if (!ActualCharacter->MyInventoryInstance->isInitialized) {
        UE_LOG(LogTemp, Error, TEXT("Inventory of Character trying to move Inventory-Field is not initialized!"));
        return;
    }
    if (ActualCharacter->MyInventoryInstance->InventoryContent[StartingField] == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("Inventorycontent of StartingItem holds a nullpointer!"));
        // TODO: Spawn fail-safe Item_None on this position to repair the Inventory
        return;
    }
    if (InventoryContent[DestinationField] == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("Inventorycontent of StartingItem holds a nullpointer!"));
        // TODO: Spawn fail-safe Item_None on this position to repair the Inventory
        return;
    }

    // Client check to save server capacity
    if (!CanItemsBeSwapped(InventoryContent[StartingField], InventoryContent[DestinationField], StartingField, DestinationField)) {
        return;
    }

    // Check if they are usables and perform actions, if so: abort.
    AUsableItem* UsableItemStart = Cast<AUsableItem>(InventoryContent[StartingField]);
    if (UsableItemStart && UsableItemStart->IsInUse) {
        return;
    }

    AUsableItem* UsableItemDest = Cast<AUsableItem>(InventoryContent[DestinationField]);
    if (UsableItemDest && UsableItemDest->IsInUse) {
        return;
    }

    Server_ProcessInventoryMove(StartingField, DestinationField);
}

bool AInventory::Server_ProcessInventoryMove_Validate(int32 StartingField, int32 DestinationField) {
    return true;
}

void AInventory::Server_ProcessInventoryMove_Implementation(int32 StartingField, int32 DestinationField) {
    AFPSCharacter * ActualCharacter = Cast<AFPSCharacter>(GetOwner());
    if (!ActualCharacter) {
        UE_LOG(LogTemp, Error, TEXT("Actor trying to move Inventory-Field item is NOT a FPSCharacter!"));
        return;
    }
    if (!isInitialized) {
        UE_LOG(LogTemp, Error, TEXT("Inventory of Character trying to move Inventory-Field is not initialized!"));
        return;
    }
    if (InventoryContent[StartingField] == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("Inventorycontent of StartingItem holds a nullpointer!"));
        // TODO: Spawn fail-safe Item_None on this position to repair the Inventory
        return;
    }
    if (InventoryContent[DestinationField] == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("Inventorycontent of StartingItem holds a nullpointer!"));
        // TODO: Spawn fail-safe Item_None on this position to repair the Inventory
        return;
    }
    if (StartingField < 0 || StartingField > (CHARACTERPANEL_SLOTS + HOTBAR_SLOTS + INVENTORY_SLOTS)) {
        UE_LOG(LogTemp, Error, TEXT("StartingField out of bounds!"));
        return;
    }
    if (DestinationField < 0 || DestinationField > (CHARACTERPANEL_SLOTS + HOTBAR_SLOTS + INVENTORY_SLOTS)) {
        UE_LOG(LogTemp, Error, TEXT("DestinationField out of bounds!"));
        return;
    }

    AUsableItem* UsableItem = Cast<AUsableItem>(InventoryContent[StartingField]);

    // Do the check already performed on the client here too, just fail safe
    if (!CanItemsBeSwapped(InventoryContent[StartingField], InventoryContent[DestinationField], StartingField, DestinationField)) {
        return;
    }

    // Check if they are usables and perform actions, if so: abort.
    AUsableItem* UsableItemStart = Cast<AUsableItem>(InventoryContent[StartingField]);
    if (UsableItemStart && UsableItemStart->IsInUse) {
        return;
    }

    AUsableItem* UsableItemDest = Cast<AUsableItem>(InventoryContent[DestinationField]);
    if (UsableItemDest && UsableItemDest->IsInUse) {
        return;
    }

    int32 StartingStackSize = InventoryContent[StartingField]->CurrentStackSize;
    int32 DestinationStackSize = InventoryContent[DestinationField]->CurrentStackSize;

    // TODO #1: Currently it won't swap based on the item name (swap is the first if), this is bad because items can have different qualities, there needs to be one more detail
    // addition to the check .. (Check for quality (slow) or set bool like alwaysswapininventory (better))
    // TODO #2: Currently you can't swap from equipeditem to another item, low prio

    if (InventoryContent[StartingField]->ItemName != InventoryContent[DestinationField]->ItemName) {
        // Different items - Swap
        UE_LOG(LogTemp, Log, TEXT("Swap"));

        if (StartingField == NumberOfEquipedItem) {
            UE_LOG(LogTemp, Log, TEXT("This is currently deactivated due to a bug"));
            return;
        }

        InventoryContent.Swap(StartingField, DestinationField);

        // Update equiped item
        if (StartingField == NumberOfEquipedItem || DestinationField == NumberOfEquipedItem) {
            // Equiped item is somehow invovled
            if (InventoryContent[NumberOfEquipedItem]->ItemName == "Item_Empty") {
                EquipedItem = nullptr;
                NumberOfEquipedItem = -1;
            }
            else {
                if (GEngine) {
                    FString LogString = "Can't do shit ma niggas, cause ";
                    LogString.Append(FString::FromInt(NumberOfEquipedItem));
                    LogString.Append(" is standing in ma way, it's a ");
                    LogString.Append(InventoryContent[NumberOfEquipedItem]->ItemName);
                    GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, LogString);
                }

                AUsableItem *NewEquipedItem = Cast<AUsableItem>(InventoryContent[NumberOfEquipedItem]);
                if (!NewEquipedItem) {
                    UE_LOG(LogTemp, Error, TEXT("Critical, trying to swap equiped Items in Inventoryswap but item is no usable!"));
                    return;
                }
                Server_EquipItem(NumberOfEquipedItem);
            }
        }
    }
    else if (StartingStackSize == InventoryContent[StartingField]->MaxStackSize && DestinationStackSize == InventoryContent[DestinationField]->MaxStackSize) {
        // Same Items, but both at max stack size, do nothing
        UE_LOG(LogTemp, Log, TEXT("Same stack size and max stacksize, do nothing"));
    }
    else {
        // Same Items, try to merge
        if (StartingStackSize + DestinationStackSize <= InventoryContent[StartingField]->MaxStackSize) {
            // New StackSize not exceeding maximum, assign empty to starting (dragged) item and add to destination
            UE_LOG(LogTemp, Log, TEXT("Stack exceeding maximum"));
            ABaseItem* ItemToDestroy = InventoryContent[StartingField];
            ParentWorld->DestroyActor(ItemToDestroy);

            TSubclassOf<ABaseItem> EmptyItem = GetItem("Item_Empty");
            ABaseItem* SpawnedEmptyItem = ParentWorld->SpawnActor<ABaseItem>(EmptyItem);
            SpawnedEmptyItem->SetOwner(GetOwner());
            SpawnedEmptyItem->SetPickedUp();
            SpawnedEmptyItem->CurrentStackSize = 0;
            SpawnedEmptyItem->Multicast_DisableItem();

            InventoryContent[StartingField] = SpawnedEmptyItem;
            InventoryContent[DestinationField]->CurrentStackSize += StartingStackSize;

            // Check if StartingField was equipped item, set to zero then
            if (StartingField == NumberOfEquipedItem) {
                Server_EquipItem(NumberOfEquipedItem);
            }
        }
        else {
            // Exceeding maximum, take from starting and give to destination
            UE_LOG(LogTemp, Log, TEXT("The server taketh, the server giveth"));
            int32 AmountICanGiveYou = InventoryContent[DestinationField]->MaxStackSize - DestinationStackSize;
            InventoryContent[DestinationField]->CurrentStackSize += AmountICanGiveYou;
            InventoryContent[StartingField]->CurrentStackSize -= AmountICanGiveYou;
        }
    }

    // Update equipped item if not performing action
    if (StartingField == NumberOfEquipedItem || DestinationField == NumberOfEquipedItem) {
        EquipedItem = UsableItem;
    }
}

bool AInventory::CanItemsBeSwapped(ABaseItem *StartItem, ABaseItem *DestinationItem, int32 StartingField, int32 DestinationField) {
    bool CanStartItemDoSwap = false;
    bool CanDestinationItemDoSwap = false;

    for (int32 i = 0; i < InventoryFlags[DestinationField].Flags.Num(); i++) {
        if (CheckItemTypeForMask(StartItem->MyItemType, InventoryFlags[DestinationField].Flags[i])) {
            CanStartItemDoSwap = true;
        }
    }

    if (!CanStartItemDoSwap) {
        // It's already clear that the operation can't be performed!
        return false;
    }

    for (int32 i = 0; i < InventoryFlags[StartingField].Flags.Num(); i++) {
        if (CheckItemTypeForMask(DestinationItem->MyItemType, InventoryFlags[StartingField].Flags[i])) {
            CanDestinationItemDoSwap = true;
        }
    }

    return CanStartItemDoSwap && CanDestinationItemDoSwap;
}

bool AInventory::CheckItemTypeForMask(TEnumAsByte<EItemType::ItemType> ItemType, TEnumAsByte<EInventoryFieldValue::InventoryFieldValue> Mask) {
    switch (Mask) {
    case EInventoryFieldValue::FLAG_EVERYTHING:
        return true;
    case EInventoryFieldValue::FLAG_USABLE:
        return ItemType == EItemType::ITEM_USABLE || ItemType == EItemType::ITEM_NONE ? true : false;
    case EInventoryFieldValue::FLAG_HELMETARMOR:
        return ItemType == EItemType::ITEM_HEAD || ItemType == EItemType::ITEM_NONE ? true : false;
    case EInventoryFieldValue::FLAG_BREASTARMOR:
        return ItemType == EItemType::ITEM_BREAST || ItemType == EItemType::ITEM_NONE ? true : false;
    case EInventoryFieldValue::FLAG_PANTSARMOR:
        return ItemType == EItemType::ITEM_LEGS || ItemType == EItemType::ITEM_NONE ? true : false;
    case EInventoryFieldValue::FLAG_BOOTSARMOR:
        return ItemType == EItemType::ITEM_FOOT || ItemType == EItemType::ITEM_NONE ? true : false;
    case EInventoryFieldValue::FLAG_SUNGLASSES:
        return ItemType == EItemType::ITEM_GLASSES || ItemType == EItemType::ITEM_NONE ? true : false;
    case EInventoryFieldValue::FLAG_TRINKET:
        return ItemType == EItemType::ITEM_TRINKET || ItemType == EItemType::ITEM_NONE ? true : false;
    default:
        UE_LOG(LogTemp, Error, TEXT("Called DEFAULT in CheckItemTypeForMask() switch statement"));
        return false;
    }
}

void AInventory::EquipItem(int32 SlotNumber) {
    AUsableItem* CheckUsableItem = Cast<AUsableItem>(InventoryContent[SlotNumber]);

    if (SlotNumber < CHARACTERPANEL_SLOTS || SlotNumber >= CHARACTERPANEL_SLOTS + HOTBAR_SLOTS) {
        // No legit SlotNumber
        UE_LOG(LogTemp, Error, TEXT("EquipItem(): Slot Number is not legit ActionBar slot, staaahp it!"));
        return;
    }
    if (!isInitialized) {
        UE_LOG(LogTemp, Error, TEXT("EquipItem(): Inventory is not initialized"));
        return;
    }
    if (CheckUsableItem == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("EquipItem(): Item in inventory is nullpointer"));
        return;
    }
    if (!CheckUsableItem) {
        // No log, cause item is just not equipable!
        return;
    }
    if (EquipedItem && EquipedItem->IsOnCooldown) {
        UE_LOG(LogTemp, Log, TEXT("OnCooldown Client"));
        return;
    }
    if (EquipedItem && EquipedItem == InventoryContent[SlotNumber]) {
        UE_LOG(LogTemp, Log, TEXT("Already equipped"));
        return;
    }

    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(GetOwner());
    if (!ActualCharacter) {
        UE_LOG(LogTemp, Error, TEXT("EquipItem(): No valid Character"));
        return;
    }
    if (!ActualCharacter->IsAlive) {
        return;
    }

    // Set mesh of 1st Person view, cause we dont need to distrubite it with the server
    ABaseItem* AffectedItem = InventoryContent[SlotNumber];

    Server_EquipItem(SlotNumber);
}

bool AInventory::Server_EquipItem_Validate(int32 SlotNumber) {
    return true;
}

void AInventory::Server_EquipItem_Implementation(int32 SlotNumber) {
    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(GetOwner());

    if (!ActualCharacter) {
        UE_LOG(LogTemp, Error, TEXT("Server_EquipItem(): Character is not Character or lost reference"));
        return;
    }
    if (!ActualCharacter->IsAlive) {
        return;
    }
    if (ActualCharacter->CharacterEmoteAnimation != EEmoteAnimation::EMOTE_NONE) {
        return;
    }

    AUsableItem* UsableItem = Cast<AUsableItem>(InventoryContent[SlotNumber]);

    if (SlotNumber < CHARACTERPANEL_SLOTS || SlotNumber >= CHARACTERPANEL_SLOTS + HOTBAR_SLOTS) {
        // No legit SlotNumber
        UE_LOG(LogTemp, Error, TEXT("Server_EquipItem(): Slot Number is not legit ActionBar Slot!"));
        return;
    }
    if (!isInitialized) {
        UE_LOG(LogTemp, Error, TEXT("Server_EquipItem(): Inventory on Character is not initialized"));
        return;
    }
    if (UsableItem == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("Server_EquipItem(): Item in inventory is nullpointer"));
        return;
    }
    if (!UsableItem) {
        UE_LOG(LogTemp, Warning, TEXT("Server_EquipItem(): Requested item is not equipable! This shouldn't have cause there is already a check clientside"));
        return;
    }
    if (EquipedItem && EquipedItem->IsOnCooldown) {
        UE_LOG(LogTemp, Log, TEXT("ON CD"));
        return;
    }

    AUsableItem *CachedPreviousItem = EquipedItem;

    EquipedItem = UsableItem;
    NumberOfEquipedItem = SlotNumber;

    if (GEngine) {
        FString LogString = "Equiperino -> ";
        LogString.Append(EquipedItem->ItemName);
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), LogString);
    }

    // Call Repfunction for Server, cause he will not execute OnRep himself
    OnRep_EquipedItemChanged(CachedPreviousItem);
}

void AInventory::OnRep_EquipedItemChanged(AUsableItem* LastItem) {
    // Call function to hide / detach previous item
    DeactivatePreviousItem(LastItem);

    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(GetOwner());

    if (!ActualCharacter || !ActualCharacter->IsAlive) {
        UE_LOG(LogTemp, Log, TEXT("OnRep_EquipedItemChanged(): Character null or dead"));
        return;
    }
    if (ActualCharacter->CharacterEmoteAnimation != EEmoteAnimation::EMOTE_NONE) {
        return;
    }

    if (EquipedItem == nullptr) {
        UE_LOG(LogTemp, Log, TEXT("OnRep_EquipedItemChanged(): Item null"));
        return;
    }

    EquipedItem->SetActorHiddenInGame(false);

    if (ActualCharacter->IsLocallyControlled()) {
        if (EquipedItem->Using1PSkeletalMesh) {
            UE_LOG(LogTemp, Log, TEXT("Has 1P Skel"));

            EquipedItem->Mesh1P->SetHiddenInGame(false);
            EquipedItem->Mesh1P->SetActive(true);
            EquipedItem->Mesh1P->AttachTo(ActualCharacter->FirstPersonMesh, EquipedItem->AttachmentSlotName);
        }
        else {
            // Use Staticmesh
            UE_LOG(LogTemp, Log, TEXT("Has no 1P Skel"));

            EquipedItem->MeshDropped->SetHiddenInGame(false);
            EquipedItem->MeshDropped->SetActive(true);
            EquipedItem->MeshDropped->AttachTo(ActualCharacter->FirstPersonMesh, EquipedItem->AttachmentSlotName);
        }
    }
    else {
        if (EquipedItem->Using3PSkeletalMesh) {
            UE_LOG(LogTemp, Log, TEXT("Has 3P Skel"));

            EquipedItem->Mesh3P->SetHiddenInGame(false);
            EquipedItem->Mesh3P->SetActive(true);
            EquipedItem->Mesh3P->AttachTo(ActualCharacter->Mesh, EquipedItem->AttachmentSlotName);
        }
        else {
            // Use Staticmesh
            UE_LOG(LogTemp, Log, TEXT("Has no 3P Skel"));

            EquipedItem->MeshDropped->SetHiddenInGame(false);
            EquipedItem->MeshDropped->SetActive(true);
            EquipedItem->MeshDropped->AttachTo(ActualCharacter->Mesh, EquipedItem->AttachmentSlotName);
        }
    }
}

void AInventory::DeactivatePreviousItem(AUsableItem* PreviousItem) {
    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(GetOwner());

    if (!ActualCharacter) {
        UE_LOG(LogTemp, Log, TEXT("OnRep_PreviousItemChanged(): Character null"));
        return;
    }

    if (PreviousItem != nullptr && !PreviousItem->IsPendingKill()) {
        if (PreviousItem == EquipedItem) {
            UE_LOG(LogTemp, Log, TEXT("Not deactivating previous item cause actualitem = previousitem"));
            return;
        }

        PreviousItem->SetActorHiddenInGame(true);

        TArray<USceneComponent*> CurrentAttachments;

        // Detach all components from Actor
        if (ActualCharacter->IsLocallyControlled()) {
            ActualCharacter->FirstPersonMesh->GetChildrenComponents(false, CurrentAttachments);
            for (USceneComponent* ChildComponent : CurrentAttachments) {
                if (ChildComponent == nullptr) {
                    UE_LOG(LogTemp, Log, TEXT("Childcomponent in deactive previous item was NULL!"));
                    continue;
                }
                ChildComponent->SetHiddenInGame(true);
                ChildComponent->SetActive(false);
                ChildComponent->DetachFromParent();
//                UE_LOG(LogTemp, Log, TEXT("Detached LOCAL item in OnRep_PreviousItemChanged()"));
            }
        }
        else {
            ActualCharacter->Mesh->GetChildrenComponents(false, CurrentAttachments);
            for (USceneComponent* ChildComponent : CurrentAttachments) {
                ChildComponent->SetHiddenInGame(true);
                ChildComponent->SetActive(false);
                ChildComponent->DetachFromParent();
//                UE_LOG(LogTemp, Log, TEXT("Detached NONLOCAL item in OnRep_PreviousItemChanged()"));
            }
        }

        CurrentAttachments.Reset();
    }
}

void AInventory::DestroyEquipedItem() {

}

AUsableItem *AInventory::GetEquipedItem() const {
    if (NumberOfEquipedItem == -1) {
        //UE_LOG(LogTemp, Log, TEXT("Cause number was zero"));
        return nullptr;
    }
    if (EquipedItem == nullptr || EquipedItem->ItemName.Equals("Item_Empty")) {
        //UE_LOG(LogTemp, Log, TEXT("Cause was nullptr or Item_Empty"));
        // can be nullpointer aswell, but returns the actual item if we droppped or moved somethign a picked something else up into that place
        return Cast<AUsableItem>(InventoryContent[NumberOfEquipedItem]);
    }
    else {
        return EquipedItem;
    }
}

int32 AInventory::GetNumberOfEquipedItem() const {
    return NumberOfEquipedItem;
}

//int32 AInventory::GetNumberOfResourceAvailable(TSubclassOf<ABaseItem> Item, int32 Amount) {
//    UE_LOG(LogTemp, Log, TEXT("2 Args"));

//    return 0;
//}

int32 AInventory::GetNumberOfResourceAvailable(TSubclassOf<ABaseItem> Item, TArray<TSubclassOf<ABaseItem>> &AlternativeResources, int32 NumTimes) {
    bool HasAlternativeIngredients = AlternativeResources.Num() == 0 ? false : true;

//    if (AlternativeResources.Num() == 0) {
//        HasAlternativeIngredients = false;
//    }

    int32 AmountOfItem = 0;

    for (int i = 0; i < INVENTORY_SLOTS; i++) {
        if (InventoryContent[i]->ItemName == Item.GetDefaultObject()->ItemName) {
            AmountOfItem += InventoryContent[i]->CurrentStackSize;
        }
    }

    // Debug //
    if (GEngine) {
        FString LogString = "Lookup for resource ";
        LogString.Append(Item.GetDefaultObject()->ItemName);
        LogString.Append(", Number found: ");
        LogString.Append(FString::FromInt(AmountOfItem));
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, LogString);
    }

    return AmountOfItem;
}

/**
 * @return Amount taken from inventory
 */
int32 AInventory::AcquireResource(TSubclassOf<ABaseItem> Item, TArray<TSubclassOf<ABaseItem>> &AlternativeResources, int32 AmountNecessary) {
    if (Role != ROLE_Authority) {
        UE_LOG(LogTemp, Error, TEXT("Tried to acquire resource, but am not the server!"));
        return -1;
    }

    // First: Find main resource, take from the smallest stack
    int32 TempAmountNeeded = AmountNecessary;

    while (true) {
        ABaseItem* SmallestStackItem = nullptr;
        int32 SmallestStackSize = -1;
        int32 CurrentIndex = -1;
        for (int i = 0; i < INVENTORY_SLOTS; i++) {
            ABaseItem* CurrentItem = InventoryContent[i];
            if (Item.GetDefaultObject()->ItemName == CurrentItem->ItemName) {
                if (SmallestStackSize == -1) {
                    SmallestStackItem = CurrentItem;
                    SmallestStackSize = CurrentItem->CurrentStackSize;
                    CurrentIndex = i;
                }
                else if (CurrentItem->CurrentStackSize < SmallestStackSize) {
                    SmallestStackItem = CurrentItem;
                    SmallestStackSize = CurrentItem->CurrentStackSize;
                    CurrentIndex = i;
                }
            }
        }

        // No more items found
        if (SmallestStackItem == nullptr) {
            break;
        }

        if (SmallestStackItem->CurrentStackSize > TempAmountNeeded) {
            // Stacksize of Item is higher than we need
            SmallestStackItem->CurrentStackSize -= TempAmountNeeded;
            TempAmountNeeded = 0;

            UE_LOG(LogTemp, Log, TEXT("AcquireResource(): Stack > Needed, shrinkened Stack"));
        }
        else {
            // Stack got used up, destroy actor and spawn empty item
            TempAmountNeeded -= SmallestStackItem->CurrentStackSize;

            SmallestStackItem->Destroy();

            TSubclassOf<ABaseItem> EmptyItem = GetItem("Item_Empty");
            ABaseItem* SpawnedEmptyItem = ParentWorld->SpawnActor<ABaseItem>(EmptyItem);
            SpawnedEmptyItem->SetOwner(GetOwner());
            SpawnedEmptyItem->SetPickedUp();
            SpawnedEmptyItem->Multicast_DisableItem();

            InventoryContent[CurrentIndex] = SpawnedEmptyItem;

            UE_LOG(LogTemp, Log, TEXT("AcquireResource(): Destroyed actor"));
        }

        if (TempAmountNeeded <= 0) {
            // <= 0: fail safe
            if (TempAmountNeeded < 0) {
                UE_LOG(LogTemp, Error, TEXT("Taken to many items from inventory, this is CRITICAL!"));
            }

            break;
        }
    }

    return AmountNecessary - TempAmountNeeded;

    // Second: Find alternative resources in loop, take from the smallest stacks
}

void AInventory::MakeEquipedItemInvisible(float DurationInSeconds) {
    if (Role != ROLE_Authority) {
        return;
    }

    if (!isInitialized || EquipedItem == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("SetEquipedItemVisibility(): Inventory not initialized or Item == null"));
        return;
    }

    EquipedItem->MakeTemporarelyInvisible(DurationInSeconds);
}

void AInventory::CreateCraftedItem(ABaseRecipe *Recipe) {
    if (Role != ROLE_Authority) {
        UE_LOG(LogTemp, Error, TEXT("CreateCraftedItem(): Can't craft item, am not authority!"));
        return;
    }

    if (!isInitialized) {
        UE_LOG(LogTemp, Error, TEXT("CreateCraftedItem(): Inventory not initialized!"));
        return;
    }

    if (!ParentWorld) {
        UE_LOG(LogTemp, Error, TEXT("CreateCraftedItem(): No ParentWorld"));
        return;
    }

    AFPSGameMode* GameMode = Cast<AFPSGameMode>(ParentWorld->GetAuthGameMode());
    if (!GameMode) {
        UE_LOG(LogTemp, Error, TEXT("CreateCraftedItem(): Couldn't access GameMode"));
        return;
    }

    AFPSCharacter* ActualCharacter = Cast<AFPSCharacter>(GetOwner());
    if (!ActualCharacter) {
        UE_LOG(LogTemp, Error, TEXT("CreateCraftedItem(): No character"));
        return;
    }
    if (!ActualCharacter->IsAlive) {
        return;
    }

    // + Perform additional resource check (this is creation time)
    FString ItemName = Recipe->CreatesItem.GetDefaultObject()->ItemName;
    int32 StackRemaining = Recipe->CreatesItem.GetDefaultObject()->CurrentStackSize;

    // Don't place items directly into equiped items, change by setting i = 0
    for (int i = CHARACTERPANEL_SLOTS; i < CHARACTERPANEL_SLOTS + HOTBAR_SLOTS + INVENTORY_SLOTS; i++) {
        if (InventoryContent[i] != nullptr && ItemName == InventoryContent[i]->ItemName) {
            if (InventoryContent[i]->CurrentStackSize == InventoryContent[i]->MaxStackSize) {
                // Full stack, skip
                continue;
            }
            else if (InventoryContent[i]->CurrentStackSize + StackRemaining <= InventoryContent[i]->MaxStackSize) {
                // Everything fits into this stack, fill up completely
                InventoryContent[i]->CurrentStackSize += StackRemaining;
                StackRemaining = 0; // is this error prone?
            }
            else {
                // It partially fits, fill up to max
                int32 PossibleFillAmount = InventoryContent[i]->MaxStackSize - InventoryContent[i]->CurrentStackSize;
                InventoryContent[i]->CurrentStackSize += PossibleFillAmount;
                StackRemaining -= PossibleFillAmount;
            }

            if (StackRemaining <= 0) {
                // Everything is placed in inventory
                if (StackRemaining < 0) {
                    UE_LOG(LogTemp, Error, TEXT("Critical! CreateCraftedItem(): StackRemaining is < 0"));
                }

                break;
            }
        }
    }

    int32 FreeInventorySpace = IsInventorySpaceAvailable();

    HaturHelper::QL("Free space: ", FreeInventorySpace);

    if (StackRemaining > 0 && FreeInventorySpace != -1) {
        UE_LOG(LogTemp, Log, TEXT("Putting into inventory"));
        // Fits into inventory, spawn it here!
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = ActualCharacter;
        SpawnParams.Instigator = ActualCharacter;
        ABaseItem *SpawnedItem = ParentWorld->SpawnActor<ABaseItem>(Recipe->CreatesItem, SpawnParams);
        SpawnedItem->SetOwner(ActualCharacter);
        SpawnedItem->Multicast_DisableItem();
        InventoryContent[FreeInventorySpace] = SpawnedItem;
    }
    else if (StackRemaining > 0) {
        UE_LOG(LogTemp, Log, TEXT("Putting into world"));
        // Couldn't fit item into inventory, spawn and move to world!
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = GameMode;
        SpawnParams.Instigator = ActualCharacter;
        ABaseItem *SpawnedItem = ParentWorld->SpawnActor<ABaseItem>(Recipe->CreatesItem, ActualCharacter->GetActorLocation(), ActualCharacter->GetActorRotation(), SpawnParams);
    }
    else {
        UE_LOG(LogTemp, Log, TEXT("Stack should be filled up"));
    }
}

int32 AInventory::IsInventorySpaceAvailable(bool ConsiderCharacterPanel, bool ConsiderHotbar) {
    int32 SpaceAvailable = -1;

    if (ConsiderCharacterPanel && ConsiderHotbar) {
        for (int32 i = 0; i < CHARACTERPANEL_SLOTS + HOTBAR_SLOTS + INVENTORY_SLOTS; i++) {
            if (InventoryContent[i] == nullptr || InventoryContent[i]->IsPendingKill() || InventoryContent[i]->ItemName.Equals("Item_Empty")) {
                // This is potentially empty
                SpaceAvailable = i;
                break;
            }
        }
    }
    else if (ConsiderCharacterPanel) {
        for (int32 i = 0; i < CHARACTERPANEL_SLOTS + HOTBAR_SLOTS + INVENTORY_SLOTS; i++) {
            if (i > CHARACTERPANEL_SLOTS && i <= CHARACTERPANEL_SLOTS + HOTBAR_SLOTS) {
                continue;
            }

            if (InventoryContent[i] == nullptr || InventoryContent[i]->IsPendingKill() || InventoryContent[i]->ItemName.Equals("Item_Empty")) {
                // This is potentially empty
                SpaceAvailable = i;
                break;
            }
        }
    }
    else if (ConsiderHotbar) {
        for (int32 i = CHARACTERPANEL_SLOTS; i < CHARACTERPANEL_SLOTS + HOTBAR_SLOTS + INVENTORY_SLOTS; i++) {
            if (InventoryContent[i] == nullptr || InventoryContent[i]->IsPendingKill() || InventoryContent[i]->ItemName.Equals("Item_Empty")) {
                // This is potentially empty
                SpaceAvailable = i;
                break;
            }
        }
    }
    else {
        // Consider neither Characterpanel nor Hotbar
        for (int32 i = CHARACTERPANEL_SLOTS + HOTBAR_SLOTS; i < CHARACTERPANEL_SLOTS + HOTBAR_SLOTS + INVENTORY_SLOTS; i++) {
            if (InventoryContent[i] == nullptr || InventoryContent[i]->IsPendingKill() || InventoryContent[i]->ItemName.Equals("Item_Empty")) {
                // This is potentially empty
                SpaceAvailable = i;
                break;
            }
        }
    }

    return SpaceAvailable;
}

void AInventory::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);

    // Clean up dead actors in inventory
    if (Role == ROLE_Authority && isInitialized) {
        for (int32 i = 0; i < INVENTORY_SLOTS; i++) {
            if (InventoryContent[i] == nullptr || InventoryContent[i]->IsPendingKill()) {
                TSubclassOf<ABaseItem> EmptyItem = GetItem("Item_Empty");
                ABaseItem* SpawnedEmptyItem = ParentWorld->SpawnActor<ABaseItem>(EmptyItem);
                SpawnedEmptyItem->SetOwner(GetOwner());
                SpawnedEmptyItem->SetPickedUp();
                SpawnedEmptyItem->CurrentStackSize = 0;
                SpawnedEmptyItem->Multicast_DisableItem();
                InventoryContent[i] = SpawnedEmptyItem;
                UE_LOG(LogTemp, Log, TEXT("Removed null item"));

                EquipedItem = nullptr;
                NumberOfEquipedItem = -1;
            }
        }
    }
}

void AInventory::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AInventory, InventoryContent);
    DOREPLIFETIME(AInventory, isInitialized);
    DOREPLIFETIME(AInventory, EquipedItem);
    DOREPLIFETIME(AInventory, NumberOfEquipedItem);
}

void AInventory::OnRep_InventoryContent() {
    UE_LOG(LogTemp, Warning, TEXT("Replicated Inventory Array"));
}
