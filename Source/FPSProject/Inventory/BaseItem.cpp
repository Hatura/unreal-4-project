#include "FPSProject.h"
#include "BaseItem.h"
#include "UnrealNetwork.h"

ABaseItem::ABaseItem(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    MeshDropped = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("ItemMeshDropped"));
    MeshDropped->bReceivesDecals = false;
    MeshDropped->SetHiddenInGame(IsInInventory);
    MeshDropped->SetActive(!IsInInventory);
    MeshDropped->CastShadow = !IsInInventory;
    MeshDropped->SetCollisionProfileName((TEXT("Item")));
    if (IsInInventory) {
        MeshDropped->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    }
    else {
        MeshDropped->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    }
    MeshDropped->SetSimulatePhysics(!IsInInventory);

    RootComponent = MeshDropped;

    Mesh3P = PCIP.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("ItemMesh3P"));
    Mesh3P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
    Mesh3P->bChartDistanceFactor = true;
    Mesh3P->bReceivesDecals = false;

    Mesh3P->CastShadow = !IsInInventory;
    Mesh3P->SetCollisionObjectType(ECC_WorldDynamic); // Disable all physics on skeletal meshes for now
    Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    Mesh3P->SetCollisionResponseToAllChannels(ECR_Ignore);
    Mesh3P->SetSimulatePhysics(false);
    Mesh3P->SetHiddenInGame(IsInInventory);
    Mesh3P->SetActive(!IsInInventory);

    Mesh3P->bOwnerNoSee = true;
    Mesh3P->AttachParent = MeshDropped;

    SetActorEnableCollision(!IsInInventory);
    SetActorHiddenInGame(IsInInventory);

    bReplicates = true;
    bReplicateMovement = true;

    // Init so it has a default value to replicate on the first try
    MeshDroppedVisibility = !IsInInventory;
    Mesh3PVisibility = !IsInInventory;

    // Default initializations
    Using3PSkeletalMesh         = false;
    ItemRarity                  = EItemRarity::RARITY_COMMON;
    CurrentStackSize            = 1;
    MaxStackSize                = 1;
    PickupReadyAfterSeconds     = 2.0f;
}

void ABaseItem::PostInitializeComponents() {
    Super::PostInitializeComponents();

    GetWorldTimerManager().SetTimer(this, &ABaseItem::SetPickupReady, PickupReadyAfterSeconds, false);
}

void ABaseItem::ReceiveHit
        (
            class UPrimitiveComponent * MyComp,
            class AActor * Other,
            class UPrimitiveComponent * OtherComp,
            bool bSelfMoved,
            FVector HitLocation,
            FVector HitNormal,
            FVector NormalImpulse,
            const FHitResult & Hit
        ) {
    UE_LOG(LogTemp, Log, TEXT("Actor ABaseItem received Hit"));
}

void ABaseItem::SetPickupReady() {
    IsPickupReady = true;
}

void ABaseItem::SetPickedUp() {
    IsInInventory = true;
}

bool ABaseItem::Multicast_DisableItem_Validate() {
    return true;
}

void ABaseItem::Multicast_DisableItem_Implementation() {
    Mesh3P->SetHiddenInGame(true);
    Mesh3P->SetActive(false);

    MeshDropped->SetHiddenInGame(true);
    MeshDropped->SetActive(false);
    MeshDropped->SetSimulatePhysics(false);
    MeshDropped->SetCollisionEnabled(ECollisionEnabled::NoCollision);

    SetActorEnableCollision(false);
    SetActorHiddenInGame(true);
    SetActorLocationAndRotation(FVector::ZeroVector, FRotator::ZeroRotator);
}

//bool ABaseItem::Multicast_ItemToWorld_Validate(FVector Position, FRotator Rotation) {
//    return true;
//}

//void ABaseItem::Multicast_ItemToWorld_Implementation(FVector Position, FRotator Rotation) {
//    DroppedMesh->SetVisibility(true);
//    DroppedMesh->SetHiddenInGame(false);
//    DroppedMesh->SetSimulatePhysics(true);
//    DroppedMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
//    DroppedMesh->SetActive(true);

//    this->SetActorEnableCollision(true);
//    this->SetActorHiddenInGame(false);
//    this->SetActorLocationAndRotation(Position, Rotation);
//}

void ABaseItem::OnRep_MeshDroppedVisiblityChanged() {
    MeshDropped->SetHiddenInGame(!MeshDroppedVisibility);
}

void ABaseItem::OnRep_Mesh3PVisibilityChanged() {
    Mesh3P->SetHiddenInGame(!Mesh3PVisibility);
}

void ABaseItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ABaseItem, CurrentStackSize);
    DOREPLIFETIME(ABaseItem, IsInInventory);
    DOREPLIFETIME(ABaseItem, MeshDroppedVisibility);
    DOREPLIFETIME(ABaseItem, Mesh3PVisibility);
}
