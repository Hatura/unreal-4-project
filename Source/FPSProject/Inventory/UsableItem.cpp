#include "FPSProject.h"
#include "TimerManager.h"
#include "UsableItem.h"


AUsableItem::AUsableItem(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    Mesh1P = PCIP.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("ItemMesh1P"));
    Mesh1P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
    Mesh1P->bChartDistanceFactor = false;
    Mesh1P->bReceivesDecals = false;
    Mesh1P->CastShadow = false;
    Mesh1P->SetCollisionObjectType(ECC_WorldDynamic);
    Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
    Mesh1P->SetSimulatePhysics(false);
    Mesh1P->AttachParent = MeshDropped;
    Mesh1P->SetHiddenInGame(IsInInventory);
    Mesh1P->SetActive(!IsInInventory);

    Mesh1P->bOnlyOwnerSee = true;

    // Init so it has a default value to replicate on the first try
    Mesh1PVisibility = !IsInInventory;

    // Default inits
    Using1PSkeletalMesh         = false;
    AttachmentSlotName          = TEXT("RHS");
    AnimationToIdleTime         = 0;
    TimeBeforeActionProcessed   = 0;
    ItemCooldown                = 0;
    IsOnCooldown                = false;
    IsInUse                     = false;
    MetaAnimationType           = EMetaAnimation::META_NONE;
    ItemState                   = EItemState::IS_IDLE;
}

void AUsableItem::UseItem() {
    // Default implementation

    if (IsInUse) {
        return;
    }

    if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), TEXT("AUsableItem::UseItem()")); }

    IsInUse = true;

    GetWorldTimerManager().SetTimer(this, &AUsableItem::WaitForIdleAnimationReach, TimeBeforeActionProcessed - AnimationToIdleTime, false);

    Server_UseItem();
}

bool AUsableItem::Server_UseItem_Validate() {
    return true;
}

void AUsableItem::Server_UseItem_Implementation() {
    if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), TEXT("AUsableItem::Server_UseItem()")); }

    IsOnCooldown = true;

    GetWorldTimerManager().SetTimer(this, &AUsableItem::ConsumeItem, TimeBeforeActionProcessed, false);
    GetWorldTimerManager().SetTimer(this, &AUsableItem::FreeCooldown, ItemCooldown, false);
}

void AUsableItem::WaitForIdleAnimationReach() {
    if (Role != ROLE_Authority) {
        return;
    }

    if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, TEXT("Anim to Idle time")); }
}

void AUsableItem::ConsumeItem() {
    // Default implementation
    IsInUse = false;

    Server_ConsumeItem();
}

bool AUsableItem::Server_ConsumeItem_Validate() {
    return true;
}

void AUsableItem::Server_ConsumeItem_Implementation() {
    // Default implementation
    CurrentStackSize -= 1;
    if (CurrentStackSize <= 0) {
        if (CurrentStackSize < 0) {
            UE_LOG(LogTemp, Error, TEXT("Critical, CurrentStackSize of Item is < 0 in Server_ConsumeItem()"));
        }

        OnConsume();

        // remove item
        UE_LOG(LogTemp, Log, TEXT("Server_ConsumeItem(): Trying to remove item.."));
        Destroy();
    }
}

void AUsableItem::OnConsume() {

}

void AUsableItem::FreeCooldown() {
    // Default implementation
    if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), TEXT("Free'd CD")); }

    IsOnCooldown = false;
}

void AUsableItem::MakeTemporarelyInvisible(float DurationInSeconds) {
    if (Role != ROLE_Authority) {
        return;
    }

    if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, TEXT("Trying to make Usable invisible")); }

    // SetHiddenInGame() is called for the Server, the Clients do it via OnRep_...()

    if (!MeshDropped->bHiddenInGame) {
        if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, TEXT("Hiding MeshDropped")); }
        MeshDropped->SetHiddenInGame(true);
        MeshDroppedVisibility = false;

        GetWorldTimerManager().SetTimer(this, &AUsableItem::MakeMeshDroppedVisibleAgain, DurationInSeconds, false);
    }

    if (!Mesh3P->bHiddenInGame) {
        if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, TEXT("Hiding Mesh3P")); }
        Mesh3P->SetHiddenInGame(true);
        Mesh3PVisibility = false;

        GetWorldTimerManager().SetTimer(this, &AUsableItem::MakeMesh3PVisibleAgain, DurationInSeconds, false);
    }

    if (!Mesh1P->bHiddenInGame) {
        if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, TEXT("Hiding Mesh1P")); }
        Mesh1P->SetHiddenInGame(true);
        Mesh1PVisibility = false;

        GetWorldTimerManager().SetTimer(this, &AUsableItem::MakeMesh1PVisibleAgain, DurationInSeconds, false);
    }
}

void AUsableItem::MakeMeshDroppedVisibleAgain() {
    if (Role != ROLE_Authority) {
        return;
    }

    MeshDropped->SetHiddenInGame(false);
    MeshDroppedVisibility = true;
}

void AUsableItem::MakeMesh3PVisibleAgain() {
    if (Role != ROLE_Authority) {
        return;
    }

    Mesh3P->SetHiddenInGame(false);
    Mesh3PVisibility = true;
}

void AUsableItem::MakeMesh1PVisibleAgain() {
    if (Role != ROLE_Authority) {
        return;
    }

    Mesh1P->SetHiddenInGame(false);
    Mesh1PVisibility = true;
}

bool AUsableItem::Multicast_DisableItem_Validate() {
    if (Super::Multicast_DisableItem_Validate()) {
        return true;
    }
    return false;
}

void AUsableItem::Multicast_DisableItem_Implementation() {
    Mesh1P->SetHiddenInGame(true);
    Mesh1P->SetActive(false);

    Super::Multicast_DisableItem_Implementation();
}

void AUsableItem::OnRep_Mesh1PVisibilityChanged() {
    Mesh1P->SetHiddenInGame(!Mesh1PVisibility);
}

void AUsableItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AUsableItem, Mesh1PVisibility);
    DOREPLIFETIME_CONDITION(AUsableItem, IsOnCooldown, COND_OwnerOnly);
}
