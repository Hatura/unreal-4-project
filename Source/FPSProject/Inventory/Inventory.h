#pragma once

#include "GameFramework/Actor.h"
#include "UsableItem.h"
#include "Inventory.generated.h"

/**
 * 
 */
USTRUCT()
struct FItemData {
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, Category = Item)
    FString ItemName;

    UPROPERTY(EditDefaultsOnly, Category = Item)
    TSubclassOf<class ABaseItem> Item;
};

UENUM(BlueprintType)
namespace EInventoryFieldValue {
    enum InventoryFieldValue {
        FLAG_EVERYTHING,
        FLAG_USABLE,
        FLAG_HELMETARMOR,
        FLAG_SUNGLASSES,
        FLAG_BREASTARMOR,
        FLAG_PANTSARMOR,
        FLAG_BOOTSARMOR,
        FLAG_TRINKET
    };
}

USTRUCT()
struct FInventoryFieldFlag {
    GENERATED_USTRUCT_BODY()

    UPROPERTY()
    TArray<TEnumAsByte<EInventoryFieldValue::InventoryFieldValue>> Flags;
};

UCLASS()
class FPSPROJECT_API AInventory : public AActor
{
    GENERATED_UCLASS_BODY()

    UPROPERTY(EditDefaultsOnly, Category = Inventory)
    TArray<FItemData> ItemClasses;

    TSubclassOf<class ABaseItem> GetItem(FString ItemName);

    UFUNCTION()
    void OnRep_InventoryContent();


public:
    static const int CHARACTERPANEL_SLOTS = 10;

    // only to check for item pickups, place them in inventory and not in hotbars, declared as const to make it dynamically changeable later
    static const int HOTBAR_SLOTS = 5;

    // TODO: Make dynamic for inventory bags etc.
    static const int INVENTORY_SLOTS = 24;

    void Tick(float DeltaTime) override;

    // Is initialized for Slate?
    UPROPERTY(Replicated, Transient)
    bool isInitialized;

    UPROPERTY()
    bool IsInventoryOpened;

    UPROPERTY()
    UWorld* ParentWorld;

    UPROPERTY(ReplicatedUsing = OnRep_InventoryContent)
    TArray<class ABaseItem*> InventoryContent;

    // Static cause it is always the same
    static TArray<FInventoryFieldFlag> InventoryFlags;

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_InitializeInventory(UWorld *InitWorld);

    UFUNCTION(BlueprintCallable, Category = Inventory)
    void OnItemPickup(ABaseItem *BaseItemInvolved);

    // Server distributing items
    UFUNCTION(Server, Reliable, WithValidation)
    void Server_OnItemPickup(ABaseItem* BaseItemInvolved);

    UFUNCTION()
    void RequestInventoryMove(int32 StartingField, int32 DestinationField);

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_ProcessInventoryMove(int32 StartingField, int32 DestinationField);

    UFUNCTION(BlueprintCallable, Category = Inventory)
    void EquipItem(int32 SlotNumber);

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_EquipItem(int32 SlotNumber);

    UFUNCTION(BlueprintCallable, Category = Inventory)
    AUsableItem* GetEquipedItem() const;

    UFUNCTION(BlueprintCallable, Category = Inventory)
    int32 GetNumberOfEquipedItem() const;

    UFUNCTION(BlueprintCallable, Category = Inventory)
    int32 GetNumberOfResourceAvailable(TSubclassOf<ABaseItem> Item, TArray<TSubclassOf<ABaseItem>> &AlternativeResources, int32 NumTimes = 1);

    UFUNCTION()
    int32 AcquireResource(TSubclassOf<ABaseItem> Item, TArray<TSubclassOf<ABaseItem>> &AlternativeResources, int32 AmountNecessary);

    UFUNCTION(BlueprintCallable, Category = Inventory)
    void MakeEquipedItemInvisible(float DurationInSeconds);

    // Let inventory spawn crafted items, Server function
    UFUNCTION(BlueprintCallable, Category = Inventory)
    void CreateCraftedItem(ABaseRecipe* Recipe);

    UFUNCTION(BlueprintCallable, Category = Inventory)
    int32 IsInventorySpaceAvailable(bool ConsiderCharacterPanel = false, bool ConsiderHotbar = true);

//    UFUNCTION()
//    void SetEquipedItem(ABaseItem* ItemToEquip);

private:
    bool CanItemsBeSwapped(ABaseItem* StartItem, ABaseItem* DestinationItem, int32 StartingField, int32 DestinationField);

    // Checks the item type against the mask (InventoryFieldValue)
    bool CheckItemTypeForMask(TEnumAsByte<EItemType::ItemType> ItemType, TEnumAsByte<EInventoryFieldValue::InventoryFieldValue> Mask);

    // Current item equiped
    UPROPERTY(ReplicatedUsing = OnRep_EquipedItemChanged)
    AUsableItem* EquipedItem;

    UPROPERTY(Replicated)
    int32 NumberOfEquipedItem;

    UFUNCTION()
    void InitInventoryFlags();

    UFUNCTION()
    void OnRep_EquipedItemChanged(AUsableItem* LastItem);

    UFUNCTION()
    void DestroyEquipedItem();

    // Internally called by OnRep_EquipItemChanged to hide / detach the previous item
    UFUNCTION()
    void DeactivatePreviousItem(AUsableItem *PreviousItem);
};
