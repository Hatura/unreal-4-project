#include "FPSProject.h"
#include "Inventory/BaseItem.h" // defines for EItemRarity
#include "FPSWorldSettings.h"


AFPSWorldSettings::AFPSWorldSettings(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}

FLinearColor &AFPSWorldSettings::GetColorForRarity(TEnumAsByte<EItemRarity::ItemRarity> Rarity) {
    switch (Rarity) {
    case EItemRarity::RARITY_CRAP:
        return RarityColor_Crap;
    case EItemRarity::RARITY_UNDEFINED:
    case EItemRarity::RARITY_COMMON:
        return RarityColor_Common;
    case EItemRarity::RARITY_UNCOMMON:
        return RarityColor_Uncommon;
    case EItemRarity::RARITY_RARE:
        return RarityColor_Rare;
    case EItemRarity::RARITY_EPIC:
        return RarityColor_Epic;
    case EItemRarity::RARITY_LEGENDARY:
        return RarityColor_Legendary;
    default:
        UE_LOG(LogTemp, Error, TEXT("GetColorForRarity(): No such color"));
        return RarityColor_Common;
    }
}
