#include "FPSProject.h"

#include "Online/CommManager.h"
#include "Slate.h"
#include "Slate/MenuStyles.h"

class FPSProjectGameModule : public FDefaultGameModuleImpl {
    // Called whenever the module is starting up. Unregister any style sets (which may have been added elsewhere) and initialize stylesets
    virtual void StartupModule() override {
        FSlateStyleRegistry::UnRegisterSlateStyle(FMenuStyles::GetStyleSetName());
        FMenuStyles::Initialize();
    }

    // Called whenever the module is shutting down, shut also down menu styles
    virtual void ShutdownModule() override {
        FMenuStyles::Shutdown();
    }
};

IMPLEMENT_PRIMARY_GAME_MODULE( FPSProjectGameModule, FPSProject, "FPSProject" );
