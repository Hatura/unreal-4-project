

#pragma once

#include "GameFramework/Actor.h"
#include "BaseRecipe.h"
#include "RecipeManager.generated.h"

/**
 * 
 */
USTRUCT()
struct FRecipeList {
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, Category = RecipeListEntry)
    FString RecipeName;

    UPROPERTY(EditDefaultsOnly, Category = RecipeListEntry)
    TSubclassOf<class ABaseRecipe> Recipe;
};

struct FKnownRecipeEntry {
    class ABaseRecipe* Recipe;

    FKnownRecipeEntry(ABaseRecipe *InRecipe)
        : Recipe(InRecipe)
    {}
};

UCLASS()
class FPSPROJECT_API ARecipeManager : public AActor
{
    GENERATED_UCLASS_BODY()

//    void Tick(float DeltaSeconds);

    // Blueprint assigned existant recipes | Add recipes per blueprint to this to initialize start recipes!
    UPROPERTY(EditDefaultsOnly, Category = RecipeManager)
    TArray<FRecipeList> AssignedRecipes;

    UPROPERTY(Transient, Replicated, BlueprintReadOnly, Category = RecipeManager)
    bool IsRMInitialized;

    // This one is used for replication, since you can't replicate SmartPointers
    UPROPERTY(ReplicatedUsing = OnRep_ArrayUpdated)
    TArray<class ABaseRecipe*> KnownRecipesREP;

    // this is synced up with the replicated noe, used in slate SListView
    TArray<TSharedPtr<FKnownRecipeEntry>> KnownRecipes;

    bool SlateNeedsUpdate;

    // This function is server only, called in Character or PlayerController
    UFUNCTION()
    void InitializeRecipesForOwner(UWorld *InitWorld);

    // Teach Recipe, is called by blueprintusable for example
    UFUNCTION()
    void TeachRecipe(TSubclassOf<ABaseRecipe> RecipeToTeach);

    // Set the array updated, so Slate knows it already has the last data
    UFUNCTION()
    void SetUpdated();

    // Only called on Clients, update Arrays here, Server has to do it on changing the contents
    UFUNCTION()
    void OnRep_ArrayUpdated();

private:
    UWorld* ParentWorld;
	
};
