#include "FPSProject.h"

#include "BaseRecipe.h"


ABaseRecipe::ABaseRecipe(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    bReplicates = true;

    // Default inits
    MaxRefiningLevel        = 5;
    CurrentRefiningLevel    = 1;
}

float ABaseRecipe::GetTotalCraftingTime() {
    return BaseCraftingTime;
}

FString ABaseRecipe::GetTotalCraftingTimeString() {
    return HaturHelper::ConvertToTime(GetTotalCraftingTime());
}

bool ABaseRecipe::HasAnotherRefiningLevel() const {
    return CurrentRefiningLevel >= MaxRefiningLevel? false : true;
}

bool ABaseRecipe::Server_AddRefiningExperience_Validate(float Amount) {
    return true;
}

void ABaseRecipe::Server_AddRefiningExperience_Implementation(float Amount) {
    CurrentRefiningEP += Amount;

    if (HasAnotherRefiningLevel() && CurrentRefiningEP >= GetRefEPForLevel(CurrentRefiningLevel+1)) {
        UE_LOG(LogTemp, Log, TEXT("BaseRecipe: LevelUp"));
        AddRefiningLevel();
    }
}

void ABaseRecipe::AddRefiningLevel() {
    if (Role != ROLE_Authority) {
        return;
    }

    CurrentRefiningLevel += 1;
}

float ABaseRecipe::GetRefEPForLevel(int8 Level) const {
    return FPlatformMath::FloorToInt(FMath::Pow(Level, 1.5f) * 40 + 50);
}

void ABaseRecipe::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME_CONDITION(ABaseRecipe, CurrentRefiningEP, COND_OwnerOnly);
    DOREPLIFETIME_CONDITION(ABaseRecipe, CurrentRefiningLevel, COND_OwnerOnly);
}
