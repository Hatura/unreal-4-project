#include "FPSProject.h"

#include "UnrealNetwork.h"
#include "FPSPlayerController.h"
#include "GameHUD.h"
#include "Slate/SGameUI.h"
#include "RecipeManager.h"


ARecipeManager::ARecipeManager(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    bReplicates = true;

//    PrimaryActorTick.bCanEverTick = true;

    IsRMInitialized = false;
    SlateNeedsUpdate = false;
}

void ARecipeManager::InitializeRecipesForOwner(UWorld* InitWorld) {
    if (!InitWorld) {
        UE_LOG(LogTemp, Error, TEXT("Can't initialize RecipeManager, World is null"));
        return;
    }

    ParentWorld = InitWorld;

    IsRMInitialized = false;

    for (int i = 0; i < AssignedRecipes.Num(); i++) {
        TSubclassOf<ABaseRecipe> RecipeToSpawn = AssignedRecipes[i].Recipe;
        ABaseRecipe* SpawnedRecipe = ParentWorld->SpawnActor<ABaseRecipe>(RecipeToSpawn);
        SpawnedRecipe->SetOwner(this);
        KnownRecipesREP.Add(SpawnedRecipe);
    }

    UE_LOG(LogTemp, Log, TEXT("Updating TSharedPtr Recipe-Array on Init"));
    KnownRecipes.Reset();
    for (int i = 0; i < KnownRecipesREP.Num(); i++) {
        KnownRecipes.Add(MakeShareable(new FKnownRecipeEntry(KnownRecipesREP[i])));
    }
    SlateNeedsUpdate = true;

    IsRMInitialized = true;
}

void ARecipeManager::TeachRecipe(TSubclassOf<ABaseRecipe> RecipeToTeach) {
    if (Role != ROLE_Authority) {
        UE_LOG(LogTemp, Warning, TEXT("TeachRecipe(): Am not the authority"));
        return;
    }

    if (!ParentWorld) {
        UE_LOG(LogTemp, Error, TEXT("TeachRecipe(): No parent world"));
        return;
    }

    ABaseRecipe* SpawnedRecipe = ParentWorld->SpawnActor<ABaseRecipe>(RecipeToTeach);
    SpawnedRecipe->SetOwner(this);
    KnownRecipesREP.Add(SpawnedRecipe);

    KnownRecipes.Reset();
    for (int32 i = 0; i < KnownRecipesREP.Num(); i++) {
        KnownRecipes.Add(MakeShareable(new FKnownRecipeEntry(KnownRecipesREP[i])));
    }

    SlateNeedsUpdate = true;
}

void ARecipeManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME_CONDITION(ARecipeManager, IsRMInitialized, COND_OwnerOnly);
    DOREPLIFETIME_CONDITION(ARecipeManager, KnownRecipesREP, COND_OwnerOnly);
}

//void ARecipeManager::Tick(float DeltaSeconds) {
//    Super::Tick(DeltaSeconds);
//}

void ARecipeManager::SetUpdated() {
    SlateNeedsUpdate = false;
}

void ARecipeManager::OnRep_ArrayUpdated() {
    if (IsRMInitialized) {
        UE_LOG(LogTemp, Log, TEXT("Updating TSharedPtr Recipe-Array"));
        KnownRecipes.Reset();
        for (int i = 0; i < KnownRecipesREP.Num(); i++) {
            KnownRecipes.Add(MakeShareable(new FKnownRecipeEntry(KnownRecipesREP[i])));
        }
        SlateNeedsUpdate = true;
    }
}
