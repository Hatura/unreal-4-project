

#pragma once

#include "GameFramework/Actor.h"
#include "Inventory/Inventory.h"
#include "Inventory/BaseItem.h"
#include "Buildings/BaseBuilding.h"
#include "BaseRecipe.h"
#include "CraftingManager.generated.h"

class AFPSCharacter;

/**
 * SetCharacterowner has to be called first!
 */
UCLASS()
class FPSPROJECT_API ACraftingManager : public AActor
{
	GENERATED_UCLASS_BODY()

    void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintCallable, Category = "Crafting | Initialization")
    void SetCharacterOwner(AFPSCharacter* Character);

    UFUNCTION(BlueprintCallable, Category = "Crafting")
    void StartCrafting(ABaseRecipe* Recipe, int32 NumTimes = 1);

    UFUNCTION(BlueprintCallable, Category = "Crafting")
    void PlantBuilding();

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_PlantBuilding(AFPSPlayerController* OwningPlayerController, ABaseRecipe* Recipe, FVector BuildingLocation, FRotator BuildingRotation);

    // Aborts both item and building crafting
    UFUNCTION(BlueprintCallable, Category = "Crafting")
    void AbortCrafting();

    UFUNCTION(BlueprintCallable, Category = "Crafting")
    bool AreResourcesAvailable(ABaseRecipe* Recipe);

    UFUNCTION(BlueprintCallable, Category = "Crafting")
    bool IsResourceInSlotAvailable(ABaseRecipe* Recipe, int32 SlotNumber);

    UFUNCTION(BlueprintCallable, Category = "Crafting")
    int32 GetNumberOfResourceInSlot(ABaseRecipe* Recipe, int32 SlotNumber);

    // Returns whether crafting in general is currently performed
    UFUNCTION(BlueprintCallable, Category = "Crafting")
    bool IsCrafting();

    // Returns whether a building is being crafted
    UFUNCTION(BlueprintCallable, Category = "Crafting")
    bool IsPreviewingBuilding();

    // Returns whether an item is being crafted
    UFUNCTION(BlueprintCallable, Category = "Crafting")
    bool IsCraftingItem();

    // 0.0f - 1.0f to item finished (percent / 100)
     UFUNCTION(BlueprintCallable, Category = "Crafting")
    float GetItemCraftingPercent();
	
private:
    UPROPERTY(Replicated)
    AFPSCharacter* OwningCharacter;

    // Stored variables, CraftingItem can be Item or Building
    AActor* CraftingItem;
    ABaseRecipe* CraftingRecipe;
    bool CraftingInProgress;
    bool ShowPreviewBuilding;
    int32 NumItemCraftTimes;

    // Craft an item, calls Inventory functions
    UFUNCTION()
    void StartItemCrafting();

    // Start crafting a building, previews the position, updated on tick
    UFUNCTION()
    void ShowBuildingPreview();

    // Finish the item crafting (another resource check)
    UFUNCTION()
    void FinishItemCrafting();

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_FinishItemCrafting(ABaseRecipe* Recipe);

    UFUNCTION()
    void ResetVariables();
};
