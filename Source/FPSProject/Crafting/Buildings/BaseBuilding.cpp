#include "FPSProject.h"

#include "Crafting/BaseRecipe.h"
#include "FPSPlayerController.h"
#include "FPSCharacter.h"
#include "BaseBuilding.h"

// TODO: Somehow update the Owner, which is a Character, on death of the Owner, so the new Character of the PlayerContrloler can actually still see and interact with this

ABaseBuilding::ABaseBuilding(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    BuildingMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Building_BuildingMesh"));
    //BuildingMesh->SetOnlyOwnerSee(true);

    RootComponent = BuildingMesh;

    InfoTextComponent = PCIP.CreateDefaultSubobject<UTextRenderComponent>(this, TEXT("Building_InfoTextComponent"));
    InfoTextComponent->SetHorizontalAlignment(EHorizTextAligment::EHTA_Center);
    InfoTextComponent->SetWorldSize(12);
    InfoTextComponent->SetTextRenderColor(FColor(0, 0, 255));
    InfoTextComponent->SetCastShadow(false);
    InfoTextComponent->bCastDynamicShadow = false;
    InfoTextComponent->SetText("NO INIT");
    InfoTextComponent->AttachParent = BuildingMesh;

    InteractionInfoText = BuildingName;

    static ConstructorHelpers::FObjectFinder<UMaterialInterface>Wireframe_Material(TEXT("Material'/Game/Materials/M_Wireframe.M_Wireframe'"));

    WireframeMaterial = Wireframe_Material.Object;
    UsingWireframe = false;

    DynamicWireframe_M = UMaterialInstanceDynamic::Create(WireframeMaterial, this);

    bReplicates = true;
    bReplicateMovement = true;

    HasPostBuildInteraction = false;
    IsBuilt = false;
}

void ABaseBuilding::PostInitializeComponents() {
    Super::PostInitializeComponents();

    // Set the Material of the Owner to Wireframe
    if (Role == ROLE_Authority) {
        Client_SetMaterials(true);
    }

    // Hide all subcomponents on preview plant
    TArray<USceneComponent*> BuildingMeshChildren;

    BuildingMesh->GetChildrenComponents(true, BuildingMeshChildren);

    if (BuildingMeshChildren.Num() > 0) {
        for (USceneComponent* Component : BuildingMeshChildren) {
            Component->SetVisibility(false, true);
        }
    }
}

bool ABaseBuilding::Client_SetMaterials_Validate(bool EnableWireframe) {
    return true;
}

void ABaseBuilding::Client_SetMaterials_Implementation(bool EnableWireframe) {
    if (EnableWireframe) {
        // Set to wireframe
        for (int i = 0; true; i++) {
            UMaterialInterface* MatInterface = BuildingMesh->GetMaterial(i);
            if (MatInterface != nullptr) {
                RootMaterials.Add(BuildingMesh->GetMaterial(i));
                BuildingMesh->SetMaterial(i, DynamicWireframe_M);
            }
            else {
                break;
            }
        }
    }
    else if (RootMaterials.Num() == BuildingMesh->GetNumMaterials()) {
        // Reverese to original materials
        for (int i = 0; true; i++) {
            UMaterialInterface* MatInterface = BuildingMesh->GetMaterial(i);
            if (MatInterface != nullptr && RootMaterials.Num() >= i) {
                BuildingMesh->SetMaterial(i, RootMaterials[i]);
            }
            else {
                break;
            }
        }
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("Couln't set materials, because !EnableWireframe and Num() != NumMaterials()"));
    }
}

void ABaseBuilding::UpdateWireframeMaterial(bool IsIllegalPosition) {
    HaturHelper::QL("Updating Wireframe Color", IsIllegalPosition);

    HaturHelper::QL("Callback 3", GetActorLocation().ToString());

    if (IsIllegalPosition) {
        DynamicWireframe_M->SetVectorParameterValue(FName(TEXT("TintColor")), FLinearColor(1.0f, 0.0f, 0.0f, 1.0f));
    }
    else {
        DynamicWireframe_M->SetVectorParameterValue(FName(TEXT("TintColor")), FLinearColor(0.0f, 1.0f, 0.0f, 1.0f));
    }
}

void ABaseBuilding::InitializeBuilding(ABaseRecipe *Recipe) {
    Server_InitializeBuilding(Recipe);
}

bool ABaseBuilding::Server_InitializeBuilding_Validate(ABaseRecipe* Recipe) {
    return true;
}

void ABaseBuilding::Server_InitializeBuilding_Implementation(ABaseRecipe* Recipe) {
    if (Recipe->Ingredients.Num() == 0) {
        UE_LOG(LogTemp, Error, TEXT("InitializeBuilding(): RecipeIngredients size is 0"));
        return;
    }

    // Transfer and initialize Ingredients from Recipe to Building
    for (FItemIngredient &RecipeIngredient : Recipe->Ingredients) {
        FBuildingIngredient TemporaryIngredient;
        TemporaryIngredient.IngredientClass = RecipeIngredient.IngredientClass;
        TemporaryIngredient.AlternativeIngredients = RecipeIngredient.AlternativeIngredients;
        TemporaryIngredient.NecessaryAmount = RecipeIngredient.Amount;

        Ingredients.Add(TemporaryIngredient);
    }

    UpdateInteractionInfoText();
}

void ABaseBuilding::AddResourceType(AFPSCharacter *Character) {
    if (!Character->MyInventoryInstance || !Character->MyInventoryInstance->isInitialized) {
        UE_LOG(LogTemp, Error, TEXT("AddResourceType(): No Inventory instance or inventory instance not initialized"));
        return;
    }

    for (int i = 0; i < Ingredients.Num(); i++) {
        FBuildingIngredient &Ingredient = Ingredients[i];
//    for (FBuildingIngredient &Ingredient : Ingredients) {
        if (Ingredient.CurrentAmount >= Ingredient.NecessaryAmount) {
            // >= just for fail safe | this resource condition is already met!
            continue;
        }

        int32 AmountAvailable= Character->MyInventoryInstance->GetNumberOfResourceAvailable(Ingredient.IngredientClass, Ingredient.AlternativeIngredients);
        if (AmountAvailable > 0) {
            // We actually have that kind of resource
            Server_AddResourceType(Character, i);
            break;
        }

        // Resource not found in inventory
    }
}

bool ABaseBuilding::Server_AddResourceType_Validate(AFPSCharacter* Character, int32 IngrIndex) {
    return true;
}

void ABaseBuilding::Server_AddResourceType_Implementation(AFPSCharacter* Character, int32 IngrIndex) {
    if (GEngine) {
        for (FBuildingIngredient &Ingredient : Ingredients) {
            FString LogString = "INGAMOUNT: ";
            LogString.Append(FString::FromInt(Ingredients[IngrIndex].CurrentAmount));
            GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Magenta, LogString);
        }
    }

    if (IngrIndex > Ingredients.Num()) {
        UE_LOG(LogTemp, Error, TEXT("Server_AddresourceType(): Illegal ingredients-index"))
        return;
    }

    if (!Character || !Character->IsAlive) {
        // Character disappeared or died in the meantime
        return;
    }

    if (!Character->MyInventoryInstance || !Character->MyInventoryInstance->isInitialized) {
        UE_LOG(LogTemp, Warning, TEXT("Character tried to craft something but has no legit Inventoryinstance"))
        return;
    }

    // Check if the Character still has the resource in his inventory and get the amount he has
    int32 AmountAvailable = Character->MyInventoryInstance->GetNumberOfResourceAvailable(Ingredients[IngrIndex].IngredientClass, Ingredients[IngrIndex].AlternativeIngredients);

    if (AmountAvailable <= 0) {
        // Resource amount changed, abort
        return;
    }
    else if (AmountAvailable > 0) {
        int32 AmountNecessary = Ingredients[IngrIndex].NecessaryAmount - Ingredients[IngrIndex].CurrentAmount;
        int32 AmountTaken = Character->MyInventoryInstance->AcquireResource(Ingredients[IngrIndex].IngredientClass, Ingredients[IngrIndex].AlternativeIngredients, AmountNecessary);

        if (AmountTaken == -1) {
            // No Server error, abort
            UE_LOG(LogTemp, Error, TEXT("Server_AddResourceType(): Illegal amount taken"));
            return;
        }

        Ingredients[IngrIndex].CurrentAmount += AmountTaken;
    }

    bool GotAllResources = true;

    // Check if Building got constructed after that
    for (FBuildingIngredient Ingredient : Ingredients) {
        if (Ingredient.CurrentAmount < Ingredient.NecessaryAmount) {
            // Don't have the necessary amount, set to false
            GotAllResources = false;
            break;
        }
    }

    if (GotAllResources) {
        ActivateBuilding();
    }
    else {
        UE_LOG(LogTemp, Log, TEXT("Not all resources yet!"));
    }

    UpdateInteractionInfoText();

//    if (GEngine) {
//        FString LogString = "Result of Server-Query [Item: ";
//        LogString.Append(Ingredients[IngrIndex].IngredientClass.GetDefaultObject()->ItemName);
//        LogString.Append("], [CurrentAmount: ");
//        LogString.Append(FString::FromInt(Ingredients[IngrIndex].CurrentAmount));
//        LogString.Append("], [Necessary Amount: ");
//        LogString.Append(FString::FromInt(Ingredients[IngrIndex].NecessaryAmount));
//        LogString.Append("]");
//        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Red, LogString);
//    }
}

void ABaseBuilding::ActivateBuilding() {
    if (Role != ROLE_Authority) {
        UE_LOG(LogTemp, Error, TEXT("Tried to activate building, but I'm not the server!"));
        return;
    }

    IsBuilt = true;

    // Make child components visible
    TArray<USceneComponent*> BuildingMeshChildren;

    BuildingMesh->GetChildrenComponents(true, BuildingMeshChildren);

    // This needs to be called on all clients actually TODO
    if (BuildingMeshChildren.Num() > 0) {
        for (USceneComponent* Component : BuildingMeshChildren) {
            // Activate everything but InfoTextComponent
            if (Component != InfoTextComponent) {
                Component->SetVisibility(true, true);
            }
        }
    }

    // Update Material to Rootmaterial and remove Rootmaterial entries
    Client_SetMaterials(false);

    UE_LOG(LogTemp, Log, TEXT("Activated Building!"));
}

void ABaseBuilding::UpdateInteractionInfoText() {
    if (Role != ROLE_Authority) {
        UE_LOG(LogTemp, Warning, TEXT("UpdateInteractionInfoText(): I am not the server"));
        return;
    }

    if (IsBuilt) {
        // Set up String for post build interaction
        if (HasPostBuildInteraction) {
            InteractionInfoText = "Press 'E'' to use";
        }
        else {
            InteractionInfoText = "EMPTY"; // change to "" after making sure that it isn't shown
        }
    }
    else {
        // Set up String for resource information
        InteractionInfoText = "I need the following:";
        for (FBuildingIngredient &IngredientEntry : Ingredients) {
            InteractionInfoText.Append("<br>");
            InteractionInfoText.Append(IngredientEntry.IngredientClass.GetDefaultObject()->ItemName);
            InteractionInfoText.Append(": ");
            InteractionInfoText.Append(FString::FromInt(IngredientEntry.CurrentAmount));
            InteractionInfoText.Append(" / ");
            InteractionInfoText.Append(FString::FromInt(IngredientEntry.NecessaryAmount));
        }
    }

    InfoTextComponent->SetText(InteractionInfoText);
}

bool ABaseBuilding::GetBuilt() {
    return IsBuilt;
}

void ABaseBuilding::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME_CONDITION(ABaseBuilding, Ingredients, COND_OwnerOnly);
    //DOREPLIFETIME(ABaseBuilding, Ingredients);
    DOREPLIFETIME(ABaseBuilding, IsBuilt);

//    if (!IsBuilt) {
//        DOREPLIFETIME_CONDITION(ABaseBuilding, InteractionInfoText, COND_OwnerOnly);
//    }
//    else {
    // This is a VERY VERY bad solution for now, Clients should construct the InteractionInfoText for themselves, also this does not need to be replicated to everyone
    // But a !IsBuilt check does not work here, why? Dunno.. TODO
    DOREPLIFETIME(ABaseBuilding, InteractionInfoText);
//    }
}

void ABaseBuilding::OnRep_UpdateIIT() {
    UE_LOG(LogTemp, Log, TEXT("Got request to update IIT"));

    InfoTextComponent->SetText(InteractionInfoText);
}

/**
 * IInteractableObjectInterface Implementation
 */

FString ABaseBuilding::GetInteractionInfoText() {
    return InteractionInfoText;
}

void ABaseBuilding::ShowInteractionInfoText(AActor *ActorInteracting) {
    if (ActorInteracting != nullptr) {
        // Rotate the InfoTextComponent to face the interacting Actor (usually a character), this still needs improvement (ignore campfire rotation)! TODO
        FVector ActorCamLoc;
        AFPSCharacter* Char = Cast<AFPSCharacter>(ActorInteracting);
        if (Char) {
            ActorCamLoc = Char->FirstPersonCameraComponent->GetComponentLocation();
        }
        else {
            ActorCamLoc = ActorInteracting->GetActorLocation();
        }

        FVector InfoTextLoc = InfoTextComponent->GetComponentLocation();

        FVector SVector = (InfoTextLoc - ActorCamLoc) * FVector(1.0f, 1.0f, 0);
        SVector.Normalize();
        FRotator NewRot = SVector.Rotation();
        NewRot.Add(0, -180.0f, 0);

        InfoTextComponent->SetRelativeRotation(NewRot);
    }

    InfoTextComponent->SetVisibility(true);

    GetWorldTimerManager().SetTimer(this, &ABaseBuilding::HideInteractionInfoText, 0.2f, false);
}

void ABaseBuilding::HideInteractionInfoText() {
    InfoTextComponent->SetVisibility(false);
}

void ABaseBuilding::SetHighlighted(float ForSeconds) {
    // This is NOT replicated!
    BuildingMesh->SetRenderCustomDepth(true);
    GetWorldTimerManager().SetTimer(this, &ABaseBuilding::Unhighlight, ForSeconds, false);
}

void ABaseBuilding::Unhighlight() {
    BuildingMesh->SetRenderCustomDepth(false);
}

TEnumAsByte<EInteractionResult::InteractionResult> ABaseBuilding::Interact(AActor *ActorInteracting) {
    UE_LOG(LogTemp, Log, TEXT("BaseBuilding::Interact"));

    if (!ActorInteracting) {
        UE_LOG(LogTemp, Warning, TEXT("BaseBuilding::Interact(): Illegal or no Actor"))
        return EInteractionResult::IA_ERROR;
    }

    // Is not built yet? Add resources
    if (!IsBuilt && ActorInteracting == GetOwner()) {
        UE_LOG(LogTemp, Log, TEXT("Buildingprocess"));

        AFPSCharacter *Character = Cast<AFPSCharacter>(ActorInteracting);
        if (!Character || !Character->IsAlive) {
            UE_LOG(LogTemp, Warning, TEXT("BaseBuilding::Interact(): Actor not character in build process or is dead"));
            return EInteractionResult::IA_ERROR;
        }

        AddResourceType(Character);

        return EInteractionResult::IA_INTERACTED;
    }

    return EInteractionResult::IA_NOTINTERACTED;
}
