

#include "FPSProject.h"
#include "Fireplace.h"


AFireplace::AFireplace(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    HasPostBuildInteraction = true;

    FireParticleEmitter = PCIP.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("Fireplace_FireParticleEmitter"));
    FireParticleEmitter->AttachParent = BuildingMesh;

    LightHelper = PCIP.CreateDefaultSubobject<UPointLightComponent>(this, TEXT("Fireplace_LightHelper"));
    LightHelper->AttachParent = BuildingMesh;
}

void AFireplace::ActivateBuilding() {
    Super::ActivateBuilding();

    // Override the behaviour in BaseBuilding, which is activating everything!
    FireParticleEmitter->SetVisibility(false, true);
    LightHelper->SetVisibility(false, true);
}

bool AFireplace::Server_ToggleFire_Validate() {
    return true;
}

void AFireplace::Server_ToggleFire_Implementation() {
    UE_LOG(LogTemp, Log, TEXT("Server Toggling Fire"));
    FireEnabled = FireEnabled ? false : true;

    // Enable on Server
    FireParticleEmitter->SetVisibility(FireEnabled, true);
    LightHelper->SetVisibility(FireEnabled, true);
}

void AFireplace::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AFireplace, FireEnabled);
}

void AFireplace::OnRep_FireplaceStatus() {
    FireParticleEmitter->SetVisibility(FireEnabled, true);
    LightHelper->SetVisibility(FireEnabled, true);
}

/**
  * IInteractableObjectInterface overrides
  */

TEnumAsByte<EInteractionResult::InteractionResult> AFireplace::Interact(AActor *ActorInteracting) {
    // Do Super::Interact() and save result
    TEnumAsByte<EInteractionResult::InteractionResult> SuperInteraction = Super::Interact(ActorInteracting);

    if (IsBuilt && SuperInteraction == EInteractionResult::IA_NOTINTERACTED) {
        UE_LOG(LogTemp, Log, TEXT("Toggling Fire"));;
        Server_ToggleFire();
        return EInteractionResult::IA_INTERACTED;
    }

    return EInteractionResult::IA_NOTINTERACTED;
}
