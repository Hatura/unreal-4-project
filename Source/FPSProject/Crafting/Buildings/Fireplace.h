

#pragma once

#include "BaseBuilding.h"
#include "ParticleHelper.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Fireplace.generated.h"

/**
 * A parent class for for InteractableBuildings that behave like campfires
 */
UCLASS(Abstract)
class FPSPROJECT_API AFireplace : public ABaseBuilding
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Fireplace)
    TSubobjectPtr<UParticleSystemComponent> FireParticleEmitter;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Fireplace)
    TSubobjectPtr<UPointLightComponent> LightHelper;

    TEnumAsByte<EInteractionResult::InteractionResult> Interact(AActor *ActorInteracting);

private:
    UPROPERTY(ReplicatedUsing = OnRep_FireplaceStatus)
    bool FireEnabled;

    void ActivateBuilding() override;

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_ToggleFire();

    UFUNCTION()
    void OnRep_FireplaceStatus();
	
};
