#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/InteractableObjectInterface.h"
#include "Inventory/BaseItem.h"
#include "BaseBuilding.generated.h"

// Forward declaration
class ABaseRecipe;
class AFPSCharacter;

/**
 * 
 */
USTRUCT()
struct FBuildingIngredient {
    GENERATED_USTRUCT_BODY()

    FBuildingIngredient() {
        CurrentAmount = 0;
    }

    UPROPERTY(EditDefaultsOnly, Category = "Building | Ingredient")
    TSubclassOf<class ABaseItem> IngredientClass;

    // Alternatives, for example if there is no importance of the item being palm wood or oak wood
    UPROPERTY(EditDefaultsOnly, Category = "Building | Ingredient")
    TArray<TSubclassOf<ABaseItem>> AlternativeIngredients;

    UPROPERTY(EditDefaultsOnly, Category = "Building | Ingredient")
    int32 NecessaryAmount;

    UPROPERTY(BlueprintReadOnly, Category = "Building | Ingredient")
    int32 CurrentAmount;
};

UCLASS(Abstract)
class FPSPROJECT_API ABaseBuilding : public AActor, public IInteractableObjectInterface
{
	GENERATED_UCLASS_BODY()

    void PostInitializeComponents();

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Building)
    TSubobjectPtr<UStaticMeshComponent> BuildingMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Building)
    FString BuildingName;

    // Enum from BaseItem, but it's the same so just take it
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Building)
    TEnumAsByte<EItemRarity::ItemRarity> BuildingRarity;

    UPROPERTY(BlueprintReadOnly, Replicated, Category = Building)
    TArray<FBuildingIngredient> Ingredients;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Building)
    TSubobjectPtr<UTextRenderComponent> InfoTextComponent;

    // Can the building be interacted with after completing it?
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Building)
    bool HasPostBuildInteraction;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Building)
    class UTexture2D* BuildingTexture;

    UPROPERTY(BlueprintReadOnly, Category = Building)
    bool UsingWireframe;

    // Only called on the owning client = Character
    UFUNCTION(Client, Reliable, WithValidation)
    void Client_SetMaterials(bool EnableWireframe);

    // update material color if the building is illegally overlapping, this is done clientside to save resources
    void UpdateWireframeMaterial(bool IsIllegalPosition);

    UFUNCTION(BlueprintCallable, Category = Building)
    void InitializeBuilding(ABaseRecipe* Recipe);

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_InitializeBuilding(ABaseRecipe* Recipe);

    UFUNCTION(BlueprintCallable, Category = Building)
    bool GetBuilt();

    /**
     * From Interface
     */

    virtual FString GetInteractionInfoText();

    virtual void ShowInteractionInfoText(AActor *ActorInteracting = nullptr);

    virtual void HideInteractionInfoText();

    virtual void SetHighlighted(float ForSeconds);

    virtual TEnumAsByte<EInteractionResult::InteractionResult> Interact(AActor* ActorInteracting);

protected:
    UPROPERTY(Replicated)
    bool IsBuilt;

    class UMaterialInterface* WireframeMaterial;

    class UMaterialInstanceDynamic* DynamicWireframe_M;

    // Server only, internally called when resource requirements are met to activate building
    UFUNCTION()
    virtual void ActivateBuilding();

private:
    UPROPERTY(ReplicatedUsing = OnRep_UpdateIIT)
    FString InteractionInfoText;

    // Only be called from server
    UFUNCTION()
    void UpdateInteractionInfoText();

    // Saved material, to reset on client to original Materials
    TArray<UMaterialInterface*> RootMaterials;

    // Internally called to add a resource to the Building, calls the server function
    UFUNCTION()
    void AddResourceType(AFPSCharacter* Character);

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_AddResourceType(AFPSCharacter* Character, int32 IngrIndex);

    // Update Interaction Info Text
    UFUNCTION()
    void OnRep_UpdateIIT();

    // From Interface
    virtual void Unhighlight();

};
