#pragma once

#include "GameFramework/Actor.h"
#include "Buildings/CraftingStation.h"
#include "Inventory/BaseItem.h"
#include "Interfaces/CraftableObjectInterface.h"
#include "BaseRecipe.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
namespace ECreatesObjectType {
    enum CreatesObjectType {
        OBJECT_ITEM,
        OBJECT_BUILDING
    };
}

UENUM(BlueprintType)
namespace ERefiningLevel {
    enum RefiningLevel {
        R_LEVEL1    UMETA(DisplayName="I"),
        R_LEVEL2    UMETA(DisplayName="II"),
        R_LEVEL3    UMETA(DisplayName="III"),
        R_LEVEL4    UMETA(DisplayName="IV"),
        R_LEVEL5    UMETA(DisplayName="V")
    };
}

USTRUCT()
struct FItemIngredient {
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, Category = "Recipe | Ingredient")
    TSubclassOf<class ABaseItem> IngredientClass;

    // Alternatives, for example if there is no importance of the item being palm wood or oak wood
    UPROPERTY(EditDefaultsOnly, Category = "Recipe | Ingredient")
    TArray<TSubclassOf<class ABaseItem>> AlternativeIngredients;

    UPROPERTY(EditDefaultsOnly, Category = "Recipe | Ingredient")
    int32 Amount;
};

USTRUCT()
struct FAssociatedCraftingStation {
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, Category = "Recipe | Associated Crafting Stations")
    TSubclassOf<class ACraftingStation> CraftingStationClass;
};

UCLASS(Abstract)
class FPSPROJECT_API ABaseRecipe : public AActor
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Recipe)
    FString DisplayName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Recipe)
    float BaseCraftingTime;

    // Can craft wihtout any stations
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Recipe)
    bool IsAlwaysCraftable;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Recipe)
    TArray<FAssociatedCraftingStation> AssociatedCraftingStations;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Recipe)
    TArray<FItemIngredient> Ingredients;

    // Create ABaseItem class or ABaseBuilding? (Or other stuff later)
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Recipe)
    TEnumAsByte<ECreatesObjectType::CreatesObjectType> CreatesObjectType;

    /**
      * One of them is ignored, depending on CreatesObjectType-Enum chosen
      */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Recipe)
    TSubclassOf<class ABaseItem> CreatesItem;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Recipe)
    TSubclassOf<class ABaseBuilding> CreatesBuilding;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Recipe)
    FSlateBrush SlateBrush;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated, Category = "Recipe | Refining")
    float CurrentRefiningEP;

    UPROPERTY(BlueprintReadOnly, Replicated, Category = "Recipe | Refining")
    int8 CurrentRefiningLevel;

    UFUNCTION(BlueprintCallable, Category = Recipe)
    float GetTotalCraftingTime();

    UFUNCTION(BlueprintCallable, Category = Recipe)
    FString GetTotalCraftingTimeString();

    // Can refening still be leveled up? If yes, can add EP
    UFUNCTION(BlueprintCallable, Category = "Recipe | Refining")
    bool HasAnotherRefiningLevel() const;

    // Server function to increase refiningexperience and check / do level up
    UFUNCTION(Server, Reliable, WithValidation)
    void Server_AddRefiningExperience(float Amount);

    UFUNCTION(BlueprintCallable, Category = "Recipe | Refining")
    float GetRefEPForLevel(int8 Level) const;

private:
    int8 MaxRefiningLevel;

    UFUNCTION()
    void AddRefiningLevel();
};
