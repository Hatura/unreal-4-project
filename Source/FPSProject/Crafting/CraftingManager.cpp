

#include "FPSProject.h"
#include "FPSCharacter.h"
#include "CraftingManager.h"

// TODO (Optional): Maybe switch to a queue crafting system and seperate the item crafting in it's own class?

ACraftingManager::ACraftingManager(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    bReplicates = true;
    bAlwaysRelevant = true;
    PrimaryActorTick.bCanEverTick = true;

    CraftingInProgress = false;
    ShowPreviewBuilding = false;

    // default init to prevent any errors
    NumItemCraftTimes = 1;
}

void ACraftingManager::SetCharacterOwner(AFPSCharacter *Character) {
    OwningCharacter = Character;
}

void ACraftingManager::StartCrafting(ABaseRecipe *Recipe, int32 NumTimes) {
    if (!OwningCharacter || !OwningCharacter->MyInventoryInstance) {
        UE_LOG(LogTemp, Error, TEXT("StartCrafting(): No Character or Inventory Instance"));
        return;
    }

    if (CraftingInProgress) {
        return;
    }

    CraftingInProgress = true;
    CraftingRecipe = Recipe;

    switch (Recipe->CreatesObjectType) {
    case ECreatesObjectType::OBJECT_BUILDING:
        ShowBuildingPreview();
        break;
    case ECreatesObjectType::OBJECT_ITEM:
        // Has resources?
        if (!AreResourcesAvailable(Recipe)) {
            ResetVariables();
            return;
        }

        NumItemCraftTimes = NumTimes;
        StartItemCrafting();
        break;
    default:
        UE_LOG(LogTemp, Error, TEXT("No such object type"));
        ResetVariables();
        return;
    }
}

void ACraftingManager::StartItemCrafting() {
    UE_LOG(LogTemp, Log, TEXT("-> StartItemCrafting()"));

    GetWorldTimerManager().SetTimer(this, &ACraftingManager::FinishItemCrafting, CraftingRecipe->GetTotalCraftingTime(), false);

    //ObjectToCraft = ItemToCraft;

    //GetWorldTimerManager().SetTimer(this, &ACraftingManager::FinishCrafting, ItemCooldown, false);
}

void ACraftingManager::FinishItemCrafting() {
    UE_LOG(LogTemp, Log, TEXT("-> FinishItemCrafting()"));

    if (CraftingRecipe == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("Error, got to FinishItemCrafting() but CraftinRecipe == nullptr"));
        return;
    }

    Server_FinishItemCrafting(CraftingRecipe);

    NumItemCraftTimes -= 1;
    // Are there more things to craft? Go on!
    if (NumItemCraftTimes > 1) {
        GetWorldTimerManager().SetTimer(this, &ACraftingManager::FinishItemCrafting, CraftingRecipe->GetTotalCraftingTime(), false);
    }
    else {
        // We're done here, reset everything
        ResetVariables();
    }
}

bool ACraftingManager::Server_FinishItemCrafting_Validate(ABaseRecipe* Recipe) {
    return true;
}

void ACraftingManager::Server_FinishItemCrafting_Implementation(ABaseRecipe* Recipe) {
    if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Server_FinishItemCrafting()"); }

    if (Recipe == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("Server_FinishItemCrafting(): Recipe is a nullptr"));
        return;
    }
    if (!OwningCharacter) {
        UE_LOG(LogTemp, Error, TEXT("Server_FinishItemCrafting(): null Character"));
        return;
    }
    if (!OwningCharacter->MyInventoryInstance || !OwningCharacter->MyInventoryInstance->isInitialized) {
        UE_LOG(LogTemp, Error, TEXT("Server_FinishItemCrafting(): Character has no inventory instance or isn't initialized"));
        return;
    }

    OwningCharacter->MyInventoryInstance->CreateCraftedItem(Recipe);
}

void ACraftingManager::ShowBuildingPreview() {
    UE_LOG(LogTemp, Log, TEXT("-> ShowBuildingPreview()"));

    //ObjectToCraft = BuildingToPreview;

    UWorld* MyWorld = OwningCharacter->GetWorld();

    if (!MyWorld) {
        UE_LOG(LogTemp, Error, TEXT("ShowBuildingPreview(): No World"));
        ResetVariables();
        return;
    }

    // Owner of buildings are PlayerControllers, cause their lifetime exceeds the character lifetime
    FActorSpawnParameters SpawnParams;
    SpawnParams.Owner = OwningCharacter;

    CraftingItem = MyWorld->SpawnActor<ABaseBuilding>(CraftingRecipe->CreatesBuilding, SpawnParams);
    //CraftingItem->SetOwner(Cast<AFPSPlayerController>(OwningCharacter->GetController()));

    ShowPreviewBuilding = true;
}

void ACraftingManager::PlantBuilding() {
    // If it isn't possible continue, else
    // [TODO]

    if (CraftingInProgress && ShowPreviewBuilding && CraftingItem != nullptr && CraftingRecipe != nullptr) {
        // Is anything overlapping it? abort ! TODO: Add real filters, TODO: Does this need a server side check? Cause the building isn't there and it's a bit harsh, place destory?
        TArray<AActor*> OverlappingActors;
        CraftingItem->GetOverlappingActors(OverlappingActors);

        if (OverlappingActors.Num() > 0) {
            return;
        }

        // Make sure it really is a building
        ABaseBuilding* Building = Cast<ABaseBuilding>(CraftingItem);
        if (!Building) {
            UE_LOG(LogTemp, Warning, TEXT("Got into PlantBuilding() but CraftingItem was no building!"));
            ResetVariables();
            return;
        }

        // Destroy client-previewed Building and spawn on the same position on the Server
        const FVector BuildingPosition = Building->GetActorLocation();
        const FRotator BuildingRotation = Building->GetActorRotation();

        Building->Destroy();
        Building = nullptr;

        AFPSPlayerController* PCOwner = Cast<AFPSPlayerController>(OwningCharacter->GetController());

        if (GEngine) { FString LogString = "Local Owner of PC: "; LogString.Append(PCOwner->Name); GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Black, LogString); }

        if (!PCOwner) {
            UE_LOG(LogTemp, Error, TEXT("PlantBuilding(): Couldn't cast to PlayerController!"));
            return;
        }

        Server_PlantBuilding(PCOwner, CraftingRecipe, BuildingPosition, BuildingRotation);

        ResetVariables();
    }
    else {
        // Something went wrong
        UE_LOG(LogTemp, Warning, TEXT("Got into PlantBuilding, but a variable contains a illegal value"));
        ResetVariables();
        return;
    }
}

bool ACraftingManager::Server_PlantBuilding_Validate(AFPSPlayerController* OwningPlayerController, ABaseRecipe* Recipe, FVector BuildingLocation, FRotator BuildingRotation) {
    return true;
}

void ACraftingManager::Server_PlantBuilding_Implementation(AFPSPlayerController* OwningPlayerController, ABaseRecipe* Recipe, FVector BuildingLocation, FRotator BuildingRotation) {
    // TODO: Security checks

    UE_LOG(LogTemp, Log, TEXT("Called Server_PlantBuilding()"));

    if (!OwningPlayerController) {
        UE_LOG(LogTemp, Error, TEXT("Server_PlantBuilding(): Illegal PlayerController"));
        return;
    }

    UWorld* MyWorld = GetWorld();

    if (!MyWorld) {
        UE_LOG(LogTemp, Error, TEXT("Server_PlantBuilding(): Couldn't access world"));
        return;
    }

    FActorSpawnParameters SpawnParams;
    SpawnParams.Owner = OwningPlayerController->GetPawn();

    ABaseBuilding* ServerBuilding = MyWorld->SpawnActor<ABaseBuilding>(Recipe->CreatesBuilding, BuildingLocation, BuildingRotation, SpawnParams);
    ServerBuilding->InitializeBuilding(Recipe);
}

void ACraftingManager::AbortCrafting() {
    UE_LOG(LogTemp, Log, TEXT("-> AbortCrafting()"));

    if (CraftingItem) {
        CraftingItem->Destroy();
    }

    if (GetWorldTimerManager().IsTimerActive(this, &ACraftingManager::FinishItemCrafting)) {
        GetWorldTimerManager().ClearTimer(this, &ACraftingManager::FinishItemCrafting);
    }

    ResetVariables();
}

bool ACraftingManager::AreResourcesAvailable(ABaseRecipe* Recipe) {
    UE_LOG(LogTemp, Log, TEXT("-> AreResourcesAvailable()"));

    bool IngredientsAvailable = true;

    for (FItemIngredient Ingredient : Recipe->Ingredients) {
        int32 AmountInInventory = OwningCharacter->MyInventoryInstance->GetNumberOfResourceAvailable(Ingredient.IngredientClass, Ingredient.AlternativeIngredients);
        if (AmountInInventory < Ingredient.Amount) {
            IngredientsAvailable = false;
        }
    }

    return IngredientsAvailable;
}

bool ACraftingManager::IsResourceInSlotAvailable(ABaseRecipe* Recipe, int32 SlotNumber) {
    return true;
}

int32 ACraftingManager::GetNumberOfResourceInSlot(ABaseRecipe* Recipe, int32 SlotNumber) {
    return 0;
}

void ACraftingManager::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);

    if (OwningCharacter && ShowPreviewBuilding && CraftingItem != nullptr) {
//        UE_LOG(LogTemp, Log, TEXT("Showing Preview Building"));

        UWorld* MyWorld = OwningCharacter->GetWorld();

        if (MyWorld) {
            FVector CamLoc;
            FRotator CamRot;

            OwningCharacter->Controller->GetPlayerViewPoint(CamLoc, CamRot);
            const FVector DirectionVector = CamRot.Vector();
            const FVector StartTraceVector = CamLoc;
            const FVector EndTraceVector = StartTraceVector + DirectionVector * 1000;

            FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("BUILDING_PREVIEW_TRACE")), true, this);
            RV_TraceParams.bTraceComplex = true;
            RV_TraceParams.bTraceAsyncScene = true;
            RV_TraceParams.bReturnPhysicalMaterial = false;
            RV_TraceParams.AddIgnoredActor(OwningCharacter);
            RV_TraceParams.AddIgnoredActor(CraftingItem);

            FHitResult RV_Hit(ForceInit);

            if (MyWorld->LineTraceSingle(RV_Hit, StartTraceVector, EndTraceVector, COLLISION_CONSTRUCTION, RV_TraceParams)) {
                // Hit
//                DrawDebugLine(MyWorld, StartTraceVector, EndTraceVector, FColor(0, 255, 0), false, 5, 0, 0.1f);

//                if (RV_Hit.GetActor() && GEngine) {
//                    GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, RV_Hit.GetComponent()->GetCollisionProfileName().ToString());
//                }

                FRotator ActorRotation = RV_Hit.ImpactNormal.Rotation();

                // Respect normals, TODO: Add option to ignore normals!
                ActorRotation.Add(-90.0f, 0, 0);

                CraftingItem->SetActorLocationAndRotation(RV_Hit.ImpactPoint, ActorRotation);
            }

            // Get overlapping actors for preview building and tint it accordingly to the overlapping status
            TArray<AActor*> ActorStorage;
            CraftingItem->GetOverlappingActors(ActorStorage);

            HaturHelper::QL("Actor is overlapping with num Actors: ", ActorStorage.Num());

            if (ActorStorage.Num() > 0) {
                // Overlapping some stuff, TODO: Implement rules for different types if neccessary
                ABaseBuilding *Building = Cast<ABaseBuilding>(CraftingItem);

                if (Building) {
                    Building->UpdateWireframeMaterial(true);
                }
            }
            else {
                // Not overlapping
                ABaseBuilding *Building = Cast<ABaseBuilding>(CraftingItem);

                if (Building) {
                    Building->UpdateWireframeMaterial(false);
                }
            }

        }
        else {
            UE_LOG(LogTemp, Error, TEXT("CraftingManager::Tick(): No World"));
        }
    }
}

bool ACraftingManager::IsCrafting() {
    return CraftingInProgress;
}

bool ACraftingManager::IsPreviewingBuilding() {
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, (CraftingInProgress && ShowPreviewBuilding) ? "Craf" : "!Craf");
    }

    return (CraftingInProgress && ShowPreviewBuilding) ? true : false;
}

bool ACraftingManager::IsCraftingItem() {
    return GetWorldTimerManager().IsTimerActive(this, &ACraftingManager::FinishItemCrafting);
}

float ACraftingManager::GetItemCraftingPercent() {
    if (CraftingRecipe != nullptr && GetWorldTimerManager().IsTimerActive(this, &ACraftingManager::FinishItemCrafting)) {
        return GetWorldTimerManager().GetTimerElapsed(this, &ACraftingManager::FinishItemCrafting) / CraftingRecipe->GetTotalCraftingTime();
    }

    return 0;
}

void ACraftingManager::ResetVariables() {
    CraftingInProgress = false;
    ShowPreviewBuilding = false;
    CraftingRecipe = nullptr;
    CraftingItem = nullptr;
    NumItemCraftTimes = 1;
}

void ACraftingManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ACraftingManager, OwningCharacter);
}
