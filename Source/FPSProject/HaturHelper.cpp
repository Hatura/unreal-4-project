#include "FPSProject.h"

#include "HaturHelper.h"

void HaturHelper::QL(FString LogText) {
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), LogText);
    }
}

void HaturHelper::QL(FString LogText, FString StringAddition) {
    if (GEngine) {
        LogText.Append(": ");
        LogText.Append(StringAddition);
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), LogText);
    }
}

void HaturHelper::QL(FString LogText, int32 IntAddition) {
    if (GEngine) {
        LogText.Append(": ");
        LogText.Append(FString::FromInt(IntAddition));
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), LogText);
    }
}

void HaturHelper::QL(FString LogText, float FloatAddition) {
    if (GEngine) {
        LogText.Append(": ");
        LogText.Append(FString::SanitizeFloat(FloatAddition));
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), LogText);
    }
}

void HaturHelper::QL(FString LogText, bool BoolAddition) {
    if (GEngine) {
        BoolAddition ? LogText.Append(": True") : LogText.Append(": False");
        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), LogText);
    }
}
