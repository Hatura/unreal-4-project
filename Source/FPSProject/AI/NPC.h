#pragma once

#include "GameFramework/Character.h"
#include "NPC.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API ANPC : public ACharacter
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(EditAnywhere, Category = "NPC")
    class UBehaviorTree* NPCBehaviorTree;
};
