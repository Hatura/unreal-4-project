#pragma once

#include "AIController.h"
#include "NPCController.generated.h"

UENUM(BlueprintType)
namespace ENPCAnimationState {
    enum NPCAnimationState {
        IDLE,
        ATTACK,
    };
}

/**
 * 
 */
UCLASS()
class FPSPROJECT_API ANPCController : public AAIController
{
	GENERATED_UCLASS_BODY()

    UPROPERTY()
    TSubobjectPtr<class UBlackboardComponent> BlackboardComponent;

    UPROPERTY()
    TSubobjectPtr<class UBehaviorTreeComponent> BehaviorTreeComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "NPC Controller")
    TEnumAsByte<ENPCAnimationState::NPCAnimationState> AnimationState;

    virtual void Possess(class APawn* InPawn) override;

    // Init blackboard values
    virtual void NPC_PostPossess();

protected:
    class ANPC* GetNPC();
};
