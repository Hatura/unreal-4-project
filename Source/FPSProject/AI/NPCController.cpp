#include "FPSProject.h"
#include "FPSCharacter.h"
#include "NPC.h"
#include "NPCController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"


ANPCController::ANPCController(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    BlackboardComponent = PCIP.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackboardComponent"));
    BehaviorTreeComponent = PCIP.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorTreeComponent"));

    AnimationState = ENPCAnimationState::IDLE;
}

void ANPCController::Possess(class APawn* InPawn) {
    Super::Possess(InPawn);

    ANPC* NPC = Cast<ANPC>(InPawn);
    if (NPC && NPC->NPCBehaviorTree) {
        BlackboardComponent->InitializeBlackboard(NPC->NPCBehaviorTree->BlackboardAsset);

        NPC_PostPossess();

        BehaviorTreeComponent->StartTree(NPC->NPCBehaviorTree);
    }
}

void ANPCController::NPC_PostPossess() {
    // default implementation
}

ANPC* ANPCController::GetNPC() {
    APawn* MyPawn = GetPawn();

    if (MyPawn == nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("ANPCController::GetNPC(): No pawn, pawn dead?"));
        return nullptr;
    }

    ANPC* MyNPC = Cast<ANPC>(MyPawn);

    if (!MyNPC) {
        UE_LOG(LogTemp, Error, TEXT("ANPCController::GetNPC(): Coulnd't cast APawn to ANPC"));
        return nullptr;
    }

    return MyNPC;
}
