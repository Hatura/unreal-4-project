

#pragma once

#include "AI/NPCController_Moving.h"
#include "NPCController_MeleeAttacker.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API ANPCController_MeleeAttacker : public ANPCController_Moving
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Melee Attacking NPC Controller")
    float MeleeAttackRange;

    // Should be a factor of 0.2, because the AI BP is only checking every 0.2s
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Melee Attacking NPC Controller")
    float AttackInterval;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Melee Attacking NPC Controller")
    bool DoesPreAttackJump;

    // Min distance to perform pre attack jump
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Melee Attacking NPC Controller")
    float PreAttackJumpDistance;

    UFUNCTION(BlueprintCallable, Category = "Melee Attacking NPC Controller")
    virtual void Attack();

    UFUNCTION(BlueprintCallable, Category = "Melee Attacking NPC Controller")
    virtual void PreAttackJump();

    bool Attacking;

protected:
    // Tell the Controller it can attack again..
    virtual void ResetAttack();
};
