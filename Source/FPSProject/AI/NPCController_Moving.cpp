#include "FPSProject.h"
#include "NPCController_Moving.h"


ANPCController_Moving::ANPCController_Moving(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}

void ANPCController_Moving::NPC_PostPossess() {
    TargetKeyID = BlackboardComponent->GetKeyID("Target");
    TargetLocationID = BlackboardComponent->GetKeyID("Destination");
}

void ANPCController_Moving::SetTarget(APawn* InPawn) {
    if (!InPawn) {
        UE_LOG(LogTemp, Error, TEXT("ANPCController_Moving::SetEnemy(): InPawn = nullptr"));
        return;
    }

    BlackboardComponent->SetValueAsObject(TargetKeyID, InPawn);
    BlackboardComponent->SetValueAsVector(TargetLocationID, InPawn->GetActorLocation());
}

void ANPCController_Moving::ClearTarget() {
    BlackboardComponent->SetValueAsObject(TargetKeyID, NULL);
    BlackboardComponent->SetValueAsVector(TargetLocationID, FVector::ZeroVector);
}

void ANPCController_Moving::SearchForTarget() {
    UWorld* MyWorld = GetWorld();

    if (!MyWorld) {
        UE_LOG(LogTemp, Error, TEXT("ANPCController_Moving::SearchForTarget(): No World"));
        return;
    }

    APawn* MyNPC = GetPawn();
    if (!MyNPC) {
        UE_LOG(LogTemp, Error, TEXT("ANPCController_Moving::SearchForTarget(): GetPawn() = nullptr"));
        return;
    }

    const FVector NPCLocation = MyNPC->GetActorLocation();
    float BestDistance = MAX_FLT;
    AFPSCharacter* BestPawn = nullptr;

    for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It) {
        AFPSCharacter* TestPawn = Cast<AFPSCharacter>(*It);

        if (TestPawn) {
            const float Distance = FVector::Dist(TestPawn->GetActorLocation(), NPCLocation);
            if (Distance < SeeRange) {
                BestDistance = Distance;
                BestPawn = TestPawn;
            }
        }
    }

    if (BestPawn) {
        SetTarget(BestPawn);
    }
    else {
        ClearTarget();
    }
}

AActor *ANPCController_Moving::GetTarget() {
    if (BlackboardComponent) {
        return Cast<AActor>(BlackboardComponent->GetValueAsObject(TargetKeyID));
    }

    return nullptr;
}
