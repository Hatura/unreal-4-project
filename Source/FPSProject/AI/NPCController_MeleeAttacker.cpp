#include "FPSProject.h"
#include "NPC.h"
#include "NPCController_MeleeAttacker.h"


ANPCController_MeleeAttacker::ANPCController_MeleeAttacker(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    Attacking = false;
}

void ANPCController_MeleeAttacker::Attack() {
    if (Role != ROLE_Authority) {
        return;
    }

    if (Attacking) {
        return;
    }

    AActor* Target = GetTarget();

    if (Target == nullptr) {
        return;
    }

    ANPC* MyNPC = GetNPC();

    if (!MyNPC) {
        return;
    }

    const FVector NPCLoc = MyNPC->GetActorLocation();
    const FVector TargetLoc = Target->GetActorLocation();

    float Distance = FVector::Dist(NPCLoc, TargetLoc);

    if (Distance > MeleeAttackRange) {
        return;
    }

    if (!LineOfSightTo(Target, MyNPC->GetActorLocation())) {
        UE_LOG(LogTemp, Warning, TEXT("No LOS"));
        return;
    }

    HaturHelper::QL("Attack!");

    AnimationState = ENPCAnimationState::ATTACK;

    // Don't call this if someone forget to set AttackInterval, just let it false
    if (AttackInterval > 0) {
        Attacking = true;
        GetWorldTimerManager().SetTimer(this, &ANPCController_MeleeAttacker::ResetAttack, AttackInterval, false);
    }
}

void ANPCController_MeleeAttacker::PreAttackJump() {
    if (Role != ROLE_Authority) {
        return;
    }

    if (!DoesPreAttackJump || PreAttackJumpDistance <= 0) {
        return;
    }

    ANPC* MyNPC = GetNPC();

    if (MyNPC == nullptr) {
        return;
    }

    AActor* Target = GetTarget();

    if (Target == nullptr) {
        return;
    }

    float Distance = FVector::Dist(MyNPC->GetActorLocation(), Target->GetActorLocation());

    if (Distance <= PreAttackJumpDistance) {
        MyNPC->Jump();
    }
}

void ANPCController_MeleeAttacker::ResetAttack() {
    Attacking = false;
}
