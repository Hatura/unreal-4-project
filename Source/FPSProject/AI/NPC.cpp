#include "FPSProject.h"
#include "NPCController.h"
#include "NPC.h"


ANPC::ANPC(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    AIControllerClass = ANPCController::StaticClass();

    bReplicates         = true;
    bReplicateMovement  = true;
}
