#pragma once

#include "AI/NPCController.h"
#include "NPCController_Moving.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API ANPCController_Moving : public ANPCController
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Moving NPC Controller")
    float SeeRange;

    // Is friendly to players?
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Moving NPC Controller")
    bool Friendly;

    // Used to store enemies if friendly (people that attacked this Entity)
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Moving NPC Controller")
    TArray<class APawn*> Hatelist;

    virtual void NPC_PostPossess() override;

    virtual void SetTarget(class APawn* InPawn);

    virtual void ClearTarget();

    UFUNCTION(BlueprintCallable, Category = "Moving NPC Controller")
    void SearchForTarget();

    UFUNCTION(BlueprintCallable, Category = "Moving NPC Controller")
    class AActor* GetTarget();

protected:
    uint8 TargetKeyID;
    uint8 TargetLocationID;
};
