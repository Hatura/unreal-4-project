#pragma once

#include "Object.h"
#include "MM_ServerListEntry.generated.h"

/**
 *
 */
UCLASS()
class UMM_ServerListEntry : public UObject
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintReadOnly, Category = "Server List Entry")
    int32 SearchResultsIndex;

    UPROPERTY(BlueprintReadOnly, Category = "Server List Entry")
    FString ServerName;

    UPROPERTY(BlueprintReadOnly, Category = "Server List Entry")
    FString ServerIP;

    UPROPERTY(BlueprintReadOnly, Category = "Server List Entry")
    int32 CurrentPlayers;

    UPROPERTY(BlueprintReadOnly, Category = "Server List Entry")
    int32 MaxPlayers;

    UPROPERTY(BlueprintReadOnly, Category = "Server List Entry")
    int32 Ping; 
};
