#include "FPSProject.h"
#include "Online/CommManager.h"
#include "MM_PlayerController.h"

/**
 * @brief AMM_PlayerController::AMM_PlayerController
 * This is used as a menu because it already gives the base functionality and isn't used for anything else!
 */

AMM_PlayerController::AMM_PlayerController(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    bShowMouseCursor = true;
}

void AMM_PlayerController::BeginPlay() {
    //HaturHelper::QL(M_GetPlayerName());

//    CommManager = NewObject<UCommManager>();
}

void AMM_PlayerController::M_StartGame_Singleplayer() {
    HaturHelper::QL("StartGame_Singleplayer()");
    //GEngine->LoadMap(GetWorld, )
}

void AMM_PlayerController::M_StartGame_Online_Host() {
    HaturHelper::QL("StartGame_Online_Host()");
}

void AMM_PlayerController::M_StartGame_Online_Client() {
    HaturHelper::QL("StartGame_Online_Client()");
}

void AMM_PlayerController::M_ExitGame() {
    HaturHelper::QL("ExitGame");
    UWorld* World = GetWorld();
}

TArray<FString> AMM_PlayerController::M_GetOnlineServerList(int32 StartIndex) {
    TArray<FString> ServerListArray;
    ServerListArray.Add("Testentry");

    const auto *MyOnlineSubsystem = IOnlineSubsystem::Get();


    return ServerListArray;
}


FString AMM_PlayerController::M_GetPlayerName() {
    FString InfoString = "[NO NAME]";

    const auto *MyOnlineSubsystem = IOnlineSubsystem::Get();
    if (MyOnlineSubsystem) {
        TSharedPtr<FUniqueNetId> MyNetID = MyOnlineSubsystem->GetIdentityInterface()->GetUniquePlayerId(0);
        if (MyNetID.IsValid()) {
            HaturHelper::QL(MyNetID->GetHexEncodedString());

            TSharedPtr<FUserOnlineAccount> MyAccount = MyOnlineSubsystem->GetIdentityInterface()->GetUserAccount(*MyNetID.Get());
            if (MyAccount.IsValid()) {
                InfoString = MyAccount->GetDisplayName();
            }
            else {
                UE_LOG(LogTemp, Log, TEXT("Invalid FUserOnlineAccount"));
            }
        }
        else {
            UE_LOG(LogTemp, Log, TEXT("Invalid FUniqueNetID"));
        }
    }
    else {
        UE_LOG(LogTemp, Log, TEXT("No online subsystem"));
    }

    return InfoString;
}

void AMM_PlayerController::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);

}
