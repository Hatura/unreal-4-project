#pragma once

#include "GameFramework/GameMode.h"
#include "MM_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AMM_GameMode : public AGameMode
{
	GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintReadWrite, Category = "MM_GameMode")
    bool ServerListNeedsUpdate;

    // Controller ID that initiated session join
    int32 JoiningControllerId;

    UPROPERTY(BlueprintReadOnly, Category = "MM_GameMode")
    TArray<class UMM_ServerListEntry*> ServerListEntries;

    UPROPERTY(BlueprintReadOnly, Category = "MM_GameMode")
    FString TravelURL;

    UFUNCTION(BlueprintCallable, Category = "MM_GameMode")
    bool HostGame(class APlayerController* PCOwner, const FString &GameType, const FString& InTravelURL);

    UFUNCTION(BlueprintCallable, Category = "MM_GameMode")
    bool FindSessions(class APlayerController* PCOwner, bool LanMatch);

    UFUNCTION(BlueprintCallable, Category = "MM_GameMode")
    bool JoinSession(class APlayerController* PCOwner, int32 SessionIndexInSearchResults);

    UFUNCTION(BlueprintCallable, Category = "MM_GameMode")
    void ClearServerList();

    // Callbacks for session creation
    void OnCreatePresenceSessionComplete(FName SessionName, bool WasSuccessful);
    void OnCreateGameSessionComplete(FName SessionName, bool WasSuccessful);

    // Callback for finding sessions
    void OnSearchSessionsComplete(bool WasSuccessful);

    // Callback for joining a session
    void OnJoinSessionComplete(bool WasSuccessful);

    // Handlers for failing network/travel failures (used in JoinSession)
    void AddFailureHandlers();

    UFUNCTION(BlueprintCallable, Category = "MM_GameMode")
    virtual TSubclassOf<class AGameSession> GetGameSessionClass() const override;

    UFUNCTION(BlueprintCallable, Category = "MM_GameMode")
    virtual class AMM_GameSession* GetGameSession();

protected:
    void JoinLocalSessionFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString);

    void TravelLocalSessionFailure(UWorld* World, ETravelFailure::Type FailureType, const FString& ErrorString);
};
