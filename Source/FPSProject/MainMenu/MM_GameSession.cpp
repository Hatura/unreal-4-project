#include "FPSProject.h"
#include "Online/OnlineGameSettings.h"
#include "FPSPlayerController.h"
#include "MM_GameSession.h"

AMM_GameSession::AMM_GameSession(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    OnCreateSessionCompleteDelegate = FOnCreateSessionCompleteDelegate::CreateUObject(this, &AMM_GameSession::OnCreateSessionComplete);
    OnStartSessionCompleteDelegate = FOnStartSessionCompleteDelegate::CreateUObject(this, &AMM_GameSession::OnStartOnlineGameComplete);
    OnDestroySessionCompleteDelegate = FOnDestroySessionCompleteDelegate::CreateUObject(this, &AMM_GameSession::OnDestroySessionComplete);

    OnFindSessionsCompleteDelegate = FOnFindSessionsCompleteDelegate::CreateUObject(this, &AMM_GameSession::OnFindSessionsComplete);

    OnJoinSessionCompleteDelegate = FOnJoinSessionCompleteDelegate::CreateUObject(this, &AMM_GameSession::OnJoinSessionComplete);
}

bool AMM_GameSession::HasSteam(bool Log) {
    const auto *MyOnlineSubsystem = IOnlineSubsystem::Get();

    if (MyOnlineSubsystem) {
        FString Playername = MyOnlineSubsystem->GetIdentityInterface()->GetPlayerNickname(0);
        bool HasSteam = MyOnlineSubsystem->DoesInstanceExist("Steam");

        TEnumAsByte<ELoginStatus::Type> MyLoginStatus = MyOnlineSubsystem->GetIdentityInterface()->GetLoginStatus(0);

        if (HasSteam && MyLoginStatus == ELoginStatus::LoggedIn) {
            FString InfoString = "[STEAM]: Logged in as: ";

            InfoString.Append(Playername);

            if (Log) { HaturHelper::QL(InfoString); }
            return true;
        }
        else if (HasSteam && MyLoginStatus == ELoginStatus::NotLoggedIn) {
            if (Log) { HaturHelper::QL("[STEAM]: Not logged in"); }
            return true;
        }
        else if (HasSteam && MyLoginStatus == ELoginStatus::UsingLocalProfile) {
            FString InfoString = "[STEAM]: Using local profile for: ";

            InfoString.Append(Playername);

            if (Log) { HaturHelper::QL(InfoString); }
            return true;
        }
        else {
            if (Log) { HaturHelper::QL("Has no Steam"); }
            return false;
        }
    }

    return false;
}

bool AMM_GameSession::HostSession(int32 ControllerID, FName MySessionName, const FString& GameType, bool IsLan, bool IsPresence, int32 MaxNumPlayers) {
    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();
    if (MyOnlineSubsystem) {
        CurrentSessionParams.SessionName = MySessionName;
        CurrentSessionParams.IsLan = IsLan;
        CurrentSessionParams.IsPresence = IsPresence;
        CurrentSessionParams.ControllerID = ControllerID;
        MaxPlayers = MaxNumPlayers;

        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();
        if (MySessions.IsValid()) {
            HostSettings = MakeShareable(new FGameOnlineSessionSettings(IsLan, IsPresence, MaxPlayers));
            HostSettings->Set(SETTING_GAMEMODE, GameType, EOnlineDataAdvertisementType::ViaOnlineService);

            MySessions->AddOnCreateSessionCompleteDelegate(OnCreateSessionCompleteDelegate);
            MySessions->CreateSession(CurrentSessionParams.ControllerID, CurrentSessionParams.SessionName, *HostSettings);
            return true;
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("GameSession::HostSessions(): No Sessioninterface"));
        }
    }

#if !UE_BUILD_SHIPPING
    else {
        // Hack workflow in development ? ! ? !
        OnCreatePresenceSessionComplete().Broadcast(GameSessionName, true);
        return true;
    }
#endif

    return false;
}

void AMM_GameSession::CreateGameSession(int32 ControllerID) {
    // Creates a game session

    const FString GameType(TEXT("Type"));
    HostSession(ControllerID, GameSessionName, GameType, false, true, 4);
}

void AMM_GameSession::HandleMatchHasStarted() {
    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();

    if (MyOnlineSubsystem) {
        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();

        if (MySessions.IsValid()) {
            UE_LOG(LogTemp, Log, TEXT("Starting session %s on server"), *GameSessionName.ToString());

            MySessions->AddOnStartSessionCompleteDelegate(OnStartSessionCompleteDelegate);
            MySessions->StartSession(GameSessionName);
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("GameSession::HandleMatchHasStarted(): No Sessioninterface"));
        }
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("GameSession::HandleMatchHasStarted(): No online subsystem"));
    }
}

void AMM_GameSession::HandleMatchHasEnded() {
    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();

    if (MyOnlineSubsystem) {
        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();

        if (MySessions.IsValid()) {
            // Tell the clients to end
//            for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It) {
//                AFPSPlayerController* PC = Cast<AFPSPlayerController>(*It);
//                if (PC && !PC->IsLocalPlayerController()) {
//                    PC->ClientEndOnlineGame();
//                }
//            }

            // Server is handled here
            UE_LOG(LogTemp, Log, TEXT("Ending session %s on server"), *GameSessionName.ToString());
            MySessions->EndSession(GameSessionName);
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("GameSession::HandleMatchHasEnded(): No Sessioninterface"));
        }
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("GameSession::HandleMatchHasEnded(): No online subsystem"));
    }
}

void AMM_GameSession::DestroyGameSession(int32 ControllerID) {
    // TODO: Why is this empty?
}

void AMM_GameSession::OnCreateSessionComplete(FName SessionName, bool WasSuccessful) {
    UE_LOG(LogTemp, Log, TEXT("OnCreateSessionComplete, Session name: %s bSuccess: %d"), *SessionName.ToString(), WasSuccessful);

    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();
    if (MyOnlineSubsystem) {
        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();
        MySessions->ClearOnCreateSessionCompleteDelegate(OnCreateSessionCompleteDelegate);
    }

    OnCreatePresenceSessionComplete().Broadcast(SessionName, WasSuccessful);

    if (!WasSuccessful) {
        DelayedSessionDelete();
    }
}

//void AMM_GameSession::OnCreatePresenceSessionComplete(FName SessionName, bool WasSuccessful) {

//}

void AMM_GameSession::OnStartOnlineGameComplete(FName SessionName, bool WasSuccessful) {
    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();

    if (MyOnlineSubsystem) {
        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();

        if (MySessions.IsValid()) {
            MySessions->ClearOnStartSessionCompleteDelegate(OnStartSessionCompleteDelegate);
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("GameSession::StartOnlineGameComplete(): No Sessioninterface"));
        }
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("GameSession::StartOnlineGAmeComplete(): No online subsystem"));
    }

    if (WasSuccessful) {
        // Tell non-local players to start online game
//        for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It) {
//            AFPSPlayerController* PC = Cast<AFPSPlayerController>(*It);
//            if (PC && !PC->IsLocalPlayerController()) {
//                PC->ClientStartOnlineGame();
//            }
//        }
    }
}

void AMM_GameSession::OnDestroySessionComplete(FName SessionName, bool WasSuccessful) {
    UE_LOG(LogTemp, Log, TEXT("OnDestroySessionComplete %s bSuccess: %d"), *SessionName.ToString(), WasSuccessful);

    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();
    if (MyOnlineSubsystem) {
        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();
        MySessions->ClearOnDestroySessionCompleteDelegate(OnDestroySessionCompleteDelegate);
        HostSettings = NULL;
    }
}

void AMM_GameSession::DelayedSessionDelete() {
    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();

    if (MyOnlineSubsystem) {
        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();
        EOnlineSessionState::Type SessionState = MySessions->GetSessionState(CurrentSessionParams.SessionName);
        if (SessionState != EOnlineSessionState::Creating) {
            MySessions->AddOnDestroySessionCompleteDelegate(OnDestroySessionCompleteDelegate);
            MySessions->DestroySession(CurrentSessionParams.SessionName);
        }
        else {
            // Retry shortly
            GetWorldTimerManager().SetTimer(this, &AMM_GameSession::DelayedSessionDelete, 1.0f);
        }
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("GameSession::DelayedSessionDelete(): No online subsystem"));
    }
}

void AMM_GameSession::FindSessions(int32 ControllerID, FName MySessionName, bool IsLan, bool IsPresence) {
    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();
    if (MyOnlineSubsystem) {
        CurrentSessionParams.SessionName = MySessionName;
        CurrentSessionParams.IsLan = IsLan;
        CurrentSessionParams.IsPresence = IsPresence;
        CurrentSessionParams.ControllerID = ControllerID;

        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();
        if (MySessions.IsValid()) {
            SearchSettings = MakeShareable(new FGameOnlineSearchSettings(IsLan, IsPresence));
            TSharedRef<FOnlineSessionSearch> SearchSettingsRef = SearchSettings.ToSharedRef();

            MySessions->AddOnFindSessionsCompleteDelegate(OnFindSessionsCompleteDelegate);
            MySessions->FindSessions(ControllerID, SearchSettingsRef);
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("AMM_GameSession::FindSessions(): No Sessioninterface"));
        }
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("AMM_GameSession::FindSessions(): No online subsystem"));
    }
}

void AMM_GameSession::OnFindSessionsComplete(bool WasSuccessful) {
    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();
    if (MyOnlineSubsystem) {
        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();
        if (MySessions.IsValid()) {
            MySessions->ClearOnFindSessionsCompleteDelegate(OnFindSessionsCompleteDelegate);

            HaturHelper::QL("Number of search results: ", SearchSettings->SearchResults.Num());
            for (int32 i = 0; i < SearchSettings->SearchResults.Num(); i++) {
                const FOnlineSessionSearchResult& SearchResult = SearchSettings->SearchResults[i];
                DumpSession(&SearchResult.Session);
            }

            OnFindSessionsComplete().Broadcast(WasSuccessful);
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("AMM_GameSession::OnFindSessionsComplete(): No Sessioninterface"));
        }
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("AMM_GameSession::OnFindSessionsComplete(): No online subsystem"));
    }
}

bool AMM_GameSession::JoinSession(int32 ControllerID, FName MySessionName, int32 SessionIndexInSearchResults) {
    bool JoinResult = false;

    if (SessionIndexInSearchResults >= 0 && SessionIndexInSearchResults < SearchSettings->SearchResults.Num()) {
        IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();
        if (MyOnlineSubsystem) {
            IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();
            if (MySessions.IsValid()) {
                MySessions->AddOnJoinSessionCompleteDelegate(OnJoinSessionCompleteDelegate);
                JoinResult = MySessions->JoinSession(ControllerID, SessionName, SearchSettings->SearchResults[SessionIndexInSearchResults]);
            }
            else {
                UE_LOG(LogTemp, Error, TEXT("AMM_GameSession::JoinSession(): No Sessioninterface"));
            }
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("AMM_GameSession::JoinSession(): No online subsystem"));
        }
    }

    return JoinResult;
}

void AMM_GameSession::OnJoinSessionComplete(FName SessionName, bool WasSuccessful) {
    bool WillTravel = false;

    UE_LOG(LogTemp, Log, TEXT("OnJoinSessionComplete %s Success: %d"), *SessionName.ToString(), WasSuccessful);

    IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();

    if (MyOnlineSubsystem) {
        IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();
        if (MySessions.IsValid()) {
            MySessions->ClearOnJoinSessionCompleteDelegate(OnJoinSessionCompleteDelegate);
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("AMM_GameSession::OnJoinSessionComplete(): No Sessioninterface"));
        }
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("AMM_GameSession::OnJoinSessionComplete(): No online subsystem"));
    }

    OnJoinSessionComplete().Broadcast(WasSuccessful);
}

const TArray<FOnlineSessionSearchResult>& AMM_GameSession::GetSearchResults() const {
    return SearchSettings->SearchResults;
}
