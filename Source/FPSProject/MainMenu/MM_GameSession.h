#pragma once

#include "GameFramework/GameSession.h"
#include "Online.h"
#include "MM_GameSession.generated.h"

struct FNetworkGameSessionParams {
    FName SessionName;
    bool IsLan;
    bool IsPresence;
    int32 ControllerID;
    int32 BestSessionIndex;

    FNetworkGameSessionParams()
        : SessionName(NAME_None),
          IsLan(false),
          IsPresence(false),
          ControllerID(0),
          BestSessionIndex(0)
    { }
};

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AMM_GameSession : public AGameSession
{
	GENERATED_UCLASS_BODY()

public:
    UFUNCTION(BlueprintCallable, Category = "MM_GameSession")
    bool HasSteam(bool Log);

    bool HostSession(int32 ControllerID, FName MySessionName, const FString &GameType, bool IsLan, bool IsPresence, int32 MaxNumPlayers);

    void FindSessions(int32 ControllerID, FName MySessionName, bool IsLan, bool IsPresence);

    bool JoinSession(int32 ControllerID, FName MySessionName, int32 SessionIndexInSearchResults);

protected:
    FNetworkGameSessionParams CurrentSessionParams;

    TSharedPtr<class FOnlineSessionSettings> HostSettings;

    TSharedPtr<class FOnlineSessionSearch> SearchSettings;

    // Delegate for creating a new session
    FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;

    // Delegate after startign a session
    FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;

    // Delegate for destroying a session
    FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;

    // Delegate for searching for sessions
    FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;

    FOnJoinSessionCompleteDelegate OnJoinSessionCompleteDelegate;

    // Delegate fired when a session search query has completed
    void OnFindSessionsComplete(bool WasSuccessful);

    // Delegate fired when a session create request has completed
    virtual void OnCreateSessionComplete(FName SessionName, bool WasSuccessful);

//    // ???
//    virtual void OnCreatePresenceSessionComplete(FName SessionName, bool WasSuccessful);

    // Delegate fired when a session start request has completed
    void OnStartOnlineGameComplete(FName SessionName, bool WasSuccessful);

    // Delegate fired when destroying an online session has completed
    virtual void OnDestroySessionComplete(FName SessionName, bool WasSuccessful);

    // Delegate fired when a session join request has completed
    void OnJoinSessionComplete(FName SessionName, bool WasSuccessful);

    // Safe delete mechanism to make sure we aren't deleting a session too soon after its creation
    void DelayedSessionDelete();

    // Event triggered when a presence session is created
    DECLARE_EVENT_TwoParams(AMM_GameSession, FOnCreatePresenceSessionComplete, FName /*SessionName*/, bool /*WasSuccesful*/)
    FOnCreatePresenceSessionComplete CreatePresenceSessionCompleteEvent;

    // Event triggered after session search completes
    DECLARE_EVENT_OneParam(AMM_GameSession, FOnFindSessionsComplete, bool /*WasSuccessful*/);
    FOnFindSessionsComplete FindSessionsCompleteEvent;

    DECLARE_EVENT_OneParam(AMM_GameSession, FOnJoinSessionComplete, bool /*WasSuccessful*/);
    FOnJoinSessionComplete JoinSessionCompleteEvent;

public:
    // @ return the search results for finding sessions
    const TArray<FOnlineSessionSearchResult>& GetSearchResults() const;

    // @return the delegate fired when creating a presence session
    FOnCreatePresenceSessionComplete& OnCreatePresenceSessionComplete() { return CreatePresenceSessionCompleteEvent; }

    // @return the delegate fired when search of session completes
    FOnFindSessionsComplete& OnFindSessionsComplete() { return FindSessionsCompleteEvent; }

    // @ return the delegate fired when joining a session
    FOnJoinSessionComplete& OnJoinSessionComplete() { return JoinSessionCompleteEvent; }

    // Creates a game session
    virtual void CreateGameSession(int32 ControllerID);

    // Handle starting the match
    virtual void HandleMatchHasStarted() override;

    // Handles when the match has ended
    virtual void HandleMatchHasEnded() override;

    // Destorys a game session
    virtual void DestroyGameSession(int32 ControllerID);
};
