#pragma once

#include "GameFramework/PlayerController.h"
#include "MM_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AMM_PlayerController : public APlayerController
{
	GENERATED_UCLASS_BODY()

//    UPROPERTY(BlueprintReadOnly, Category = "Communication")
//    class UCommManager* CommManager;

    void BeginPlay();

    void Tick(float DeltaTime);

    UFUNCTION(BlueprintCallable, Category = "Menu")
    void M_StartGame_Singleplayer();

    UFUNCTION(BlueprintCallable, Category = "Menu")
    void M_StartGame_Online_Host();

    UFUNCTION(BlueprintCallable, Category = "Menu")
    void M_StartGame_Online_Client();
	
    UFUNCTION(BlueprintCallable, Category = "Menu")
    void M_ExitGame();

    UFUNCTION(BlueprintCallable, Category = "Menu")
    TArray<FString> M_GetOnlineServerList(int32 StartIndex);

    FString M_GetPlayerName();

};
