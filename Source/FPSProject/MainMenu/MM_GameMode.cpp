#include "FPSProject.h"
#include "Runtime/Online/OnlineSubsystem/Public/Online.h"
#include "Runtime/Online/OnlineSubsystem/Public/OnlineSessionSettings.h"
#include "MM_GameSession.h"
#include "MM_ServerListEntry.h"
#include "MM_GameMode.h"

AMM_GameMode::AMM_GameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    // This is overriden by blueprint, TODO: Do we even need this class in C++ then?
    ServerListNeedsUpdate = false;
}

bool AMM_GameMode::HostGame(APlayerController *PCOwner, const FString& GameType, const FString &InTravelURL) {
    bool HostGameResult = false;

    if (PCOwner) {
        AMM_GameSession* MenuSession = Cast<AMM_GameSession>(GameSession);
        if (MenuSession) {
            ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(PCOwner->Player);
            if (LocalPlayer) {
                TravelURL = InTravelURL;
                bool IsLanMatch = TravelURL.Contains(TEXT("?bIsLanMatch"));
                MenuSession->OnCreatePresenceSessionComplete().AddUObject(this, &AMM_GameMode::OnCreatePresenceSessionComplete);
                if (MenuSession->HostSession(LocalPlayer->ControllerId, GameSessionName, GameType, IsLanMatch, true, 4)) {
                    HostGameResult = true;
                }
            }
            else {
                UE_LOG(LogTemp, Warning, TEXT("GameMode::HostGame(): No local player"));
            }
        }
        else {
            UE_LOG(LogTemp, Warning, TEXT("GameMode::HostGame(): No Menu session"));
        }
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("GameMode::HostGame(): No Playercontroller"));
    }

    return HostGameResult;
}

void AMM_GameMode::OnCreateGameSessionComplete(FName SessionName, bool WasSuccessful) {
    AMM_GameSession* MenuSession = Cast<AMM_GameSession>(GameSession);
    if (MenuSession) {
        MenuSession->OnCreatePresenceSessionComplete().RemoveUObject(this, &AMM_GameMode::OnCreatePresenceSessionComplete);
    }
}

void AMM_GameMode::OnCreatePresenceSessionComplete(FName SessionName, bool WasSuccessful) {
    AMM_GameSession* MenuSession = Cast<AMM_GameSession>(GameSession);
    if (MenuSession) {
        MenuSession->OnCreatePresenceSessionComplete().RemoveUObject(this, &AMM_GameMode::OnCreatePresenceSessionComplete);
        if (WasSuccessful) {
            GetWorld()->ServerTravel(TravelURL);
        }
        else {
            HaturHelper::QL("Failed to create a presence session");
        }
    }
}

bool AMM_GameMode::FindSessions(APlayerController *PCOwner, bool LanMatch) {
    bool FindSessionsResult = false;

    if (PCOwner) {
        AMM_GameSession* MenuSession = Cast<AMM_GameSession>(GameSession);
        if (MenuSession) {
            ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(PCOwner->Player);
            if (LocalPlayer) {
                MenuSession->OnFindSessionsComplete().RemoveAll(this);

                MenuSession->OnFindSessionsComplete().AddUObject(this, &AMM_GameMode::OnSearchSessionsComplete);
                MenuSession->FindSessions(LocalPlayer->ControllerId, GameSessionName, LanMatch, true);

                HaturHelper::QL(GameSessionName.ToString());

                FindSessionsResult = true;
            }
            else {
                UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::FindSessions(): No local player"));
            }
        }
        else {
            UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::FindSessions(): No Menu session"));
        }
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::FindSessions(): No Playercontroller"));
    }

    return FindSessionsResult;
}

void AMM_GameMode::ClearServerList() {
    ServerListEntries.Empty();
}

void AMM_GameMode::OnSearchSessionsComplete(bool WasSuccessful) {
    HaturHelper::QL("Session finding completerino");

//    UMM_ServerListEntry* Entry = NewObject<UMM_ServerListEntry>();
//    Entry->ServerIP = "127.0.0.1";
//    Entry->ServerName = "Testserver";
//    Entry->CurrentPlayers = 0;
//    Entry->MaxPlayers = 12;

//    ServerListEntries.Add(Entry);

//    UMM_ServerListEntry* Entry2 = NewObject<UMM_ServerListEntry>();
//    Entry2->ServerIP = "127.0.0.1";
//    Entry2->ServerName = "Testserver";
//    Entry2->CurrentPlayers = 0;
//    Entry2->MaxPlayers = 12;

//    ServerListEntries.Add(Entry2);

    if (WasSuccessful) {
        HaturHelper::QL("Found sessions..");

        ServerListEntries.Empty();

        AMM_GameSession* MenuSession = Cast<AMM_GameSession>(GameSession);
        if (MenuSession) {
            const TArray<FOnlineSessionSearchResult> &SearchResults = MenuSession->GetSearchResults();
            HaturHelper::QL("Confirmed number of search results: ", SearchResults.Num());

            for (int32 i = 0; i < SearchResults.Num(); i++) {
                const FOnlineSessionSearchResult& Result = SearchResults[i];

                UMM_ServerListEntry* Entry = NewObject<UMM_ServerListEntry>();
                Entry->SearchResultsIndex = i;
                Entry->ServerIP = Result.Session.SessionInfo->ToString();
                Entry->ServerName = Result.Session.OwningUserName;
                Entry->CurrentPlayers = Result.Session.SessionSettings.NumPublicConnections - Result.Session.NumOpenPublicConnections;
                Entry->MaxPlayers = Result.Session.SessionSettings.NumPublicConnections;
                Entry->Ping = Result.PingInMs;

                ServerListEntries.Add(Entry);
            }
        }
        else {
            UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::OnSearchSessionsComplete(): No Menu session"));
        }
    }

    ServerListNeedsUpdate = true;
}

bool AMM_GameMode::JoinSession(APlayerController *PCOwner, int32 SessionIndexInSearchResults) {
    bool JoinResult = false;

    if (PCOwner) {
        AMM_GameSession* MenuSession = Cast<AMM_GameSession>(GameSession);

        if (MenuSession) {
            ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(PCOwner->Player);

            if (LocalPlayer) {
                JoiningControllerId = LocalPlayer->ControllerId;

                AddFailureHandlers();

                MenuSession->OnJoinSessionComplete().AddUObject(this, &AMM_GameMode::OnJoinSessionComplete);
                JoinResult = MenuSession->JoinSession(JoiningControllerId, GameSessionName, SessionIndexInSearchResults);

                JoinResult = true;
            }
            else {
                UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::JoinSession(): No local player"));
            }
        }
        else {
            UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::JoinSession(): No Menu session"));
        }
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::JoinSession(): No Playercontroller"));
    }

    return JoinResult;
}

void AMM_GameMode::OnJoinSessionComplete(bool WasSuccessful) {
    AMM_GameSession* MenuSession = Cast<AMM_GameSession>(GameSession);

    if (MenuSession) {
        MenuSession->OnJoinSessionComplete().RemoveUObject(this, &AMM_GameMode::OnJoinSessionComplete);
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::OnJoinSessionComplete(): No Menu session"));
    }

    if (WasSuccessful) {
        IOnlineSubsystem* MyOnlineSubsystem = IOnlineSubsystem::Get();

        if (MyOnlineSubsystem) {
            FString URL;

            IOnlineSessionPtr MySessions = MyOnlineSubsystem->GetSessionInterface();
            if (MySessions.IsValid() && MySessions->GetResolvedConnectString(GameSessionName, URL)) {
                APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), JoiningControllerId);
                if (PC) {
                    PC->ClientTravel(URL, TRAVEL_Absolute);
                }
                else {
                    UE_LOG(LogTemp, Error, TEXT("AMM_GameMode::OnJoinSessionComplete(): No such player controller"));
                }
            }
            else {
                UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::OnJoinSessionComplete(): No session subsystem or couldn't get connection string"));
            }
        }
        else {
            UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::OnJoinSessionComplete(): No online subsystem"));
        }
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("AMM_GameMode::OnJoinSessionComplete(): Joining the session was unsuccessful"));
    }
}

void AMM_GameMode::AddFailureHandlers() {
    if (GEngine->OnNetworkFailure().IsBoundToObject(this) == false) {
        GEngine->OnNetworkFailure().AddUObject(this, &AMM_GameMode::JoinLocalSessionFailure);
    }
    if (GEngine->OnNetworkFailure().IsBoundToObject(this) == false) {
        GEngine->OnTravelFailure().AddUObject(this, &AMM_GameMode::TravelLocalSessionFailure);
    }
}

void AMM_GameMode::JoinLocalSessionFailure(UWorld *World, UNetDriver *NetDriver, ENetworkFailure::Type FailureType, const FString &ErrorString) {
    UE_LOG(LogTemp, Error, TEXT("Network join failure: %s"), *ErrorString);
}

void AMM_GameMode::TravelLocalSessionFailure(UWorld *World, ETravelFailure::Type FailureType, const FString &ErrorString) {
    UE_LOG(LogTemp, Error, TEXT("Network travel failure: %s"), *ErrorString);
}

TSubclassOf<AGameSession> AMM_GameMode::GetGameSessionClass() const {
    return AMM_GameSession::StaticClass();
}

AMM_GameSession* AMM_GameMode::GetGameSession() {
    AMM_GameSession* MenuSession = Cast<AMM_GameSession>(GameSession);

    if (MenuSession) {
        return MenuSession;
    }

    return nullptr;
}
