#pragma once

#include "GameFramework/GameMode.h"
#include "Inventory/Inventory.h"
#include "Crafting/RecipeManager.h"
#include "GameFramework/Character.h"
#include "FPSGameMode.generated.h"

/**
 * 
 */
UCLASS()
class AFPSGameMode : public AGameMode
{                       
	GENERATED_UCLASS_BODY()

    void BeginPlay() override;

    // From a Blueprint derived Inventory-Class
    TSubclassOf<class AInventory> InventoryClass;

    TSubclassOf<class ARecipeManager> RecipeManagerClass;

public:
    virtual TSubclassOf<class AGameSession> GetGameSessionClass() const override;
};
