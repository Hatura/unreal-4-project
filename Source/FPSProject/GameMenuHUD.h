

#pragma once

#include "GameFramework/HUD.h"
#include "FPSCharacter.h"
#include "GameMenuHUD.generated.h"

/**
 * 
 */
UCLASS()
class AGameMenuHUD : public AHUD
{
    GENERATED_UCLASS_BODY()

    void BeginPlay();

    void DrawHUD() override;

public:
    UFUNCTION(BlueprintImplementableEvent, Category = "Menus|Main Menu")
    void PlayGameClicked();

    UFUNCTION(BlueprintImplementableEvent, Category = "Menus|Main Menu")
    void QuitGameClicked();

private:
    /** Crosshair asset Pointer */
	UTexture2D* CrosshairTex;

    /** Font asset pointer */
    UFont* DefaultFont;

    UPROPERTY()
    FText ServerOrClientText;

    // Reference to the Main Menu Slate UI
    TSharedPtr<class SMenuUI> MenuUI;
};
