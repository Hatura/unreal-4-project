#include "FPSProject.h"
#include "OnlineGameSettings.h"

FGameOnlineSessionSettings::FGameOnlineSessionSettings(bool IsLan, bool IsPresence, int32 MaxNumPlayers) {
    NumPublicConnections = MaxNumPlayers;
    if (NumPublicConnections < 0) {
        NumPublicConnections = 0;
    }
    NumPrivateConnections = 0;
    bIsLANMatch = IsLan;
    bShouldAdvertise = true;
    bAllowJoinInProgress = true;
    bAllowInvites = true;
    bUsesPresence = IsPresence;
    bAllowJoinViaPresence = true;
    bAllowJoinViaPresenceFriendsOnly = false;
}

FGameOnlineSearchSettings::FGameOnlineSearchSettings(bool SearchingLan, bool SearchingPresence) {
    bIsLanQuery = SearchingLan;
    MaxSearchResults = 10;
    PingBucketSize = 50;

    if (SearchingPresence) {
        QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
    }
}
