#pragma once

#include "Runtime/Online/OnlineSubsystem/Public/Interfaces/OnlineSessionInterface.h"

class FGameOnlineSessionSettings : public FOnlineSessionSettings
{
public:
    FGameOnlineSessionSettings(bool IsLan = false, bool IsPresence = false, int32 MaxNumPlayers = 4);

    virtual ~FGameOnlineSessionSettings() { }
};

class FGameOnlineSearchSettings : public FOnlineSessionSearch
{
public:
    FGameOnlineSearchSettings(bool SearchingLan = false, bool SearchingPresence = false);

    virtual ~FGameOnlineSearchSettings() {}
};
