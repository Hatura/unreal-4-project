

#include "FPSProject.h"
#include "GameMenuHUD.h"
#include "Slate/SMenuUI.h"
#include "FPSCharacter.h"


AGameMenuHUD::AGameMenuHUD(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("Texture2D'/Game/Textures/crosshair.crosshair'"));
	CrosshairTex = CrosshairTexObj.Object;

    // Setup the default font
    static ConstructorHelpers::FObjectFinder<UFont> FontObj(TEXT("Font'/Game/Fonts/Linotype.Linotype'"));
    DefaultFont = FontObj.Object;
}

void AGameMenuHUD::BeginPlay() {
    if (GEngine && GEngine->GameViewport) {
        UGameViewportClient* Viewport = GEngine->GameViewport;

        SAssignNew(MenuUI, SMenuUI).GameMenuHUD(TWeakObjectPtr<AGameMenuHUD>(this));

        Viewport->AddViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(MenuUI.ToSharedRef()));
    }
}

void AGameMenuHUD::DrawHUD() {
	Super::DrawHUD();
	// Draw very simple crosshair
	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);
	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition((Center.X - (CrosshairTex->GetSurfaceWidth() * 0.5f)),
		(Center.Y - (CrosshairTex->GetSurfaceHeight() * 0.5f)));
	// draw the crosshair
	FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);

    //AFPSCharacter *MyPawn = Cast<AFPSCharacter>(GetOwningPawn());

    APlayerController *PlayerController = GetOwningPlayerController();
    if (PlayerController) {
        switch (PlayerController->Role) {
        case ROLE_None:
            ServerOrClientText = FText::FromString(TEXT("ROLE_NONE"));
            break;
        case ROLE_SimulatedProxy:
            ServerOrClientText = FText::FromString(TEXT("ROLE_SIMULATED_PROXY"));
            break;
        case ROLE_AutonomousProxy:
            ServerOrClientText = FText::FromString(TEXT("ROLE_AUTONOMOUS_PROXY"));
            break;
        case ROLE_Authority:
            ServerOrClientText = FText::FromString(TEXT("ROLE_AUTHORITY"));
            break;
        case ROLE_MAX:
            ServerOrClientText = FText::FromString(TEXT("ROLE_MAX"));
            break;
        }
    }
    else {
        if (GEngine) {
            GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Couldn't fetch PlayerController in HUD-Constructor"));
        }
    }

    Canvas->DrawText(DefaultFont, ServerOrClientText, Center.X, Center.Y, 0.5f, 0.5f);
}
