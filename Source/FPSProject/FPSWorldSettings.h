#pragma once

#include "GameFramework/WorldSettings.h"
#include "FPSWorldSettings.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AFPSWorldSettings : public AWorldSettings
{
	GENERATED_UCLASS_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot")
    FLinearColor RarityColor_Crap;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot")
    FLinearColor RarityColor_Common;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot")
    FLinearColor RarityColor_Uncommon;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot")
    FLinearColor RarityColor_Rare;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot")
    FLinearColor RarityColor_Epic;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot")
    FLinearColor RarityColor_Legendary;

    UFUNCTION(BlueprintCallable, Category = "Loot")
    FLinearColor &GetColorForRarity(TEnumAsByte<EItemRarity::ItemRarity> Rarity);
};
