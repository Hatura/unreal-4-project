#include "FPSProject.h"
#include "FPSPlayerController.h"
#include "FPSGameMode.h"
#include "UnrealNetwork.h"

AFPSPlayerController::AFPSPlayerController(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}

void AFPSPlayerController::PostInitializeComponents() {
    Super::PostInitializeComponents();

    if (Role == ROLE_Authority) {
        Name = "Server";
    }
    else {
        Name = "Knecht";
        Name.Append(" ");
        Name.Append(FString::FromInt(FMath::RandRange(1, 1000)));
    }

    if (Role == ROLE_Authority) {
        UWorld* MyWorld = GetWorld();
        if (MyWorld) {
            AFPSGameMode* MyGameMode = Cast<AFPSGameMode>(MyWorld->GetAuthGameMode());
            if (MyGameMode) {
                if (!MyRMInstance) {
                    MyRMInstance = MyWorld->SpawnActor<ARecipeManager>(MyGameMode->RecipeManagerClass);
                    MyRMInstance->InitializeRecipesForOwner(MyWorld);
                    MyRMInstance->SetOwner(this);

                    if (GEngine) {
                        GEngine->AddOnScreenDebugMessage(-1, 10, FColor::MakeRandomColor(), TEXT("Initialized recipe manager instance for PC"));
                    }
                }
                else {
                    UE_LOG(LogTemp, Error, TEXT("MyRMInstance was already assigned, when trying to initialize RecipeManager in PlayerController"));
                }
            }
            else {
                UE_LOG(LogTemp, Error, TEXT("GameMode was null when trying to initialize RecipeManager in PlayerController"));
            }
        }
        else {
            UE_LOG(LogTemp, Error, TEXT("World was null when trying to initialize RecipeManager in PlayerController"));
        }
    }
}

void AFPSPlayerController::SetupInputComponent() {
    Super::SetupInputComponent();
}

void AFPSPlayerController::BeginPlay() {

}

void AFPSPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AFPSPlayerController, MyRMInstance);
}
