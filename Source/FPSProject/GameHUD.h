#pragma once

#include "GameFramework/HUD.h"
#include "GameHUD.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AGameHUD : public AHUD
{
	GENERATED_UCLASS_BODY()

    // Getter + Setter for Inventory
    bool GetInventoryOpen();
    void SetInventoryOpen(bool Open);

    bool GetCharacterPanelOpen();
    void SetCharacterPanelOpen(bool Open);

    bool GetCraftingManagerOpen();
    void SetCraftingManagerOpen(bool Open);

    UFUNCTION(BlueprintCallable, Category = "GameHUD")
    bool IsAnySubwidgetOpen();

    TSharedPtr<class SGameUI> GameHUDWidget;
    TSharedPtr<class SWidget> WidgetContainer;

    void DrawHUD();

public:
    virtual void PostInitializeComponents() override;

private:
    UPROPERTY(Transient)
    bool IsSlateInitialized;
};
