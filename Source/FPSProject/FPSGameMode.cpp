

#include "FPSProject.h"
#include "FPSGameMode.h"
#include "FPSGameState.h"
#include "GameMenuHUD.h"
#include "GameHUD.h"
#include "MainMenu/MM_GameSession.h"
#include "FPSPlayerController.h"


AFPSGameMode::AFPSGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// set the default pawn class to our Blueprinted Character
	static ConstructorHelpers::FObjectFinder<UBlueprint> PlayerPawnObject(TEXT("Blueprint'/Game/Blueprints/BP_FPSCharacter.BP_FPSCharacter'"));

	if (PlayerPawnObject.Object != NULL) {
		DefaultPawnClass = (UClass*)PlayerPawnObject.Object->GeneratedClass;
    }

    static ConstructorHelpers::FObjectFinder<UClass> InventoryBlueprint(TEXT("Blueprint'/Game/Blueprints/Inventory/Inventory.Inventory_C'"));
    if (InventoryBlueprint.Object != NULL) {
        InventoryClass = InventoryBlueprint.Object;

        if (GEngine) {
            GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Yellow, TEXT("Successfully loaded inventory class"));
        }
    }

    static ConstructorHelpers::FObjectFinder<UClass> RecipeManagerBlueprin(TEXT("Blueprint'/Game/Blueprints/Recipes/RecipeManager.RecipeManager_C'"));
    if (RecipeManagerBlueprin.Object != NULL) {
        RecipeManagerClass = RecipeManagerBlueprin.Object;

        if (GEngine) {
            GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Yellow, TEXT("Successfully loaded recipe manager class"));
        }
    }

    GameStateClass = AFPSGameState::StaticClass();
    PlayerControllerClass = AFPSPlayerController::StaticClass();
    HUDClass = AGameHUD::StaticClass();
}

void AFPSGameMode::BeginPlay() {
	Super::BeginPlay();
}

TSubclassOf<AGameSession> AFPSGameMode::GetGameSessionClass() const {
    return AMM_GameSession::StaticClass();
}


