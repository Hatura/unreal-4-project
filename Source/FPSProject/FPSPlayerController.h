#pragma once

#include "GameFramework/PlayerController.h"
#include "Crafting/RecipeManager.h"
#include "FPSPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AFPSPlayerController : public APlayerController
{
    GENERATED_UCLASS_BODY()

    void PostInitializeComponents() override;
    void BeginPlay() override;

    // Recipe manager, bound to PlayerController
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = RecipeManagerInstance) // Transient removed
    ARecipeManager* MyRMInstance;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Player)
    FString Name;

protected:
    void SetupInputComponent() override;

private:

};
