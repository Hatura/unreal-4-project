#pragma once

#include "GameFramework/Actor.h"
#include "SimpleResourceGatherPoint.generated.h"

UENUM(BlueprintType)
namespace ESRGP_Type {
    enum SRGP_Type {
        PALMWOOD,
        STONE
    };
}

/**
 * This is a base class for instanced static mesh that act as foliage (because the foliage tool doesn't support removing instanced foliage)
 * Use this class to create unique resource gather points (Like trees, stones) and assign what they produce with the enum
 *
 * TODO: Bring that stuff from the blueprint override to C++, currently there is no information on how to keep track of the InstancedStaticMesh
 * number if done via C++, also the static mesh is not visible in PIE
 *
 * So for now: DO NOT PARENT THIS, PARENT THE BLUEPRINTOVERRIDE!
 */
UCLASS()
class FPSPROJECT_API ASimpleResourceGatherPoint : public AActor
{
	GENERATED_UCLASS_BODY()

    virtual void PostInitializeComponents() override;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Simple Resource Gather Point")
    TSubobjectPtr<UInstancedStaticMeshComponent> InstancedMeshComp;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Resource Gather Point")
    class UStaticMesh* SGRP_Mesh;

    // First value is min, second is max, scaling is uniform (X, Y, Z)
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Resource Gather Point")
    FVector2D MinMaxScaleUniform;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Resource Gather Point")
    TEnumAsByte<ESRGP_Type::SRGP_Type> ResourceType;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Resource Gather Point")
    float AmountAwarded;

    UPROPERTY(BlueprintReadOnly, Category = "Simple Resource Gather Point")
    TArray<int32> MeshIndex;

    UFUNCTION(BlueprintCallable, Category = "Simple Resource Gather Point")
    void AddMeshInstance(FTransform InstanceTransform);
};
