#include "FPSProject.h"
#include "SimpleResourceGatherPoint.h"


ASimpleResourceGatherPoint::ASimpleResourceGatherPoint(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    InstancedMeshComp = PCIP.CreateDefaultSubobject<UInstancedStaticMeshComponent>(this, TEXT("SRGP_InstancedStaticMesh"));
    RootComponent = InstancedMeshComp;

    // Default inits
    ResourceType            = ESRGP_Type::PALMWOOD;
    AmountAwarded           = 1.0f;
}

void ASimpleResourceGatherPoint::PostInitializeComponents() {
    Super::PostInitializeComponents();

    if (SGRP_Mesh != nullptr) {
        InstancedMeshComp->SetStaticMesh(SGRP_Mesh);
    }
}

void ASimpleResourceGatherPoint::AddMeshInstance(FTransform InstanceTransform) {
    InstancedMeshComp->AddInstance(InstanceTransform);

    HaturHelper::QL("Added instance, current number of ChildInstances", InstancedMeshComp->GetNumChildrenComponents());
}
