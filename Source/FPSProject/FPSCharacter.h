

#pragma once

#include "GameFramework/Character.h"
#include "FPSPlayerController.h"
#include "GameHUD.h"
#include "Inventory/Inventory.h"
#include "Inventory/Weapons/InstantWeapon.h"
#include "Inventory/Weapons/ProjectileWeapon.h"
#include "Crafting/CraftingManager.h"
#include "FPSCharacter.generated.h"

/**
 *
 */
UENUM(BlueprintType)
namespace EInteractableWorldObject {
    // Don't use this enum, use a list of ACraftingStation subclasses instead later on Recipe filtering
    enum InteractableWorldObject {
        NONE,
        CRAFTINGSTATION_SIMPLE,
    };
}

UENUM(BlueprintType)
namespace EEmoteAnimation {
    enum EmoteAnimation {
        EMOTE_NONE,
        EMOTE_FUCKYOU
    };
}

UCLASS()
class AFPSCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

    void PostInitializeComponents() override;

    void BeginPlay() override;

    void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

    void ReceiveDestroyed() override;

    void Tick(float DeltaTime) override;
	
	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	TSubobjectPtr<UCameraComponent> FirstPersonCameraComponent;
	
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
    TSubobjectPtr<USkeletalMeshComponent> FirstPersonMesh;

//    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Equiped Item Mesh")
//    TSubobjectPtr<UStaticMeshComponent> EquipedItem_FP;

//    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Equiped Item Mesh")
//    TSubobjectPtr<UStaticMeshComponent> EquipedItem_TP;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Collecting")
    TSubobjectPtr<class UBoxComponent> ItemCollectorBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sockets")
    FName RightHand1hSocket;

    /** Gun muzzle's offset from the camera location*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    FVector MuzzleOffset;

    /** Projectile class to spawn*/
    UPROPERTY(EditDefaultsOnly, Category = Projectile)
    TSubclassOf<class AFPSProjectile> ProjectileClass;

    // Is the Character alive, controllable?
    bool IsAlive;

    // The inventory INSTANCE
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, ReplicatedUsing = OnRep_InvTest, Category = Inventory) // Transient removed
    AInventory* MyInventoryInstance;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = CraftingManager)
    ACraftingManager* CraftingManager;

    UPROPERTY(BlueprintReadOnly, Category = Item)
    TEnumAsByte<EInteractableWorldObject::InteractableWorldObject> CharacterInteractingWith;

    UPROPERTY(BlueprintReadOnly, Replicated, Category = Character)
    TEnumAsByte<EEmoteAnimation::EmoteAnimation> CharacterEmoteAnimation;

    UFUNCTION()
    void OnRep_InvTest();

    UFUNCTION()
    void TestAction();

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_SetEmote();

    UFUNCTION()
    void ResetEmoteStatus();

    // Returns actor on trace
    UFUNCTION(BlueprintCallable, Category = "Character | Interaction")
    AActor* TraceAndReturnActor();
	
protected:
	UFUNCTION()
	void MoveForward(float Val);

	UFUNCTION()
	void MoveRight(float Val);

    void AddControllerPitchInput(float val) override;
    void AddControllerYawInput(float val) override;

	// sets jump flag when key is pressed
	UFUNCTION()
	void OnStartJump();

	// clears jump flag when key is released
	UFUNCTION()
	void OnStopJump();

    UFUNCTION()
    void OnToggleInventory();

    UFUNCTION()
    void OnToggleCharacterPanel();

    UFUNCTION()
    void OnToggleCraftingManager();

    // Use item | Start weapon fire
    UFUNCTION(BlueprintCallable, Category = "Character | Action")
    void StartPrimaryAction();

    // Stop weapon fire
    UFUNCTION(BlueprintCallable, Category = "Character | Action")
    void StopPrimaryAction();

    UFUNCTION(BlueprintCallable, Category = "Character | Action")
    void OnInteract();

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_OnInteract(AFPSCharacter* Initiator, AActor* InteractableActor);

    UFUNCTION()
    void OnFire();

    UFUNCTION(Reliable, Server, WithValidation)
    void Server_OnFire(FVector Origin, FVector_NetQuantizeNormal ShootDir);

    UFUNCTION(BlueprintCallable, Category = "Character | Action")
    void OnAbortCrafting();

    // Calls functions on inventory instance
    void EquipItem1();
    void EquipItem2();
    void EquipItem3();
    void EquipItem4();
    void EquipItem5();

    UFUNCTION()
    void OnItemCollectorOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

    AFPSPlayerController* GetMyPlayerController();
    AGameHUD* GetMyGameHUD();

private:
    // Trace only every X Seconds
    float TraceAccumulator;

    FORCEINLINE void DoInteractableTrace();
};
