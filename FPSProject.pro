TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

# Declare and assign specific variable, you may change this path to work (do not add \ at the end of path !)
UNREAL_PATH = C:\Program Files\Unreal Engine\Unreal Engine
VISUAL_PATH = C:\Program Files (x86)\Microsoft Visual Studio 12.0

# Will define all unreal defines
include(defines.pri)

# Project source path (you may complete this)
HEADERS += Source\FPSProject\*.h \
    Source/FPSProject/InventorySystem/BaseItemClass.h \
    Source/FPSProject/BaseItemClass.h \
    Source/FPSProject/FPSStyle.h \
    Source/FPSProject/FPSStyleTestWidget.h \
    Source/FPSProject/FPSStyleTestWidgetStyle.h \
    Source/FPSProject/FPSUIResources.h \
    Source/FPSProject/MenuStyles.h \
    Source/FPSProject/GlobalMenuStyle.h \
    Source/FPSProject/GameMenuHUD.h \
    Source/FPSProject/SMenuUI.h \
    Source/FPSProject/SGameUI.h \
    Source/FPSProject/GlobalGameHUDStyle.h \
    Source/FPSProject/NIGWindow.h \
    Source/FPSProject/SGameUI_InventoryOverlay.h \
    Source/FPSProject/Inventory/Items/BaseItem.h \
    Source/FPSProject/Inventory/Inventory.h \
    Source/FPSProject/Inventory/BaseItem.h \
    Source/FPSProject/SGameUI_InventoryContentBox.h \
    Source/FPSProject/SCaptureOverlay.h \
    Source/FPSProject/SGameUI_CharacterPanelOverlay.h \
    Source/FPSProject/CharacterPanel/CharacterPanel.h \
    Source/FPSProject/Inventory/UsableItem.h \
    Source/FPSProject/Inventory/WeaponItem.h \
    Source/FPSProject/Inventory/InstantWeapon.h \
    Source/FPSProject/Inventory/ProjectileWeapon.h \
    Source/FPSProject/Inventory/Weapons/InstantWeapon.h \
    Source/FPSProject/Inventory/Weapons/ProjectileWeapon.h \
    Source/FPSProject/Crafting/BaseBuilding.h \
    Source/FPSProject/Crafting/BaseRecipe.h \
    Source/FPSProject/Crafting/CraftingManager.h \
    Source/FPSProject/Crafting/RecipeManager.h \
    Source/FPSProject/SGameUI_CraftingMenu.h \
    Source/FPSProject/Crafting/Buildings/CraftingStation.h \
    Source/FPSProject/Crafting/Buildings/BaseBuilding.h \
    Source/FPSProject/Inventory/AbstractMetaItems.h \
    Source/FPSProject/CraftableObjectInterface.h \
    Source/FPSProject/Interfaces/CraftableObjectInterface.h \
    Source/FPSProject/Inventory/Weapons/Axe.h \
    Source/FPSProject/Slate/CraftingMenu/SGameUI_CraftingMenu.h \
    Source/FPSProject/Slate/Inventory/SGameUI_CharacterPanelOverlay.h \
    Source/FPSProject/Slate/Inventory/SGameUI_InventoryContentBox.h \
    Source/FPSProject/Slate/Inventory/SGameUI_InventoryOverlay.h \
    Source/FPSProject/Slate/SGameUI.h \
    Source/FPSProject/Slate/SMenuUI.h \
    Source/FPSProject/Slate/GlobalGameHUDStyle.h \
    Source/FPSProject/Slate/GlobalMenuStyle.h \
    Source/FPSProject/Slate/MenuStyles.h \
    Source/FPSProject/Slate/CraftingMenu/SGameUI_CraftingMenuEntry.h \
    Source/FPSProject/Slate/CraftingMenu/SGameUI_CraftingMenuTooltip.h \
    Source/FPSProject/Crafting/Buildings/InteractableBuilding.h \
    Source/FPSProject/Crafting/Buildings/Fireplace.h \
    Source/FPSProject/Interfaces/InteractableObjectInterface.h \
    Source/FPSProject/Inventory/Usables/BlueprintUsable.h \
    Source/FPSProject/HaturHelper.h \
    Source/FPSProject/FPSWorldSettings.h \
    Source/FPSProject/GameSettings.h \
    Source/FPSProject/Slate/Interfaces/MovableWidget.h \
    Source/FPSProject/Slate/Interfaces/MovableWidgetInterface.h \
    Source/FPSProject/Slate/Interfaces/MovableWindowInterface.h \
    Source/FPSProject/Slate/MovableWidget.h \
    Source/FPSProject/Slate/BackgroundOverlay.h \
    Source/FPSProject/Slate/CraftingMenu/SGameUI_CraftingProgressBar.h \
    Source/FPSProject/AI/NPC.h \
    Source/FPSProject/AI/NPCController.h \
    Source/FPSProject/AI/NPCController_Moving.h \
    Source/FPSProject/AI/NPCController_MeleeAttacker.h \
    Source/FPSProject/AI/Specific/NPCC_AlienCrab.h \
    Source/FPSProject/MainMenu/MM_GameMode.h \
    Source/FPSProject/MainMenu/MM_Pawn.h \
    Source/FPSProject/MainMenu/MM_PlayerController.h \
    Source/FPSProject/Online/CommManager.h \
    Source/FPSProject/Online/CommManager.h \
    Source/FPSProject/MainMenu/MM_ServerListEntry.h \
    Source/FPSProject/Online/MM_ServerListEntry.h \
    Source/FPSProject/Online/MM_GameSession.h \
    Source/FPSProject/MainMenu/MM_GameSession.h \
    Source/FPSProject/Online/OnlineGameSettings.h \
    Source/FPSProject/IAFoliage/SimpleResourceGatherPoint.h \
    Source/FPSProject/FPSGameState.h \
    Source/FPSProject/IAFoliage/WorldResourceManager.h
SOURCES += Source\FPSProject\*.cpp \
    Source/FPSProject/InventorySystem/BaseItemClass.cpp \
    Source/FPSProject/BaseItemClass.cpp \
    Source/FPSProject/FPSStyle.cpp \
    Source/FPSProject/FPSStyleTestWidget.cpp \
    Source/FPSProject/FPSUIResources.cpp \
    Source/FPSProject/MenuStyles.cpp \
    Source/FPSProject/GlobalMenuStyle.cpp \
    Source/FPSProject/GameMenuHUD.cpp \
    Source/FPSProject/SMenuUI.cpp \
    Source/FPSProject/SGameUI.cpp \
    Source/FPSProject/GlobalGameHUDStyle.cpp \
    Source/FPSProject/NIGWindow.cpp \
    Source/FPSProject/SGameUI_InventoryOverlay.cpp \
    Source/FPSProject/Inventory/Items/BaseItem.cpp \
    Source/FPSProject/Inventory/Inventory.cpp \
    Source/FPSProject/Inventory/Items/Item_Wood.cpp \
    Source/FPSProject/Inventory/BaseItem.cpp \
    Source/FPSProject/SGameUI_InventoryContentBox.cpp \
    Source/FPSProject/SCaptureOverlay.cpp \
    Source/FPSProject/SGameUI_CharacterPanelOverlay.cpp \
    Source/FPSProject/CharacterPanel/CharacterPanel.cpp \
    Source/FPSProject/Inventory/UsableItem.cpp \
    Source/FPSProject/Inventory/WeaponItem.cpp \
    Source/FPSProject/Inventory/InstantWeapon.cpp \
    Source/FPSProject/Inventory/ProjectileWeapon.cpp \
    Source/FPSProject/Inventory/Weapons/InstantWeapon.cpp \
    Source/FPSProject/Inventory/Weapons/ProjectileWeapon.cpp \
    Source/FPSProject/Crafting/BaseBuilding.cpp \
    Source/FPSProject/Crafting/BaseRecipe.cpp \
    Source/FPSProject/Crafting/CraftingManager.cpp \
    Source/FPSProject/Crafting/RecipeManager.cpp \
    Source/FPSProject/SGameUI_CraftingMenu.cpp \
    Source/FPSProject/Crafting/Buildings/CraftingStation.cpp \
    Source/FPSProject/Crafting/Buildings/BaseBuilding.cpp \
    Source/FPSProject/Inventory/AbstractMetaItems.cpp \
    Source/FPSProject/CraftableObjectInterface.cpp \
    Source/FPSProject/Interfaces/CraftableObjectInterface.cpp \
    Source/FPSProject/Inventory/Weapons/Axe.cpp \
    Source/FPSProject/Slate/CraftingMenu/SGameUI_CraftingMenu.cpp \
    Source/FPSProject/Slate/Inventory/SGameUI_CharacterPanelOverlay.cpp \
    Source/FPSProject/Slate/Inventory/SGameUI_InventoryContentBox.cpp \
    Source/FPSProject/Slate/Inventory/SGameUI_InventoryOverlay.cpp \
    Source/FPSProject/Slate/SGameUI.cpp \
    Source/FPSProject/Slate/SMenuUI.cpp \
    Source/FPSProject/Slate/GlobalGameHUDStyle.cpp \
    Source/FPSProject/Slate/GlobalMenuStyle.cpp \
    Source/FPSProject/Slate/MenuStyles.cpp \
    Source/FPSProject/Slate/CraftingMenu/SGameUI_CraftingMenuEntry.cpp \
    Source/FPSProject/Slate/CraftingMenu/SGameUI_CraftingMenuTooltip.cpp \
    Source/FPSProject/Crafting/Buildings/InteractableBuilding.cpp \
    Source/FPSProject/Crafting/Buildings/Fireplace.cpp \
    Source/FPSProject/Interfaces/InteractableObjectInterface.cpp \
    Source/FPSProject/Inventory/Usables/BlueprintUsable.cpp \
    Source/FPSProject/HaturHelper.cpp \
    Source/FPSProject/FPSWorldSettings.cpp \
    Source/FPSProject/GameSettings.cpp \
    Source/FPSProject/Slate/Interfaces/MovableWidget.cpp \
    Source/FPSProject/Slate/Interfaces/MovableWidgetInterface.cpp \
    Source/FPSProject/Slate/Interfaces/MovableWindowInterface.cpp \
    Source/FPSProject/Slate/MovableWidget.cpp \
    Source/FPSProject/Slate/BackgroundOverlay.cpp \
    Source/FPSProject/Slate/CraftingMenu/SGameUI_CraftingProgressBar.cpp \
    Source/FPSProject/AI/NPC.cpp \
    Source/FPSProject/AI/NPCController.cpp \
    Source/FPSProject/AI/NPCController_Moving.cpp \
    Source/FPSProject/AI/NPCController_MeleeAttacker.cpp \
    Source/FPSProject/AI/Specific/NPCC_AlienCrab.cpp \
    Source/FPSProject/MainMenu/MM_GameMode.cpp \
    Source/FPSProject/MainMenu/MM_Pawn.cpp \
    Source/FPSProject/MainMenu/MM_PlayerController.cpp \
    Source/FPSProject/Online/CommManager.cpp \
    Source/FPSProject/Online/CommManager.cpp \
    Source/FPSProject/MainMenu/MM_ServerListEntry.cpp \
    Source/FPSProject/Online/MM_ServerListEntry.cpp \
    Source/FPSProject/Online/MM_GameSession.cpp \
    Source/FPSProject/MainMenu/MM_GameSession.cpp \
    Source/FPSProject/Online/OnlineGameSettings.cpp \
    Source/FPSProject/IAFoliage/SimpleResourceGatherPoint.cpp \
    Source/FPSProject/FPSGameState.cpp \
    Source/FPSProject/IAFoliage/WorldResourceManager.cpp

# Project include path (you also may complete this)
INCLUDEPATH += Source\FPSProject

# include path for generated files
INCLUDEPATH += Intermediate\Build\Win64\Inc\FPSProject

# Will add all unreal include path 
include(includes.pri)
